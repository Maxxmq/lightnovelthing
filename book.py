import shutil

from ebooklib import epub
import os.path



def makebook(folder, title):
    path = ""
    if folder.startswith("/"):
        path = os.path.curdir + folder
    else:
        path = os.path.curdir + "/" + folder


    if os.path.isdir(path):

        book = epub.EpubBook()
        book.set_identifier('id44773005')
        book.set_title(title)
        book.set_language('en')

        chapterno = 1
        toc = ()
        while os.path.isfile(path + "/chapter" + str(chapterno) + ".html"):
            chapter = epub.EpubHtml(title="chapter"+str(chapterno), file_name="chapter"+str(chapterno)+".xhtml")
            file = open(path + "/chapter" + str(chapterno) + ".html", "r", encoding="utf8")
            chapter.content = file.read()
            file.close()
            book.add_item(chapter)
            toc = toc + (chapter, )
            chapterno = chapterno + 1
        book.spine = list(book.get_items())

        book.toc = toc
        book.add_item(epub.EpubNcx())

        style = 'BODY {color: white;}'
        nav_css = epub.EpubItem(uid="style_nav", file_name="style/nav.css", media_type="text/css", content=style)
        book.add_item(nav_css)

        epub.write_epub(title + ".epub", book, {})
        os.rename(os.path.curdir + "/" + title + ".epub", os.path.curdir + "/" + "/BOOKS/" + title + ".epub")
        print(title + " - BOOK MADE")

    else:
        print("ERROR FOLDER PATH DOSENT EXIST")
        return False

    return True
