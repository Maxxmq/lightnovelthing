import requests
from bs4 import BeautifulSoup

from book import makebook

url = "https://krytykal.org/only-sense/volume-"#(volnumber)/chapter-(chapternumber)


def getpage(pageurl):
    r = requests.get(pageurl)
    soup = BeautifulSoup(r.text, "html5lib")
    html = soup.find("div", "entry-content")
    title = soup.find("h1", "entry-title").prettify()

    for tag in html.find_all("sup"):
        if tag.get('class') != None:
            atag = tag.contents[0]
            href = atag.get('href')
            atag.string = "( " + html.find("li", id=href[1:]).text[1:] + " )"
            del atag['href']
            del atag['title']
            tag.unwrap()
        else:
            continue


    while html.ol != None:
        html.ol.decompose()
    while html.sup != None:
        html.sup.decompose()

    return title + "\n\n" + html.prettify(formatter="html")


for vol in range(1, 10):
    if vol == 1 or vol == 2:
        html = ""
        for chap in range(1, 11):
            if chap == 1:
                chapurl = url + str(vol) + "/prologue"
            elif chap == 10:
                chapurl = url + str(vol) + "/epilogue"
            else:
                chapurl = url + str(vol) + "/chapter-" + str(chap-1)

            html = html + getpage(chapurl)

        file = open("FILES/OnlySense/chapter" + str(vol) + ".html", 'w', encoding="utf-8")
        file.write(html)
        file.close()
        print("chapter " + str(vol) + " - DONE")



    else:
        html = ""
        for chap in range(1, 9):
            if chap == 1:
                chapurl = url + str(vol) + "/prologue"
            elif chap == 8:
                chapurl = url + str(vol) + "/epilogue"
            else:
                chapurl = url + str(vol) + "/chapter-" + str(chap-1)

            html = html + getpage(chapurl)

        file = open("FILES/OnlySense/chapter" + str(vol) + ".html", 'w', encoding="utf-8")
        file.write(html)
        file.close()
        print("chapter " + str(vol) + " - DONE")

makebook("FILES/OnlySense", "OnlySense")
print("DONE")