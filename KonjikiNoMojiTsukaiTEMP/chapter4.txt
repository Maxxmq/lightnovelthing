Chapter 04: Word Magic

(I think the “Asbit Plateau” is due east of here)
Hiiro started walking towards the plateau while remembering the picture of the “Lucky Herb” he was shown. After walking for a while, he found it rather easy. It looked exactly like on the picture.
It had a small, white bud at its head and was growing all around.
(Even a kid could do this quest)
It was close to the town and could be collected easily as it grew in large quantities. An easy task, even for a beginner.
(No one’s around here)
Looking around, he noticed that no one besides him was here. Then he recited ≪Status≫ and looked at the column where ≪Word Magic≫ was written, lightly touching it with his finger. Upon that, the panel changed and showed an explanation about the ≪Word Magic≫.
(So the help shows up after all when I click it. This really is like a game)
Hiiro didn’t just wanted to read the help. He wanted to try the magic here.
If possible, he would like to avoid showing off his unique magic, because he feared that he would get dragged back to the king if he stood out, after he finally obtained freedom.
(Well, it depends on the magic anyway)
If all unique magic were indeed powerful, it was quite possible that his fear came true. Besides, it was necessary to understand the magic to grasp his self, like the old woman had told him earlier.
But he didn’t intend to be reckless, since there was the so-called ≪rebound≫. Hiiro just wanted to find out what his magic was, not to die here already.
≪Word Magic≫ MP cost: 30
Imagine the magic power gathering in your finger and draw a word. An effect in accordance with the meaning of the word is brought forth. It is a unique magic with the phenomenal power to apprehend and contort the underlying principles. ≪Single Chain unlocked≫ refers to the possible chain length of words. This magic was once ?%&GR!&*
For some reason the last part was corrupted and unreadable. He was quite bothered about it, but he somehow understood this magic. Still, he wouldn’t know if he was right unless he actually tried out the magic.
(So it’s called Word Magic. Let’s try it for now)
With that in mind, he took a deep breath and concentrated magic power in his fingertip in the same way as in front of the old woman. Before it took him a little while, but on his second try, his finger lighted up with magic power rather smoothly.
(A word, huh… Will anything do? But since it’s manifesting the word…)
He drew on the ground with his finger. Upon that, the pattern glowed in a pale light. He had written the kanji for “hard” while imagining the ground hardening. It was easier to imagine with kanji.
And the moment he recited ≪Activate≫ in his mind, the magic power from the word flowed into the ground while making sizzling sounds like on an electric discharge.
(Did that do it…?)
He knocked onto the ground. It was hard. Incredible hard. Pretty much like concrete. Until a moment ago, it had been loose dirt without a doubt.
He went around, checking the range of effect.
Clonk, Clonk, Clonk…Splat.
The ground was hardened in an area roughly 6.5m² wide.
The glowing word dispersed and vanished too. It was quite convenient that it left no traces behind, because it lowered the likelihood of his magic getting exposed. Then he reverted it with the kanji for “Origin”.
The electrical discharge happened again and the ground returned to how it had been before.
(This is… more overpowered than I thought)
Hiiro realized the incredible potential of his own magic. A magic to apprehend and contort the underlying principles. It meant affecting anything with a single word.
For example, if he gave the “lucky herbs” around here the effect from the word “wither”, then they would wither. If he gave a rock the effect “split”, then it would break into two.
(It can change all kind of phenomena regardless… Moreover)
With that in mind, he concentrated magic power in his finger once more and wrote another word into the ground. Upon that, a fire suddenly broke out and scorched the grasslands. The word he had written was “fire”. But this time, the fire extinguished after one minute.
(Creating from nothing… Looks like I got my hands on some awesome magic)
He exhaled in awe, realizing that the unique magic held a far greater power than he had imagined.
Still, he was happy about the usefulness of his magic. He should be able to live here without a problem with it. An almighty magic sure was handy.
(But I shouldn’t forget that I still don’t know everything about it. Part of the help text was corrupted and considering the output…)
He called up the ≪Status≫ again by saying so.



Hiiro Okamura
Lvl 1
HP 24/24
MP 30/120
EXP 0
NEXT 10
ATK 13
DEF 8
AGL 27
HIT 11
INT 17
≪Magic Attribute≫ None
≪Magic≫ Word Magic (Single Chain Unlocked)
≪Title≫ Innocent Bystander, World Traveller, Word Master


(I knew it, it costs quite some MP)
His full MP was 120 and now it was down to 30. He had used the magic thrice, so it consumed 30 MP per usage, just like the description said.
He couldn’t compare it to other magic, but the consumption was most likely quite high. His MP being higher than the other numbers was a peculiarity from being a world traveller, or so he heard.
A three-digit number was quite unthinkable for a normal level one. It should usually be around the same as the HP. And the consumption for beginner magic should correspond with that.
In all the games he had played so far, there never had been a magic that cost 30MP right from the beginning. It was an established fact that the strength of a magic, and in accordance its MP cost, increased with level ups.
(I can cast it four times with my current max MP. I need to level up quickly)
After all, the more often you could use a magic, the better. Even more so with an almighty magic like ≪Word Magic≫.
(Okay, I got the magic down. Time to bring this stuff back)
He returned to the town with the bag filled to the brim with „Lucky Herbs“. It would be common to get attacked by nearby monsters in such a situation, but to his luck, he reached the town without any incidents.
There he headed to the Guild to complete the quest.


„Allow me to verify the details of your quest, Hiiro Okamura-sama. You accepted the rank F quest „Lucky Herb Harvest“. Please show me your loot.“
Told so by the woman at the counter, Hiiro put the „Lucky Herbs“ from his bag on a big weighing scale.
„…Okay, that makes twenty-two bundles. Your rewards amounts to 7700 Rigin in total. Please give me your card.“
When he handed over the card, she took it somewhere. After a while she came back and returned his card. Looking at the currency column of the card, the earlier value of 0 had turned into 7700.
“The quest is completed. Good work.”
She lowered her head politely and showed him a business smile like always. Hiiro gave a small nod and left the Guild.
(Good, I have money now. Guess food comes first. I still haven’t eaten anything since I came here)
Asking the villagers, he looked for a restaurant. ≪Victorias≫ was a big walled town, divided into the trade, craft, pleasure and living districts where a lot of people lived.
With each district being rather big, it was structured as if various towns had been merged into one.
Hiiro went to the trade district for a restaurant. When he entered one that he found on the way, it turned out that it served delicious fish cuisine. He didn‘t hate fish, so he decided to stay and checked the menu.
But as expected, all the dish names didn‘t mean anything to him. He ordered today‘s suggestion, since he had no clue.
„Okay~ One serving of ‚Addicting Seafood Noodles‘! Please wait a moment~“
The waitress cheerfully took his order. While he waited for his food, he checked his ≪Status≫ again. There he noticed that his MP recovered up to 40 after having sunk down to 30 from using ≪Word Magic≫ three times.
It probably recovered over time when he rested, just like his stamina. But the recovery rate wasn’t really profitable as it only recovered 10 MP since he used the magic over an hour ago.
(Well, rather than resting, I just walked around the town without using MP)
If he rested in the true sense of the word, meaning sleep, then the recovered amount should be different. As he pondered about that, his steamy food was served.
A lot of seafood swam in a ramen bowl. It was full of eggs from a fish like a salmon roe, something like shrimps and kelp or brown seaweed. He took his chopsticks and stirred it.
By doing so, an exquisite fragrance tickled his nasal cavities. Instantly his stomach reported in with a growl as to demand food at once. He munched on something exquisite like a shark fin.
“Oh!”
He couldn’t hold in his voice. The fin was well flavoured and its fish flavour spread in his mouth. It tasted and smelled so good that he couldn’t help but crave for more.
Then he drank the broth with the spoon. It was good enough to be served as a separate dish. The broth with plenty seafood was so addicting that he would gulp it down in one-go if he didn’t pay attention. It was lightly flavoured, yet superb.
Next he tried the noodles. On a closer look, something like small grains were kneaded into it. From one mouthful, a sea flavour assaulted his taste buds. Mashed fish was kneaded into the noodles. Truly Seafood Noodles.
(Yeah, the addicting in the name isn’t just for show)
He finished his meal in a matter of few minutes and could go for another two or three bowls, but he resisted the temptation, since he was tight on money. Still, 450 Rigin was cheap for it.
After he stomached the satisfying aftertaste, he looked for an inn and made plans for the future, involving levelling up and collecting money, for a while. 