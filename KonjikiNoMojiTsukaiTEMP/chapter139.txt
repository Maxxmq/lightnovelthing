 Chapter 139: Hiiro’s Coming-Out

Shinobu spoke about their group, the heroes who had been summoned to 【Edea】, as well as the reason for why they had come to the 【Demon Country: Xaos】without concealing a single detail.

After discussing it with Shuri, they together had decided that she should do so. It had been Shuri’s idea to talk with the Maou about themselves.

Naturally, Shinobu had been unable to deny the possibility of them being executed on the spot. However, if they remained where they were, not only would they continue to cause trouble for Hiiro, but the chances of them eventually being discovered was high.

Rather than begging for mercy after being found, she felt that coming to the Maou by themselves to appeal to her would be much safer. Even Ornoth had told them that if they were obedient, they wouldn’t be killed, furthering supporting her decision.

“I see. You two are undeniably heroes, correct? However, that leaves one small question.” (Eveam)
“Wh-what might that be?” (Shinobu)

Shinobu asked as she held her breath.

“You said that you had already entered the demon continent when we were crossing the bridge. If that is the case, then who were the heroes that we saw on the bridge?” (Eveam)
“Saw…….on the bridge?” (Shinobu)

As Shinobu gave a blank look, Eveam similarly muttered, “Hm?” and-

“Why are you making such a face? You four heroes were undoubtedly on the bridge you know?” (Eveam)
“…….that must be some kind of mistake……..because we were definitely……..we met an 『Evila』called Iraora, who allowed us to cross the bridge.” (Shinobu)

Upon hearing the name Iraora, Eveam displayed a clouded expression. She had heard about the tragedy that occurred on 【Mütich Bridge】. And, in order to confirm it, they had performed an investigation, yet it seemed to be true.

Former 《Cruel Brigade》member Iraora. She had recognized his strength, and believed that she could leave the defense of the bridge to him. Hence, she had trusted him and left that duty to him. However, she didn’t think that he would betray them, and as a result, lead to the loss of numerous comrades and Greyald.

“Then you, no, all four of you had already crossed thanks to Iraora quite a while ago?” (Eveam)
“I believe so. The king did say that the plan would work out ‘cause he was on our side.” (Shinobu)
“I see……however, if you two don’t know of them, then just who were the heroes that we saw……..” (Eveam)

Eveam muttered with a pensive look on her face, but was unable to come to an answer.

“Allow me to ask one thing.” (Aquinas)

During such a discussion, Aquinas posed a question. Simply by standing there, it was clear that he was a different existence from the others due to his intimidating aura. Shinobu’s nervousness rose even further.

“Kiria…….do you know that name?” (Aquinas)
“Kiria? N-no, I don’t……..but” (Shinobu)
“Then what about the name Valkiria?” (Aquinas)
“N-no……” (Shinobu)

Aquinas stared fixatedly at Shinobu. It appeared that he was checking to see if she had lied or not. Then, he glanced towards Eveam.

“Your Majesty, it is likely that these two do not know anything. It’s the King of Victorias after all. He likely sent the heroes over here, and was planning to use them as a pretext for war later on.” (Aquinas)

It was the same view that Hiiro had told Shinobu and co.

As she realized that it was like that after all, Shinobu hung her head in shame as feelings of frustration overflowed from her chest.

“If the heroes died here, then he would use that as the reason for a new war…….huh. If it’s that king, then it seems plausible that he would be capable of such a thing.” (Eveam)

Eveam frowned sorrowfully.

“Then you really weren’t told anything after all. Even though you’re heroes……..” (Eveam)
“Also, about the heroes that we saw. If you think about how Kiria betrayed us, then those heroes were……..” (Aquinas)
“I see…….dolls, huh.” (Eveam)
“Yes, not only that, but they were made quite…….elaborately. If it was Kiria, who was a doll-maker to begin with, it’s possible. Up until now, we have been helped on numerous occasions thanks to her abilities after all.” (Aquinas)
“………That’s right……..she did help us. Even so………” (Eveam)

Eveam gave a bitter expression as she ground her teeth. Aquinas continued speaking in her place.

“Ornoth, is it true that the remaining two heroes were blown away?” (Aquinas)
“Yeah, it’s true. Due to Prince Lenion’s attack. I made some soldiers looking for them for the time being, but it seems like they can’t find them.” (Ornoth)
“………fumu. It appears that there were no lies within what they’ve told us. However, there is one thing that doesn’t make sense.” (Aquinas)

Shinobu was startled once more. Naturally, there was nearly nothing left that they hadn’t told them already. They had intended on answering any questions they gave honestly. Her heart began beating violently at the thought that there were still some inadequacies with their explanations.

“You don’t need to get that nervous. Although, even if I say that, I guess it can’t be helped. What I don’t understand isn’t about you, but Hiiro………about his actions.” (Aquinas)

“………..eh?” (Shinobu)

Shinobu became dumbfounded, but the surrounding people simultaneously glanced towards Hiiro. The person in question, Hiiro, still had his eyes closed, but-

(So it came to this after all………) (Hiiro)

As though he had expected it, Hiiro opened his eyes and raised his face to look at Aquinas.

“That’s right. What I don’t understand is why Hiiro, who should have been your enemy, brought you heroes all the way here. If you were simply strangers, he would either ignore you, or kill you two human girls. Either way, there’s no way that Hiiro and the two of you are completely unrelated. That’s what I sensed, but how is it?” (Aquinas)

Hiiro clicked his tongue internally. Probably, if it was Eveam, then she wouldn’t have thought of that question. Whether for the better or the worse, since she had such a straight-forward personality, she may not have held any doubts with regards to the meaning of Hiiro’s actions.

Even if she held some doubts, she wouldn’t have had the confidence to relay them well. However, the other party was unfortunately a top-ranking person amongst the Maou’s army. Hiiro felt that he didn’t seem to be a person who would be amused by word games. Honestly speaking, he had wanted things to go on without that question arising.

However, Aquinas had magnificently managed to point out that question. The first one to react towards the identified issue was Shinobu.

“A-ah, that is! We………it’s because we earnestly asked him for such an unreasonable request! That’s why he really didn’t do anything bad!” (Shinobu)

Though she spoke in a very flustered manner, she didn’t want to cause any more problems for Hiiro, who had brought them here, and tried to give an excuse. However, when Hiiro had brought the two of them here, he was more than prepared for this kind of situation to result.

“Iya, I cannot imagine that this man would do something like this for complete strangers. There would either need to be some secret agreement, or………..perhaps elicited empathy caused by an old friendship………huh.” (Aquinas)

At Aquinas’s inquiry, Shinobu’s face became pale and was about to object when-

“Yeah, that’s right. I’ve been acquainted with these guys before.” (Hiiro)

Hiiro’s expression didn’t change as he uttered so indifferently. Some of the surrounding peoples’ facial colour changed. At the thought that he was an acquaintance of the heroes’, their level of wariness increased further.

The soldiers began to get noisy as their hostility gradually increased. Even Marione gave him a glare that suggested that it wouldn’t be strange if he started to attack him now.

However, Aquinas did not show any signs of surprise, and, instead, gave a face of comprehension as he spoke once more.

“………as I thought. I heard about it from the soldier who guided you here. That you were behaving as though they were acquaintances. And that Ornoth also seemed to know about it as well.” (Aquinas)

When Hiiro had first met with the four heroes in this country, Ornoth had also been nearby. Based on the conversation they had at that time, even if they couldn’t be thought of on friendly terms, he had at least judged them to have known each other from before.

“Hi-Hiiro? Is that true?” (Eveam)

As Eveam still couldn’t believe it, she timidly asked thus.

“Yeah.” (Hiiro)
“I-is that so………iya, it’s not like I intend on condemning you for just being acquaintances. But um……” (Eveam)
“Why is an 『Evila』like myself the acquaintance of heroes…right?” (Hiiro)
“Ye-yes, but…….” (Eveam)
“That’s simple.” (Hiiro)

Other than Hiiro’s companions, the eyes of all the people in the room popped out and widened as they froze. It was because Hiiro suddenly began using magic and-

“………..it’s because I was also one of the summoned people.” (Hiiro)

