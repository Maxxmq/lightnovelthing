CHAPTER 206: THE BEGINNING OF THE FEAST

The next day, after taking a good rest, although still suffering from his muscular pain, Hiiro was able to part with his fatigue.

Though it was already in the early afternoon when he woke up, loud noises dominated the castle. But, no sense of tension was felt. Instead, everyone’s face was full of feelings of joy.

From the outside of the castle, cheerful music and the people’s vigor have reached their ears. Yes. As Eveam declared yesterday, today is scheduled to hold a banquet of victory.

However, in the town overlooking from the castle, there are people here, people there, and people everywhere. Like ants gathering over a huge ice cream, they cover the ground busily.

Men who are having pleasant chat while holding liquor in one hand. Women who are wearing upright clothing and showing a splendid dance.

Hands are clapping happily here, children humming songs there, everyone was enjoying the reverberations of the victory everywhere.

Around the castle, many people gather to hear Eveam’s speech. She look around such people with a broad smile on her face.

And Eveam, to everyone, formally announced the alliance with the [Gabranth]. Of course, there are those who have dubious expressions too, but still, everyone raised a roar to the Demon Lord who defeated the [Gabranth].

Even if there is still some way to go before fully implementing the alliance, still as this is one big new step, even a little bit, everyone felt it too.

Even though there are still some lingering discomfort, with this, the beastmen consented that the fighting is finally over.

However Eveam, this time, didn’t mention Avoros in her speech. With everybody rejoicing with so much effort, it became impossible for her say that there will be another impending crisis of a war again.

The real intentions of Avoros is still under investigation. She decided to consult with Aquinas and have decided to inform everyone after everything is grasped.

“Being a ruler sure is difficult.”

Having picked up his muttering quickly,

“Ho-hou, fancying to be a king?”

It was Lilyn, also known as the Red Loli. She looks up to him with a cunning smile.

“I want to know how you made your decision with your present words.”

“Fufun, this one has no guts”

“I don’t want to accept leading others. It’s a pain in the ass”

“Kukuku, it suites a bastard like you”

However, as he thought about it, he let his eyes swim to Eveam who continues making her speech.

(It sure have grown up a little……that flower garden)

Eveam also, should have spent much efforts to be able to make them consider by this duel. Though what she have done in her life haven’t still paid off so far, the alliance which she wished came true, and it became a matter of concern whether she was able to grow up as a ruler.

Even though Eveam’s head is filled with ideas of an idealist, there is no other person this excellent other than her to arrive here.

(Well, judging from her face, she had become a little better…isn’t she?)

When he met her for the first time, there is this pathetic feeling of insecurity drifting around her, and not a single atom of her being as a ruler was not felt either, but after the duel, he felt that she became a slightly firm face of dignity.

(Though I can feel her beginning to walk the path of a ruler, whether she will grow bigger or not depends on her from now on)

To be frank, even if Eveam grows up or not, it doesn’t matter. What’s more important is that she sticks to her promise.

As he heard that the Fortuna Great Library will open as soon as this party is over, Hiiro can’t stand his anticipation for it.

And the feast today. Because the feast dishes were made by the cook named Musun, his stomach is rumbling just from thinking about it.

(Aah～ Dinner isn’t ready yet……)

Because he missed the time of the day to eat with all his might, he anxiously waited for the meal tonight.

The sun has set, and the long-awaited night came. It wasn’t in the dining area where there is a long table like the last time, but Hiiro was called in the banquet hall, and a surprising scene was revealed before him.

A party that totally resembles a reception of a wedding ceremony of the King of the country was held. With the halls decorated gorgeously, illuminated by the light of a vivid chandelier, many people wearing formal attire were waiting for Hiiro.

To Hiiro’s appearance, various gazes flit about. Respect, Yearning, Envy, Love, Jealousy, etc. the people who were aware of his achievements aiming to be him by all means.

“……slurp”

Even though Hiiro has no time for them, he was captivated to the gorgeous meal placed on the table. Seeming to be made from various high quality ingredients, he saw a shining halo in it.

Thinking that he may not be able to endure it any longer, Hiiro runs to the table at full speed. By the way, he was dressed up in a black tuxedo as well.

Since it was troublesome to wear it one by one, he made the tuxedo instead by using  the Word Magic『変化』(Change).

When he sat at the large table, Musun was there.

“Well well, if it isn’t Hiiro-sama, if you don’t mind, I could provide the description of the dish but-”

However, before she were able to finish her words, Hiiro lifts the plate with the dish in his hand, taking it away. And, gobbling it in his mouth, he made an expression full of ecstasy.

To his reaction, Musun’s cheek loosened a bit with pleasure to Hiiro who is with his mouth wide open.

“Nomnomnom…….Hmm? This is so good!”

He thrusted something like a fish with his fork. Then stuck it out in order to show it to Musun, who broke into a smile and said,

“That is called Grilled Salted Tiger Tuna. A tuna which has a tiger-like pattern, its meat has less fat and has a very light taste. It has a perfect elastic body. The body which returns a tooth worth biting, it has a texture that leaves an impression of irresistible to connoisseurs.”

Certainly there is not a trace of elasticity left. Because the meat has a little fat, the taste isn’t too strong and really boosts your appetite. In addition, the seasoning is quite exquisite, it matches the rich sauce that looked like a soy sauce, making it even tastier.

Next, his eyes moved to a considerably big ball-shaped leaves wrapped up like a lettuce.

“This one is called Steamed Rice Cake in Lemon Leaves. When steamed, the greatest taste of the can be achieved. But that isn’t just merely wrapping it up with Lemon Leaf and steaming it. With the unique aroma and acidity of the lemon, soaking the moderately into it, it brings out the best taste”

“Nomm………Woah!?”

He didn’t think that the taste and texture of meat could be brought out in a rice cake. This is out of his expectations. Although it is a rice cake, it has a flavor of meat. And, with the aroma of lemon drifting about, the sour taste accumulating in the tongue incites the appetite even more.

“This is terrific…….Oh, what’s this?”

It was something which seems to be a certain food on the plate, but this one burns just like a fire. It was as large as a fist, but somewhat bewildering to be held by bare hands.

But seeing Hiiro being unable to stand it any longer, Musun happily gave an explanation.

“It’s all right. As for that, called the Fireball Egg, it is made by boiling the egg of the Flaming Red Bird, though it looks like it has been wrapped in fire making one hesitate to hold it, it becomes possible to hold and boil it after freezing it for once. Of course, this is edible. The very rich flavor, on the very first taste……No, first of all, have a bite”

Though she ended it in a worrisome way, it is not certainly hot like fire if held barehanded. However, the fragrant smell of the totally burnt soy sauce drifts from the .

He provoke it with his index finger in temptation, and took a gulp of it unconsciously.

“Guu……Hmm?”

Quickly, the food texture is just like tempura that had just been fried, the pleasant aroma spreads in his mouth. It tastes like fried eggs yet this one has a secret ingredient within.

When it reached his stomach, it appeared. The insides of his tummy becomes hot suddenly. However, there is no unpleasant feeling. In fact, it gives somewhat a reassuring warmth in the whole body.

For the body that gets chilled completely, it has a sensation like a warm soup that was poured inside the stomach, he leaks out sigh of ecstasy unintentionally.

“Fufu, when the enters the stomach, it gives a warming effect to the body. Moreover, that time, because a pleasant sensation runs through the whole body, this food becomes quite addictive”

Certainly, this sense of comfort is quite addicting, and I really want to feel it all the time.

“Mmmm, Delicious!”

“I am truly grateful. By the way, please have this main dish”

As Musun said so, she showed a huge platter.

“Whoa～ look how round that is!”

Nikki who had come nearby before everyone is aware exclaimed with her eyes gleaming. She is also dressed in a formal attire suited for the party. She wore a blue dress like a girl, but unlike her usual image, she truly gave an atmosphere appropriate for a lady.

Besides her are Lilyn and Silva, Shamoe being led by a hand, and Mikadzuki approached, too. Though Silva is in his usual butler clothes, the three women wore gorgeous clothes in their bodies.

Shamoe was wearing a provocative jade dress that exposed skin and generally emphasizes her volouptous chest while Mikadzuki was wearing something similar to Nikki.

And……

“Hey, Nitoryuu, why are you in butler clothes?”

Before their eyes, there is Camus who wore a tailcoat which is strangely looking good in him. While the person in question inclines his neck,

“Nn……Suit me?”

As he heard him talk, because Silva matches me somehow or other, he suggested this one, and because his face is certainly regulated well that he can be classified as a handsome man and a beautiful girl, when such clothes are worn, the height of his looks really stand out.

“This……may I eat?”

“Do as you like”

“……suit myself”

Camus, feeling hungry, began eating the meals with great relish.

Furthermore……

“Hmm? What?”

Lilyn……as expected, was in gothic Lolita outfit after all. But the way she dressed is so perfect, totally possessing the prettiness of a doll. Truly a shining red haired beauty in a white dress.

“Well, as I thought, that kind of clothing suits you well after all”

As one would expect of this loli, he nodded in his mind many times,

PUFF!

Her face boils up suddenly, with the complexion rivaling the that he has eaten a while ago.

“Wha-Wha-What the hell are you saying, you pleb!”

Why are you getting angry? I only said that you look good because you look good.

“Fu,Fun! O-Of courshhe, i-it’s all natural for me to look good!”

If you look more closely, your cheeks have slightly loosened, Are you not angry that much? When he thought that there is no need to worry and turn his face away,

“Ah…”

He heard a voice that seemed to be of disappointment, but it was already drowned out,

“Wooaah～! This is so good!”

“It’s true～! Shamoe-chan, you too, try it～！”

“Y-yes, right away!”

After seeing Nikki and Mikadzuki taking bites of the dish, Shamoe worrying about the mess on their lips, pulls out a handkerchief from her bosom and goes to the two.

“……isn’t it great, milady? Hiiro-sama praised you”

“Wha!? I-it’s not like I’m glad or anything! O-of course, it’s natural for a retainer to praise her master…… Besides, he’s undergone great troubles about thinking on praising me in it…So I need a response even a little in order to satisfy him, or something like that……”

“Milady, your true thoughts are leaking about”

“Wha!?”

While Silva and Lilyn are having a good conversation in the rear, Nikki’s loud voice was resounded greatly again.

“U-uhmm Musun-dono, what is this roundish thing!”

Yes, this is the main dish which Musun showed, and as you see it, it was in a sphere of considerable size, twice the soccer ball apparently.

And you could verify that around it is bound by strings.

“Everyone, by all means please look at it without averting your eyes”

As Musun said so to gather everyone’s eyes, she was holding a pair of scissors on her right hand.

Next, she cut the string around the sphere with a snap. Then,

Suddenly, a cut runs to the sphere lengthwise, and hot steam gushes out from that rift. Furthermore, it spread over the platter with ten equal parts as if it’s a flower in full bloom.

Though they’re unable to see ahead because of the steam, an aroma that terrifically stimulates the appetite fills in the surroundings. As the steam gradually fades away, the total picture of the dish becomes clear.

From that, red liquid which ooze out from the cut portion like melted cheese overflowed.

“oooh～ What a nice smell!”

“mmm～ This smell, I like it!”

Nikki and Mikadzuki’s noses twitch as they sniff through the aroma while having their eyes closed, dyeing their cheeks red. Lilyn and Silva also, captivated by the dish, stared hard.

“Everyone, this is the Cheese Chili Meat Pizza – Scorched and Ball-Fried in Bean Curd ”

「皆さま、これは《チーズチリミートピザ・おこげボール揚げ》になります」

Pizza? This one? No, it is certainly cut above and divided, ridden with various seafood, and if the red liquid was the cheese, then it was completely a pizza in appearance.

However, I haven’t seen such pizza as weird as this. Just as this was in a spherical form a while ago, where did that smell that seemed to be a burning rice coming from?

Becoming aware of my doubts, Musun moves her mouth without removing her smile.

“Please let me explain. First, was putting the food on the dough once. I cooked it in an oven like a common pizza. In which was also baked, I made it like a sphere like the one you have seen earlier, and applied a protective coating with rice which I cooked in turn. After having tied it up with a string so that the form won’t collapse from the top, I tossed it into oil heated in 180 degrees Centigrade. Well, before rounding it, I inserted the red ingredient called Chili Meat Cheese inside to secure it all around”

I see, I See, Everybody nodded in response many times.

“As for this , while being fried in medium heat, it begins to gradually melt inside, and spreads to the dough evenly. It is a cooking style to fully contain the flavor inside. Please, have some”

Each one pick up a slice of the pizza carefully so as not to burn themselves.

“Ahm……Nomnomnom……Thi-This is……!?”

But they couldn’t stop their hands anymore. Instead of savoring it slowly and carefully, they gobbled it in their mouths with surprising speed.

(What is this perfectly balanced pizza!?)

Moreover, the texture is very pleasant. The crust was tenderly done, the wrappings made out of rice, because it was fried, created a crunchy texture different from a burnt rice. It was a delicacy which integrated the perfect harmony of hardness and softness.

Also, the seafood appearing on top of it are delicious. The tender prawns, the oysters giving out milk-like extracts, and the seaweeds resembling Wakame (sea mustard) that have also spread about. Apparently, it is typically classified as a seafood pizza.

And this so-called red cheese. The cause that unified everything. Though I was prepared for the considerable spiciness from that color, it is really just harsh to. The taste of good quality meat that is included in the cheese, impresses you so many times with just one slice.

Mikadzuki eating it with great relish is a proof of that. Also this cheese, it also matches both the bread and the scorched part exquisitely. Naturally,

(This pizza is the best!)

That impression, was what everyone who ate the pizza spoke of tonight.

Nikki and Mikadzuki eat it whole-heartedly, and even requested another serving. Shamoe also stuffs it in her mouth with great relish while heartwarmingly staring at the two. But, if you focus on her left and right hand as it moves subtly, she’s writing on something which seems to be a notepad.

Probably because she wants to replicate this taste too, she’s writing down the recipe and the ingredients.

Camus also, with gleaming eyes was impressed by the pizza he ate for the first time.

“nohuohuohuohuohuo! This is so delicious that my cheeks would fall! nohuohuohuohuohuo!”

“Yeah. This is not bad. This goes well with wine”

With a glass of wine in one hand, two people nearby drank in satisfaction before everyone is aware.

(Yes, this is undeniably the best)

Hiiro also continued moving his mouth till it’s fully satisfied. 