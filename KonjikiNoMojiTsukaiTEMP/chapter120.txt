Chapter 120: The Protagonist the Relieves His Stress

Truthfully, Eveam could not completely believe what Hiiro had said. However, she was aware that he definitely had something to do with their disappearance. In addition, she, at the very least could not feel their magic power nearby, thus making the credibility of Hiiro’s words rise exponentially.

“Hi-Hiiro…….did you really…….?” (Eveam)

As she still couldn’t fully believe it, her eyes opened wide as she asked him this. However, Hiiro scowled at her, almost as if he was expressing that he had reached the limits of his patience.

“This is the last time I’ll say it. If you don’t have anything to say, I’m gonna send you back, no questions asked.” (Hiiro)
“Ah, wa-wait a minute! Kiria! Come with………..” (Eveam)

Eveam wanted to tell Kiria to return home with her, however, she shivered upon seeing her inhuman eyes. At the same time, the terror she felt upon realizing that the Kiria she knew no longer existed floated into her mind.

(Then just what in the world was the time we’ve spent together up until now……..Kiria)

Despite Eveam’s thoughts, Kiria had already started to move. With tremendous velocity, she closed in on Eveam, and similar to before, attempted to pierce through her chest. However,

Bashiiiiiii!

The first one to be astonished was Kiria. Kiria flew back as though repelled by something.

“Sorry, but I can’t let you kill this guy” (Hiiro)

Hiiro spoke thus as the word [Safeguard]/ 『防御』shined on the back of his hand. Although this was a previously installed word, to those who were unaware of that, they once again misunderstood that he had used Light magic upon seeing the sparkling wall of light.

“It defended against my attack? Just what in the world is that magic?” (Kiria)

Kiria indifferently spouted out her words.

“I don’t have any obligation to answer, do I? Do your utmost to worry about what exactly occurred.” (Hiiro)

It seemed that after averting his eyes in the exchange earlier, he held a small grudge over having felt her taking a point off of him. Eveam gazed at the protective wall with amazement, yet, after realizing that she would be safe there, returned her expression to normal as she sharply looked at Kiria.

“Kiria……I still don’t fully understand it, but I will surpass myself as the Maou! And then one day, I’ll open your eyes!” (Eveam)
“……..haa, I’m already awake though?” (Kiria)

Eveam’s words were made out to be pushing goodwill onto Kiria. As Eveam ground her teeth in frustration, she turned and sharply glanced at Rudolf.

“King of Victorias” (Eveam)

As to be expected of a king, even though many unforeseen incidents had occurred, Rudolf maintained his dignity and looked back at her silently.

“Allow me to say one thing. I………” (Eveam)
“……………..” (Rudolf)
“I won’t give up! Because-” (Eveam)

Pishun!

In an instant, her figure disappeared. Behind that was a highly irritated Hiiro.

“You talk too long” (Hiiro)

After he had simply lowered the curtains on Eveam’s stage, Hiiro-

“Ah, now that I think of it, are you fine?” (Hiiro)

-called out to Judom. Although he was a human, after seeing him cover for Eveam, Hiiro determined that he was an ally.

This was the real reason why he had left Eveam here without sending her flying off. However, since she was talking for so long, he got irritated and ended up sending her off. That was why he had no choice but to ask the person in question whether or not he would go to the【Demon Country】 with him. It was just because he happened to be going there already, so whatever was fine.

“No, according to the conversation, you’re returning to the Demon World right? I have business that I need do over here, so I’m fine” (Judom)
“I see, then I’ll leave you here.” (Hiiro)
“Ah, wait a sec. ………give this to Maou-chan for me.” (Judom)

As Judom said that, he gave a single sheet of paper over. It was a paper with Teckil’s words written on it. Hiiro silently received it and put it into his breast pocket.

“Naa oi, what’s your name?” (Judom)
“If you want to know, you should ask that stupid king over there.” (Hiiro)
“Ask Rudolf?” (Judom)

As Hiiro said that and looked at Rudolf, Rudolf simply frowned as if he were trying to express, “Just who is he?”

(Ah, I see, I look like one of the 『Imp race』right now.) (Hiiro)

As he thought that, he was thinking of ignoring Judom and disappearing like that, but-

(Ah, now that I think of it, I had something to tell him) (Hiiro)

As he turned his body toward Rudolf,

“Hey King.” (Hiiro)
“…….?” (Rudolf)
“You used the heroes as sacrificial pieces right?” (Hiiro)
“……………..” (Rudolf)
“Well, I don’t really care about that anyhow.” (Hiiro)

Hiiro seemed to hear the surrounds retort so it’s okay??

“At that time, when I was first summoned, I was still a novice, so until I got stronger I travelled while hiding myself.” (Hiiro)
“………summoned, you say?” (Rudolf)

Rudolf’s eyebrows twitched and rose. Seeing that, Hiiro’s face loosened slightly.

“But now things are different. I’ve gained enough experience so that it’s okay even if I’m exposed and I stand out.” (Hiiro)
“Summoned…….that attitude………could it be you are………!?” (Rudolf)

Gradually, Rudolf’s face began to warp in shock.

“Now I can say it. I’m thankful to you for summoning me to this 【Edea】” (Hiiro)
“…………….” (Rudolf)
“I probably won’t ever see you again, so I figured I should at least give you my thanks” (Hiiro)
“You……….I see, you were the one summoned together with the Heroes.” (Rudolf)
“That’s right, the Innocent Bystander.” (Hiiro)

The king’s astonished face was so amusing that Hiiro chuckled. But then, Rudolf appeared to have thought of something as he shook his head in order to clear his thoughts.

“Hmph, don’t say stupid things. You are an 『Evila』, are you not! The ones summoned at that time were……..ah!?” (Rudolf)

At that moment, Hiiro’s face returned from an 『Imp』back to normal. Of course, it returned to normal because he had used the word [Origin]/ 『元』.

“Did they have a face……..like this?” (Hiiro)

At that, everyone in the area was surprised. Teleportation and healing magic, as well as the wall of light. On top of that, transformation magic. Hiiro’s magic was simply so mysterious that the scene involuntarily became silent, as though time had stopped.

“Ahh~ That was a little refreshing. Because of my idiot disciple’s stupid antics, and that Maou’s long talk, I was irritated. But now, I’m a little refreshed.” (Hiiro)

It seems that he was enjoying everyone’s bewilderedness in order to relieve all of his stress. Yet, the Hiiro from half a year ago would undoubtedly not have done something like this.

(Hm~ Could this be due to the influence of Aka-Loli……..?)

That’s right, deriving enjoyment out of making fun of others was something that his travelling companion, Liliyn, had practically patented. However, after spending a long time with her, Hiiro felt that he had been slightly influenced by her.

(No, I should restrain myself a bit…….)

As he didn’t want to become like Liliyn, he reflected upon his actions. On the other hand, as he felt strangely gratified and his mood lightened, he felt that it was good that he did it. Hiiro once more used the word [Change] / 『化』to return to his 『Imp Race』form.

“Now then, I guess I’ll be going now.” (Hiiro)
“Wait, you youngster!” (???)
“Ahh?” (Hiiro)

The one who had jumped high into the air before Hiiro had realized it, was Leowald. He was gathering power into both fists. It was the same appearance he took when using the technique he had released earlier.

“Where did you send the Maou!” (Leowald)
“………..find her yourself.” (Hiiro)

As Hiiro simply spoke thus, he quickly faced downwards.

“Wha! Then I’ll just ask that body of yourrrsssss! Take this! 《Maximum Blaze Fang Attack》!” (Leowald)

Similar to before, a bright red fang with a tremendous amount of destructive force came crashing down. It collided with the protective wall Hiiro created.

Boooooooooooooooooooom!

Screeeeeeeeeech!

The sound of impact had a clash of magic power against magic power, followed by the roar of their attacks colliding against each other violently.

“………hou, as expected of the Beast King” (Hiiro)

Having confirmed the title ‘Beast King’ in his 《Status》 earlier, Hiiro understood that this was the king of the 【Beast Kingdom: Passion】. He had also heard through rumors that the strength of this king was overwhelming.

After feeling that the wall he created might lose to Leowald’s power, Hiiro let out a voice of admiration towards Leowald’s physical strength.

“But it’s too bad.” (Hiiro)

Bashiiiiiiiiiiiiiiiiiiiiin!

“Guhaaaaaaaaa!?” (Leowald)

Just as he thought the wall let out a dazzling light, Leowald felt the area of the wall he directed his power at return something back towards him.

[Reflection]/ 『反射』

It was the effect of a word that Hiiro had newly written. That word was capable of, just once, repelling anything. It was a word with exceedingly cheat-like effects.

Like that, Leowald was sent flying and rolled about on the ground. To him, Hiiro said just one thing,

“This just shows that our levels are different. Train some more, Beast King. See ya” (Hiiro)

Pishun!

This time, Hiiro disappeared from the scene.

“N-no way……for father’s 《Binding》to be so easily……..” (Leglos)

Leowald’s first prince, Leglos, was surprised at the mysterious boy who had so simply reflected the attack of his father, who was far stronger than himself. Naturally, he thought that his father would soon become consumed with rage and rampage about, and swallowed nervously as he gazed at Leowald.

However, his expectations were completely betrayed.

“Gahahahhahahaahahahahaaha!” (Leowald)

Leowald began laughing as if he was enjoying himself.

“Fa-father…..?” (Leglos)

Seeing Leowald’s appearance, he involuntarily went speechless and stiffened. Utterly ignorant of his son’s thoughts, Leowald slapped his knee multiple times.

“Iya~ He really got me there! What’s with that youngster! To take me for a fool like that, what a pleasant youngster! Gahahahahaha!” (Leowald)

Worried that he had hit his head or something, Leglos approached, but-

“Oi, did you see that Leglos? That Red-robed youngster.” (Leowald)
“Eh, ah, yes. Mo-more importantly, are you alright, father?” (Leglos)
“Of course! At this level, I was surprised, but didn’t receive any damage! Iya, but what an interesting youngster! I, by all means, would love to face him with my full power next time! Gahahahaha!” (Leowald)
As Leglos watched his father laugh heartily, he inclined his head in indignation as he remembered something.

“The Maou got away, but there was an interesting encounter! Right now, let’s be glad about that! And also, the Maou should have returned to the Demon world! Let’s head there immediately Leglos!” (Leowald)
“Ye-yes!” (Leglos)
“Gahaha! I hope that youngster is there as well! Interesting! Truly interesting!” (Leowald)

 