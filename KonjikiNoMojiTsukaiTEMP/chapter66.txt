Chapter 66: The Chaotic Trio

Hiiro, who had passed by the guest room, looked back at the path that his journey had taken him through so far.

After he had hiked over the mountain, a forest had appeared before him. Speeding through such a forest, he was met with a small lake that spread itself across the horizon. In the center of the lake stood a lone island.

The lake was so peculiar, that it wouldn't be strange to expect to see a donut shape if one were to view the lake from the sky. In other words, the island played the perfect role as the donut hole. The the lake water had a shade of deep crimson as the small floating island exuded an overwhelming presence.

The reason for this was due to what was on top of the island. Perhaps in an attempt to highlight the island’s presence, a fairly large mansion had been erected upon it. Similar to the previously seen 《King’s Tree》, a garden was spread, surrounding the mansion. It held gorgeous flowers and raised plantation that secreted a pleasant aroma.

Silva stated that that mansion was the one he served in. As a boat had been prepared nearby, they had decided to ride on it. It seemed that as this boat was facing the Mountain of Poison, it could be determined that Silva had used this when crossing the lake.

As they got on the boat, while leisurely making their way to the island, Hiiro was informed about the background of the mansion.

There seemed to be three individuals that lived in this house, including Silva. There was the Master, the Maid, and the Butler. This meant that other than these three, there were no other persons on the premises.

It seemed that the garden that was visible from the boat was created for the sake of the maid’s hobby. In addition, Hiiro was told that as the Master was very whimsical, her personality was very difficult to deal with.

Certainly, one would not normally send their servant off towards the toxic【Venom Mountain】. Was this an indication of the extent that the master trusted Silva, or...

As Hiiro thought thus, he arrived on the island. Mikazuki looked at Hiiro as if asking him what she should do. As Silva was obviously unable to grant her entry into the mansion, it was decided that she would be kept in a feeding ground for livestock. As it was fairly close, Hiiro waited while Silva led Mikazuki towards the feeding grounds.

Upon looking at it up close, the mansion’s size seemed to have increased. It was without a doubt an establishment that emanated wealth. Yet as the only residents of the mansion were only three people, Hiiro certainly thought it was peculiar. As Hiiro was not particularly interested, however, he didn't bother to ask about it.
As Hiiro exited the garden and approached the mansion entrance, he encountered a lone girl, cleaning as she held a broom in hand.

Noticing their presence, the maid widened her eyes with all her might as she startlingly shrieked Silva’s name. Following this, she kept weeping 「Thank goodness. Thank the Heavens.」(1) with watery eyes. She then redirected her gaze towards Hiiro.

Upon realizing that she was completely visible to Hiiro, she winced for whatever reason as her body trembled in fright. As he had determined that he was not very welcomed, Hiiro approached as he wore a wry smile.

When Silva explained as to who Hiiro was, the frightened atmosphere surrounding her slightly subsided. However, her eyes immediately widened as she stuttered「W-w-w-w-we need to report this!」, following which she opened the door with tremendous speed before entering the mansion. In the instant that she tried to enter the mansion, however, she flashily tumbled to the floor with a *doka*(2). As there was nothing that one would be able to stumble on within the vicinity, those who would bear witness to such a scene would feel exasperated.

As Silva looked over her situation while wearing a seemingly pleasant smile-


Silva: 「Iya~(3) As expected, a young lady is pleasant, don’t you agree~? That *purunpurun*(4) is absolutely irresistible~ Nofofofofo~(5)!」


Let us retract the previous statement(6). If one were to inspect closely, Silva’s cheeks were slightly flushed and his nose was slightly extended. In addition, there was a dangerous light lurking in the depths of his eyes. Seeing this, Hiiro involuntarily backed away.


Silva: 「Now then, shall we proceed inside.」


Even though he said this, Hiiro began to seriously consider whether it would be fine to follow after such a pervert. However, as he had come all the way here, he considered that it would be fine to enter as long as he remained vigilant of the pervert.


Although the inside was considerably dim, tall vases, paintings and other ornaments decorated the surroundings. The cleaning seemed to be very meticulous as there was barely any dust and the decorations were kept in very good condition. Although this was probably the maid’s work, Hiiro was genuinely impressed as cleaning such a large mansion with such attentive detail was no easy feat.

 As Hiiro passed the guest room, he was told to wait there. Thus, he did as he was told as he placed himself on a sofa.


Hiiro: (Putting that all aside, this is a pretty huge mansion, huh?)


Hiiro thought as he stared at the lake visible from the window. Although it was true that a mansion surrounded by a lake may certainly sound like a romantic place, Hiiro couldn't help but wonder why they would conspicuously build a mansion on top of an island as it seemed severely inconvenient.


 Was it build due to the indulgence of wealth? Or was it built for some other reason...


 Hiiro: (Well, I don’t really give a damn. After I finish eating, I’m outta here.)


 It would be just as it was before. As he determined that he would continue his journey alone, he began to write words on his arm,


 Hiiro: (For Mikazuki, I’ll install 「Speed」|『速』 and 「Protect」|『防』. Just in case something happens, I’d better be prepared.)


The words Hiiro wrote on his arm were 「Protect」|『防』, 「Speed」|『速』, and 「Pry」|『覗』. The first two words were generally used to deal with a predicament should it arise. The third word, 「Pry」|『覗』, when activated instantly allowed Hiiro to view the target’s 《Status》. As Silva was overly sensitive to magical energy, if Hiiro wanted to use magic, he would quickly draw his attention. As such, Hiiro deemed that his abilities would be eventually revealed.


As such, if he were to set up the words in advance, he could activate magic while eliminating the need to write the words by concentrating magic in his finger. Also, it was possible to activate the magic and obtain its effects, even in a short time period.


Hiiro: (Well, there’s still the one minute time limit before I can activate it but...well, I doubt I’ll get a chance to use it.)


Putting the matter of the time limit aside, Hiiro could now wait for Silva to come before activating the words. However, Hiiro doubted that Silva would come within a minute. He also set up the words not for the purpose of using them against Silva, but for investigating his master.


As he believed that he would not be simply taken straight to the dining table, Hiiro thought that even though the other party was wealthy, it would still be preferable to have information about them than not. To that end, seeing as he could get a glimpse of their 《Status》, he was in a very advantageous situation.


Once he had made his preparations, Hiiro waited for a while before Silva returned. As his Butler Uniform that had been previously dirty was now clean, it was apparent that he had gone to change his clothes.


Silva: 「Now, please come this way, for I will introduce you to the master.」


Following Silva’s direction, Hiiro departed from the guest room. As Hiiro walked through a lengthy corridor, a pleasant aroma drifted into the passageway, tickling his nasal cavity.


*Gugyuruu~*(7)


Due to the smell, a stomach began to growl. However, the source of the sound did not come from Hiiro.


Silva: 「Iya~, I believe that my stomach seems to be quite famished~」


Hiiro: 「Bastard, you ate my food, didn't you!?」


Before they had arrived here, almost all of Hiiro’s food had now been supposedly residing in Silva’s stomach. In spite of this, the fact that Silva’s stomach was howling was truly shocking.


Silva: 「Nofofofofo! I must be ready for any situation that arises! Because I am a Butler! Nofofofofo!」


Hiiro: 「Not that again...」


As he looked carefully at the hungry, perverted butler, Hiiro could only spit out a sigh. Hiiro wanted to make a tsukkomi, saying that being a butler was utterly unrelated!.


Silva: 「Behold, we have arrived.」


As Hiiro was stopped in front of the door, Silva began to slowly open it. What appeared before him were several long desks covered in pure white table cloths, on top of which were a variety of dishes neatly arranged. The odor was so stimulating it made one involuntary swallow salivation.
At the end of the long tables was someone sitting in a chair.


??? 「I welcome you, unusual visitor.」





She possessed long hair that seemed to look like flames as it maintained a hue of fiery crimson. As she raised her strong, assertive eyes towards Hiiro's direction, her lovely little lips distorted into the shape of a crescent moon. Her pure white, Gothic Lolita style dress matched her red hair splendidly. 

 Although being watched by evaluating eyes was unpleasant, there was something else that caught Hiiro's attention.


Hiiro: (This person couldn't be...?)


As he thought thus, Hiiro glanced at Silva, to which Silva responded with a small nod.


Silva: 「I shall proceed with the introductions. This is the master of this mansion. Liliyn Li Reysis Redrose-sama.」


Hearing Silva’s introduction, Hiiro carefully looked at Liliyn.


Hiiro: (As I thought...... but even so............)


He once again examined Liliyn. He lightly exhaled.


Hiiro: (......isn't she just a brat?)


Indeed, her appearance was indisputably a child. She was very much similar to Muir. Well, as Muir was too young, perhaps the comparison would apply better to Mimiru.


What was before him could only be described as a girl around the age of ten. However, Hiiro knew that Silva would not tell such a pointless lie. As such, the little girl that was in front of his eyes was really the master of this mansion, the person Silva served.


Hiiro: (So this kid was the perpetrator responsible for sending Silva to the mountain of poison?)


It was natural to doubt this suspicion, however, Silva simply pulled out a comb from his pocket and began to adjust his hair.


Silva: 「Oh dear oh dear, Nofofofofo.」


As Silva said thus, Liliyn began to pout as her face was filled with disgust. Whether or not Silva noticed her expression, he continued.


Silva: 「Aa...as usual, such a beautiful and adorable Ojou-sama(8)...」


As he wondered what the hell Silva was saying, Hiiro merely looked at him.


Silva: 「No, I am mistaken. Compared to before, you shine as if a you were a bewitching moon upon a mystical dark night. Your charm that knows not of tranquility. I...I...」


Seeing this, Liliyn simply shook her head in grief.


Silva: 「I AM INDESCRIBABLY OVERJOOOOOOOOOOOOOOOOOOOOOOOYED!!!!」


As Silva’s body was trembling, he abruptly leaped towards Liliyn. Following this......


Silva: 「Ojjoussamaaaaaaaaa~~~~~n-!!!!」


While enthusiastically pouting his lips, Silva flew through the air with his arms spread wide. Although Hiiro had seen many of Silva’s eccentricities, even he was stunned, left speechless by the current situation.


*Dogo!* *Baki!* *Bako!* *Bogo~n!*(9)


After a few moments had passed, Silva’s head was plunged into the floor. The figure that had trampled on his back with a single foot was Liliyn. The maid that was close to the two began to panic as she unconsciously kept spouting 「Awa awa(10)」.


Liliyn: 「Unbelievable, this perverted Jiji(11) bastard! Why the hell doesn't he just die!?」


Although it seemed that the little girl had spoken something outrageous, a muffled voice could be heard from under the floor.


Silva: 「This….this is….what they call...love….」


*Bakin!*(12)


Silva: 「Bufo-!(13)」


It seemed that the girl had apparently dealt the finishing blow to the pervert. As the body was trampled by her foot, it made an unpleasant sound. Ever since, the perverted Jiji was remained silent.
After dusting herself off with a *ponpon*(14), she placed herself down onto the seat that she had sat down in previously.


Liliyn: 「Well, I have exterminated the creepy insect. Shamoe, introduction.」


Liliyn continued the self-introductions as if nothing had just occurred. The provoked maid let out a 「Feh!?(15)」as she hurriedly lowered her head.


Shamoe: 「A-awawawawawawa! S-Shamoe is, well-! Um, Shamoe is a maid so I do the cleaning, the cooking, and gardening as a hobby for a maid, i-i-i-i-i-i-f you were to put it simply, then I am just an average maid! 」

Hiiro(16): 「...you said that you were a maid three times in your introduction though?」


Shamoe: 「Fe-!? I-I-I-I've failed again!」


Her face flushed a bright scarlet. And then-


*Gongongongongongongongon!*(17)


She began hammering her forehead against the wall.


Shamoe: 「Baka baka baka baka-!(18) Shamoe is a big baka! Even though I decided that I would do it correctly~~!」


Hiiro began to reassess the situation. Buried under the floor was the head of a perverted butler. Near the wall was a maid that suddenly panicked, slamming her head repeatedly in tears. Lastly, the one who was grinning while overlooking the maid’s antics, a little girl


 Hiiro: (This is undoubtedly chaos...)


Hiiro began to suspect that he was largely mistaken in coming to this mansion.

 