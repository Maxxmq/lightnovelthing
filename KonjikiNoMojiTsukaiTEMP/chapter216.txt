Chapter 216 – Spirit Forest

Through the guidance of the snake, they discovered a beautiful waterfront after passing through the forest. An altar-like structure was built at the center, with a big white lump visible on top.

(What is that……?)
He stared, focusing his eyes but can’t recognize anything from his distance.

“Follow me”

The snake says so, crawling on top of a lotus-like leaf floating on the water connected towards the altar.

“I-it won’t sink, right?”

He understood Nikki’s worries. It seems okay because the snake is lightweight in the first place, but she doesn’t think that it can support a weight of a human person.

The monkey, realizing it in her expression,

“Don’t worry. It won’t sink”

But to Hiiro who is being excessively cautious, he tries to press his hand to the bottom. Certainly, it doesn’t sink. Though a ripple spreads out because it is floating above the water, it felt like the top of a slightly soft soil.

Still, the three people advanced cautiously till they arrived at the altar. Then he was able to recognize the identity of the white lump clearly in his vision.

“Hohoho, we have waited for you, Omnipotent One”

(TN Note: 《万能の者》is in the raws. Can also be translated as all-around/all-purpose/utility/almighty person, according to jisho.org. I chose to that one because it’s cooler)

It was an enormous white snake. With its massive body wound up in a coil, you can think of it as a lump from a distance.

“Whoa～ Soo big-desuzoo～”

“……huge”

Nikki and Camus voiced out in reflex. It is really big, that’s for sure. If it opens his mouth supposedly, it can fit up to three people all at the same time.

“You did god coming here. Are these your companions, Omnipotent One?”

“Yes they are, grandfather”

“Hohoho, I see, I see”

Though the corners of its mouth stretched in joy as it raised a laugh, it only resembled a snake finding his prey, gloating in itself. Nikki is really frightened, you know?

“Oi”

because he can’t sit still forever,

“Lead me to the reception dishes quickly”

He quickly pointed his demand. As expected, the monkey and even the snake’s jaws widely opened and froze in his speech and behavior. However, the huge snake only laughed about it.

“Hohoho, aren’t you the impatient one. We have prepared it properly. Look over there”

When he focused his attention to a certain place, on the left side of the exit of the forest, a big table was prepared, with different kinds of dishes laid on top.

Then, without any foreboding the body of the white snake began to glow. Though the light was so intense it makes you grimace, it calmed down at once. However, as the light goes smaller, it gradually took a certain person’s shape.

When the light disappeared completely, the huge snake in the altar earlier vanished, leaving only an old man full of white hair and a white beard in its place.

“I shall introduce myself first. I am called Hoozuki. Best regards from now on”

He smiled, rubbing his long beard. Though Nikki and Camus are blinking in surprise in the present situation, Hiiro who was quietly observing moved his eyes and asked.

“………So? Are you the Spirit King?”

The person in question nods in response.

“Oh, very old indeed. Clearly different from the queen of the Fairies”

“Hohoho, even though it’s like hitting a whip on an old man’s body, I can still work out. Plus, the present [Fairy Queen] who is the same age as this princess here are still young”
“……princess?”
“It’s me”

It was the small white snake that guided him here. But for Niña and this princess to be at the same age……though you may not think of this in particular, if anything, it also has a kind of dignified presence the same as her.

Even though she looks like an ordinary small animal. Also, her tone sounds sassy.

“……is there something you want to say?”

“……who knows”

Does her intuition become sharper in her animal form? She glared in scorn, but he smoothly warded it aside.

“Rather than that, let’s skip the self-introduction and get on with the food already. The talk comes after that. Good?”
“Hohoho, it can’t be helped then”
On the table which was seen from the distance a while ago which he didn’t even notice till now, slime-like creatures of different colors appeared.

“What are they?”
“They are spirits too, you know?”

The princess pointed out, but

“These guys……spirits?”

In every angle only slimes could be seen. Should it be called handmade slimes? It won’t be even strange if you came across them as giveaways of a crane game, as there are a lot of them, all in different colors.

“These children are lumps of power that nature produced, so to say they are “Newborn Spirits” which are not yet awake, ne”

“This princess, could you stop with that manner of speaking as if you’re a companion?”
“Ya～i, he’s mad at the princess yanno～”

The monkey who got scolded by Hoozuki and the princess who made an embarrassed face when rebuked felt interesting. But then the princess’ eyes flashed in a moment,

“What? You want to pick a fight? You want to die?”
“Eh……ah……w-well……no, nothing, please don’t mind me……”

Just how weak is that monkey…… it’s like a husband found having an affair and could say nothing at all.

“Now now, the two of you, make up. You’re in front of our guest, you know?”
“……understood”

As he say so, she averts her eyes from the monkey, which then felt relieved to be able to escape in the situation.

“So……Tenn?”

“Eh? What is it?”

Apparently, Tenn seems to be the monkey’s name.

“I saw your impolite behavior to our guest in the altar, so you’ll receive punishment for it later, get it?”

“Wha,wawawawawawa!?”

Hoozuki smiled sweetly, in contrast to Tenn who turned pale and shiver in despair. Even though he looks like an old man, he’s still a king after all. He’s smiling, but you can feel the pressure through that smile. Tenn looks like he’s about to pee in any moment……nah, he’s already fainted.

Seeming to be happy about it, the princess broke into a smirk. Weren’t you close and supposed to help Tenn?

(Nay, it is said that you get along well if you quarrel often……)

Then after everyone arrived at the table, Hoozuki began to speak.

“We wish to express our gratitude for coming to such place. Allow me, in behalf of all the spirits to give our thanks”
“Grandfather”
“Hm? What is it, princess?”

“You aren’t heard……that”
“Oh?”

Before the eyes of the two, as if they aren’t interested in his speech, Hiiro was already staring hard at the dish in front of him.

“Oi, can I eat already?”

“Hohoho, then we’ll have a meal first, shall we?”

The moment they got permission, their eyes scanned the diversity of dishes placed on leaves serving as plates.

“oooh～ good smell nanodesuzoo～！”

“……looks tasty”

Unable to decide which to choose first,drools cover Camus and Nikki lips. Hiiro who was also the same catches sight in one plate.

There, was a meat with a bone shaped like those you see in mangas, but this one emits a rainbow color. Meat juices drip down as if it’s melting. Moreover, the meat juices shine in rainbow colors, too.

(TN Note: Will you poop or fart rainbows too? You know, when the meat has been processed in your body?)

“That is the Rainbow Meat. But I’ll say this first, it falls to the class of being a dessert”

Dessert? This is the first time a meat is classified as a dessert.

Though this Rainbow Meat is called meat, it is heard that it possesses the sweetness and acidity of a fruit. For the bone, truthfully speaking, it’s not a bone, but a hardened candy called White Honey.

Certainly, that would enter the class of being a dessert. However, since it caught his eyes and was curious about the taste, he was seized by the impulse of want to try it first.

“I’ll take the Rainbow Meat desuzoo～”
“……me too”

Are the two also curious about the taste? They ordered the same too.

Certainly, the texture isn’t like meat, if anything, it’s like a Castella. Then, for the first time, they noticed a sweet lump-like filling on the inside. The filling which is also of rainbow color oozed out and wrapped the Castella over.

(TN Note: Castella is a Japanese sponge cake)

“This is a confectionery, that’s for sure!”

He thought that the sweet syrup oozing out of the Castella is a confectionary, but the thick luscious sweetness, even to that degree, has carried the highest degree of elegance in it.

With this, they desired a hot tea. On the table, a teacup also made from leaves is placed, with a light brown liquid in it.

It has an aroma of a black tea. They took a sip, and because it is in the proper temperature not to scald themselves, it entered their throats quickly.

“…………Haa～”

They sighed in ecstasy. The taste is like tea, and can be classified as such from the Lemon Tea family. Its compatibility with the Rainbow Meat is too high.

“I can’t stand it anymore desuzoo～！”

“*munch**munch**munch**munch*”

Nikki and Camus who have seemed to like it stuffed them in their mouths, savoring it in the way.

In addition, various service dishes of the spirits are tasted too. There should have been a considerable amount in the table, but all have disappeared in approximately 30 minutes.

“Th-that many dishes only in a flash……what kinds of stomachs do these children have……”

The princess who should have been calm and composed, can only be stunned by the scene before her eyes.

“Especially that glasses guy, to eat that much……”

Looking at Hiiro who felt like he’s in heaven with his swollen stomach, even Tenn is astonished.

“Hohoho, it seems it was worth it to prepare those dishes seeing your satisfied faces”

As expected of Hoozuki, he was only brought into a smile. Afterwards, after having received tea once again, Hoozuki who thought that the issue could be advanced finally begins to talk.

“Have you had your fill?”

“Yeah. I’m satisfied”

Upon telling his honest feelings, Hozkuhi returns with a nod. However, Hiiro squints his eyes for a moment,

“And? What business do you want in me?”

And decided to get down to the main subject. He didn’t think of this as simply a party just to let them have and enjoy their cooking as it is. Obviously, they have intentions. That’s what he thought, on order to return as soon as possible. 