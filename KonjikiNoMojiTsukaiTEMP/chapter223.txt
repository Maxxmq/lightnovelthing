Chapter 223 – Obstinate Snake Princess

When Hiiro is about to fall, Tenn supported him with his right hand, the barrier has been dispelled, and Nikki and Camus came from their rear with an incredible momentum.

“Ma-Masterr!”

Tenn who leaked a light sigh laid down Hiiro in that place, and Nikki whose face looks mortified directs her gaze to Tenn in vexation.

“Don’t worry. It’s his win”
“……Eh”

Then, the Tenns disappeared one by one, leaving only one in front of Nikki right now.

“That guy did searched for me splendidly, therefore……”
“Hiiro……won?”

With a deadpan expression, Camus spoke to Tenn which was then answered with a nod,

“Yeah, congratulations”
“Uoooo! Thank goodness desuzoo!”

Nikki clung to Hiiro who was lying down. Then Hoozuki comes over.

“Hohoho, to surpass a trial by that kind of method, really”

Despite his disheveled face, Hoozuki smiled in bliss, while the princess seemingly annoyed by this faced Hiiro.

(What’s with this kid? For him to break through the trial by that method, it’s too reckless!)
With feelings unable to understand at all, she observed Hiiro who splendidly passed the trial.

“Hmm, he has the backbone,  he has the ability too, and above all, he is well liked by the Spirits. Hmm, I knew it, he’s perfect for the princess as a bride……”

……crack

“Grandfather……aren’t you getting a little too far?”
“Ii-it’s only a joke, princess…… so please don’t release your bloodlust to me…”

The princess went to Hoozuki’s back before everyone is aware, with her sharp fingernail slightly touching his neck. Hoozuki turned pale while letting out cold sweat from his entire body.

“These two, what are you doing……”

Tenn shrugs his shoulders in amazement, then releasing a white smoke, turning back again into a monkey.

“Kikii, I found an interesting fellow!”

He put up the both ends of his mouth to a smirk while looking at Hiiro’s face. Seeing Tenn’s joyful smile, for some reason a painful feeling passes through the princess’ chest.

Seeing the princess’ facial expression,

“So, princess, what about him? Forming a contract that is……”

(TN Note: Yes, the contract he’s speaking of has another meaning, the one that signifies the end of bachelorhood of a man)

But the princess surprisingly makes a big step back.

“Wha-what are you talking about, Grandfather! I-I don’t have any intentions of making a contract throughout my entire life! A worthless creature of a human even more!”
“Hmm, is that so?”
“O-of course. Th-though I admit that I recognize this kid’s perseverance even a little……he’s still a human after all, although it’s unbelievable, and isn’t that already decided he’s going to be Tenn’s contractor?”

Though she kept on fast-talking her excuses, Hoozuki’s grinning didn’t stop.

“It’s only a joke. The candidate for the contract will surely be this child. However, the contract is all about compatibility too. So if Tenn is no use, then how about you, princess? Well, if that recoil won’t affect his body when he fail that is”

Certainly, when failing a contract, the risk is high. Though Tenn consented in making the contract for the time being, it doesn’t mean that the contract itself will succeed. The worst case is he may die if it fail.

“You’re saying that for your own convenience! I am not an alternative!”

 

“But, your face earlier shows like you envy Tenn, is it not?”

Then, the princess’ cheeks reddened, and turned her face away for it not to be seen by anyone.

“I-I-I-I-I haven’t done such a thing! Really, how rude!”
“Hohoho, is that so? Well then……”

Hoozuki slowly turned his glance to Hiiro.

“Certainly he’s not destined to be the one to make a contract with the princess. I think that Tenn fits him better”
“………”

Hearing the words of Hoozuki in the back, even though she thought that it’s the proper thing, there is still some prickling sensation in her chest. That’s from Tenn’s joyful expression from a while ago.

In actuality, because the spirits comprise of a single substance, there are no troubles in their living. But their power is restricted considerably in the outside world, so they can’t stay outside for a long period of time. That’s a common knowledge between spirits.

However, if there is a contractor, it is possible to share their existence together, and because they act together, they can move however they want in the outside world.

Above all, if contracted, the spirit could now show their true powers on the world outside, and moreover the magic power supplied from the contracting party gives off a sweet pleasant sensation.

Of course, the princess hasn’t been in a contract yet, and because of that, she doesn’t know what that sensation is. But according to the other spirits who have contractors, she heard that they were able to taste a similar feeling of ecstasy.

(TN Note: Okay this is getting a little too overboard. Please, stop the contract, and MAKE HIM YOURS ALREADY FOR OUR SAKE! Well, as long as they act like they’re partners-in-crime, I don’t mind another Derflinger though)

But they say that the sensation of the connection which is being trusted by the contractor is the best. Being together, leads to the happiness of the spirit. Therefore the natural-born spirits are always longing for a contracting party to appear.

Every one of them desires to feel such sensation. But it is extremely rare for a contracting party to appear. Rather, it can be said that such chances were lost after the Spirit Forest was transferred to the bottom of the sea.

In the past it was an abundant and beautiful forest, and the people who got lost and encounter them were the ones who were able to make contracts with them. But that’s impossible now. That’s because there can’t be a person who would lose himself in the bottom of the sea.

That’s why even though they wanted to, she and Tenn both have given up. However, on a certain day she heard a report from Niña in Fairy Garden about a certain boy from Hoozuki. As for the boy, although he is a human being, he seems to be well-liked by fairies.

Tenn who heard that story said that he wanted to see the boy with his very own eyes, and asked Hoozuki for the permission to search for him. He failed several times, but was able to finally meet at last.

Apparently, it can be seen on Tenn’s face that he seems to have taken interest to the boy somehow. And although Hoozuki brought the topic about contracts made him surprised, he was happy.

(TN Note: So Tenn is somewhat like a lost chick. A lost monkey chick.. Urgh. Not cute. not cute at all)

However, for an unknown reason whether it is a High Ranking Spirit’s pride or not, he decided to put him to a test. In actuality, they can make a contract immediately without actually doing such things but Tenn was unexpectedly obstinate about this.

 

Therefore in this trial, perhaps the reason why Tenn picked up an unreasonable demand is to test the fighting spirit of the boy. He wanted to ascertain whether his heart won’t be shaken in doing an impossible thing.

This is because the strength of the heart is the most needed requirement for the contract. But contrary to their expectations, the boy broke through the trial using a method that they could never think of.

It’s not just Tenn, he also stunned everyone who were in the place. And that unforeseen expectations made Tenn happy. He might also have felt it, too. That boy is surely a deserving contractor.

That’s why he made a face full of joy. A joy in which a contractor, something that he has given up long ago was suddenly found, and his heart is probably dancing because of his contractor having a surprising strength.

Seeing Tenn’s vibrant smile, in addition to the feelings of support for him, erased her feelings of jealousy that had appeared.

Perhaps, coming across to see this miracle, her feelings of vexation came out at the same time Tenn obtained joy.

 

Because she kept herself silent she was made fun by Hoozuki, but as she was looking at the smiling Tenn,

“……be relieved, princess”
“……eh?”

Hoozuki faced her with gentle eyes.

“your destined person will surely appear someday……definitely”
“Ss-such things……I didn’t say that I need them……”

She denied it, but her voice held no power in them.

“Hohoo, but before that you should succeed my position first”
“…………”
“Surely there are no more contractors for me, but for the princess, it’s still possible. Although it isn’t a long way off”
“What was that reckless remark…… please say that you’re kidding?”
“Hohoho, your host will certainly appear. A person who will coexist with you well as a host, surely”

Looking at him brushing his beard while smiling, she leaked out a sigh. But those words of him ignited a light to her heart.

“Besides, look, maybe one of them will surprisingly become your host you know?”

He said while looking at Nikki and Camus. But she can’t see them as a proper vessel in any possible angle.

“Besides, isn’t it because of the princess’ permission that these two were able to come here?”

Certainly, they won’t be able to come here if they haven’t gotten permission, so hers allowing them is a fact.

“Well, there’s no need to worry. I’ll make sure it’s according to your expectations”

Hoozuki’s words sank into her chest gently. It will be according to her expectations. Yes. Even Tenn double-checked the boy in his own way.

If that’s the case, then I too, must not give up. Too continue dreaming about the fated contractor that will come for me someday, as Hoozuki told me.

(…………but those two people…………Nah, it’s impossible)

The first one is still a child, and the other person is a man who looks like a girl. Though she felt the two are possessing a spirit of a true warrior, compared to that boy, they gave off an atmosphere that doesn’t seem to meet her standards.

(Hm……that child, however……)

She focused her gaze on the two people while being lost in thoughts. but

(……must be my imagination)

She gave the answer and shook her head in disapproval. 