CHAPTER 150 - RESOLUTION OF THE GABRANTH

“....Uuh” (???)


“You have finally woken up Lenon?” (???)


“...A…..Aniki?” (Lenon)



Lenion was laid on a makeshift bed. Leglos smiled wryly as he saw the bandaged person speak.


“You seemed to have been beaten up pretty badly.” (Leglos)


Lenon slightly clicked his tongue as he remembered the reason for being bedridden. 


“Big Brother, how long did I pass out?” (Lenon)


“When I asked Barid, you seemed to have slept 2 whole nights.” (Leglos)


“Shit…...what a mess.” (Lenon)


“Your opponents were three people from 《Cruel》 right? But even so, wasn’t it a miracle when you managed to survive that?” (Leglos)


“Haa, I would rather die than live in disgrace, about that place…” (Lenon)


“You’re a fool!” (???)


Beast King Leowald who made that angry remark suddenly showed up in that place. 


“F-Father…” (Lenon)


“Lenon, what were you going to say that time? You would rather die than live in shame? Oh? You would rather die and grumble in disgrace in here?” (Leowald)


“....tsk.” (Lenon)


Lenon looked away seemingly embarrassed. Leowald who saw such attitude from Lenon, loudly sighed.


“Listen well Lenon, you are not strong enough yet to be able to choose the place where you will die.” (Leowald)


His gaze had a certain sharpness, including the light of relief in the depths of his eyes. He had a sense of duty to lead his son out of his wrong attitude, as he was quite relieved that his son was safe.


“The soldiers who died to defend you, you must repay those lives as well, you should become strong from this to be able to choose the place where you will die. Right now, you still have a long way to go.” (Leowald)


“........I understand.” (Lenon)


Lelgos, the elder brother who saw the blunt response shrugged his shoulders in amazement.


“Hm, by the way father, you got here quite early. Were you able to defeat the Demon Lord?” (Lenon)


“No, an unexpected hindrance turned up.” (Leowald) 


“A hindrance?” (Lenon)


Leglos told Lenon what happened at the conference.


“Such a thing happened huh? Who was that red-robe?” (Lenon)


The red-robe was Hiiro.


“I have not understood the details. What I do know is that person is a user of light magic while being an 『Evila』...that person does not seem to be an ordinary person.” (Leglos)


Lenon who saw the serious expression of Leglos, found this quite interesting as he was intrigued by this person.


“Furthermore, that person seems to be the central figure who obstructed our plan.” (Leglos)


When he told Lenion that this information came from the soldiers, even he cannot help but be dumbfounded.


“Hey, wait a minute, then that outrageous explosion, and the one who quickly defeated Crouch, he was that person as well?” (Lenon)


“It appears to be so.” (Leglos)


“Did we not receive any information about that existence at all? I mean, isn’t it strange for that person to not be well-known if such a non-standard existence like that is real?”


“Perhaps, that person is not the type to leisurely displays his own power.” (Leowald)


Leowald promptly answered his question.


“Rather, at least up to now, that person was avoiding actions that may stand out. He seems to have said such things to the Victoria’s King.” (Leowald)


“....Then, for what reason did that person suddenly move this time?” (Lenon)


“Who knows, though I do not understand the reason, the ability of that person is nothing but a threat.” (Leowald)


“Indeed, that person was even unharmed from Father’s attack.” (Leglos)


“D-Don’t say such foolish things Aniki! That person received Father’s attack unscratched?” (Lenon)


“It’s the truth.” (Leowald)


Lenon hardened without being able to object due to Leowald admission. He knew Leowald’s ability. Even now, he is still being treated by that power single handedly.


Neither receiving Leowald’s attack nor the talk of its flawlessness was too believable.


“Besides, another problem occurred. Rather, I believe this is the biggest problem…” (Leglos)


Leglos frowns as he had a hard time saying it.


“What happened?” (Lenon)


“......The bridge was destroyed.” (Leglos)


“.....Ha?” (Lenon)


Leglos has been at his wits end when he heard what took place two days ago while Lenon was bedridden.


“This action is too much for our situation.” (Leglos)


They were perplexed about the cause of that disturbing situation. Besides, it would not be even an exaggeration to state that this was their defeat.


“Betrayal from a beastman? What the heck is that?” (Lenon)


The bonds of the beastmen are strong. Therefore, it was unbelievable that a fellow comrade who they once trusted had betrayed them. However, what was confusing was the fact that the bridge was destroyed by some unusual power for a beastmen.


“Rather, to tell you the truth, I have an idea of that person.” (Leowald)


“.....Eh?” (Lenon)


“When I heard the characteristics of that person from Barid, though the face was certainly a beastmen, he said some things about that person which for some reason is similar to the red-robe I confronted.” (Leowald)


Physique, attitude, usage of magic, everything closely resembled the boy in red-robe.


“If that person is able to impersonate someone, that person would likely have taken the shape of a beastmen and came here. Apparently, that person seems to be able to teleport as well, due to the fact Barid lost sight of the that person instantly.” (Leowald)


“.....Who in the world is that person?” (Lenon)


kukukuku, then, they heard Leowald strange laugh, the two people who saw this stared dumbfoundedly at him.


“Isn’t he quite an interesting boy? To think he even destroyed the bridge. Moreover, he came alone right in the middle of the enemy’s territory. I want see him again by all means.” (Leowald)


The two people shrugged their shoulders in amazement as they saw the man expressing such a happy-looking smile.


“S-say, Aniki?” (Lenon)


“W-what is it?” (Leglos)


“Regarding about that person, Father seems to have been pleased with him” (Lenon)


“It looks like it. Even if I was in Father’s position, it would be my first experience. To have my attack easily reflected back at me. Moreover, flawlessly too.” (Leglos)


“Ha? He did not only prevented it but also intercepted it…. who the hell is that…” (Lenon)


Although jealousy clearly dwelled in his expression, he had similar feelings with Leglos as he also expressed a wry smile.


“However, I kind of understand Father’s feelings too. Up to now, there was not a single person who fought him directly. Therefore, though Father looked forward to being able to fight with the general Aquinas, he seemed to have found a rather more interesting toy.” (Leglos)


“.....I kind of want to give my sympathy to that person a little, becoming Father’s practice target.” (Lenon)


“I’m of the same opinion.” (Leglos)


Leglos who sees Leowald still in his own world laughing while reminiscing, let out a sigh.


A sudden thought came to him.


(If I’m not mistaken, half a year ago, Mimir’s voice has been restored by a 『Spirit』, however, I seemed to have heard that it was also wearing a red-robe…….. Don’t tell me…) (Leglos)


Although, they are evidently the same person, Leglos craned his neck as he wipes out that kind of notion.


“By the way, what shall we do from here on out? This is the territory of the 『Humas』 right? Do we have any method of collecting our colleagues in the demon world?” (Lenon)


Leowald who laughing broke his smile as he made a serious expression on Lenon’s question.


“About that, a lot of our brethren were arrested in the period of two days.” (Leowald)


“Na-!? …..nay… that is right” (Lenon)


Lenon tightens his teeth as his fist quivered. This was the result of him comfortably sleeping for two days, he was somehow convinced this would have happened.


“Those guys did not let this chance slip by. Of course, that’s natural…..but why did they arrest them? If it was me, I would exterminate them.” (Lenon)


Leowald who was a father smiled wryly on Lenon’s scary remark.


“The Demon Lord of this generation seems to possess a different disposition.” (Leowald)


“Ha?” (Lenon)


“In case of the predecessor Demon Lord, he would definitely murder all our brethren as you have said. However, the current Demon Lord is still a young lady” (Leowald)


“I know that….but…” (Lenon)


“Although I was able to observe her a little, with regards to her speech and behavior, the Demon Lord is too naive.” (Leowald)


“Therefore, she arrested them without murdering?” (Lenon)


“I fear that it’s likely they want to end this war.” (Leglos)


Leglos answered his question. As he had said, in exchange for liberating the captives, they intend to conclude the war using a non-aggression treaty.


“This war, no matter how we think of it, this is 『Gabranth』 and 『Humas』 defeat. Since the bridge was destroyed, we lost the method to invade the demon world with our war potential.” (Leglos)


“What you are saying is that we are in a deadlock?” (Lenon)


“Yes, the place in which our remaining potential is gathered has been trying to look for a method on the other side now. Although future talks are originally necessary with the Victoria’s king, strangely, the humans returned to their own country.” (Leglos)


“Whoa, are they running away?” (Lenon)


“I don’t know if they ran away, I do not even understand if there is any significance of returning home, anyway, only a few 『Humas』 is assigned to this place now.” (Leglos)


As Leglos says, only the human soldiers originally tasked to guard the border were left, the other soldiers had returned to their country as ordered by their commander. 


“Did something happened at 【Victorias】 ?” (Lenon)


“I have no idea. Even if something did, I do not have the time to pay attention to it.” (Leglos)


“That’s right. The enemy will likely move soon from our side if we wait in here. They may try to force us to negotiate using our captured comrades.” (Leowald)


Leowald spoke with a grim expression. After all, he did not like to be the one playing the second move. However, since a large quantity of his comrades were arrested by the opponent, he can’t move thoughtlessly as he does not know what the fate of his comrades would be.


He was prepared to defeat the enemy even if his companions died in vain, but such method does not exist right now. After all, not being possible to cross the bridge is quite an awful obstacle. 


“However Father, supposing they intend to use the non-aggression treaty for our comrades liberation, how will we respond?” (Lenon)


Lenon and Leglos both glanced at their King requesting an answer from Lenon’s question.


“.... I wonder.” (Leowald)


“Hey, what is with that I wonder…..” (Lenon)


“At any rate, we will talk about it when that time comes.” (Leowald)


“Is that so.” (Lenon)


“However, if you want to hear my true intentions, I want to regain our comrades safely. I consider the beastmen the same as my family. But, I need to meekly accept our situation, though I’m sure that our pride will not permit this. Do not forget that we have tasted a lot of hardships in the past.” (Leowald)


He spoke those words mixed with anger.


“We are this time allied to the humans due to our similar hatred, only because the 『Evila』 are too strong. I judged that we could surely suppress them with this. Well, the result ended up this way.” (Leowald)


He sighed as he was self-ridiculing himself.


“Speaking of grudges, the 『Humas』 has a bigger grudge towards them. But, so that we may live in this world in our own way, we should first defeat the 『Evila』.” (Leowald)


“However, we failed right?” (Leglos)


“You have a point. However, to give up because of this one defeat, I believe our will is not that weak.” (Leowald)


He clenched his fist and turned to the two people where a strong will appeared in his eyes.


“This time, it is surely a big loss to have lost a comrade. However, if the opponent thrust that condition into our shields, isn’t the act to nod in agreement the right thing to do?” (Leowald)


“........” (Leglos)


“As for the captured comrades, all of them have resolved on this. Rather, they participated in this war simply because they have prepared for this. If we regret our lives here and accept defeat, aren’t we trampling down their resolutions?” (Leowald)


“Father..” (Lenon)


“Father…” (Leglos)


“Let’s have a conference for the time being. However, everyone’s opinion is identical. We should not yield to those guys. Everything has not yet been deprived of us!” (Leowald)


Then, suddenly.


“ “ “ “Uoooooooooooo! “ “ “ “


He heard a loud voice shaking the atmosphere from the surroundings. When he saw it, beastmen soldiers had gathered before him unnoticed, everyone raised their fist loudly and raised their morale.


“Y-You guys...” (Leowald)


Even Leowald cannot help but be taken away in astonishment.


“Cheers to our King-sama!”


“Right, right! We haven’t been defeated yet!”


“We will fight to the last!”


Those words came from several soldiers intending to show their willingness to fight. Due to those voices, Leowald gladly raised the corners of his mouth.


“Well said my comrades! That is right! We can still fight! This is the blazing pride of the 『Gabranth』, while it is still hot, it will continue to burn!” (Leowald)


Leowald shouted while grasping his fist around his heart.


“If they want to win from us, they have to extinguish this flame!” (Leowald)


“”””Extinguish! “”””


“This flame will be together with us to the last!” (Leowald)


“”””Be together with our flames!””””


“We are!” (Leowald)


“”””We are!”””” 


“On the day, this flame will burn out!” (Leowald)


“””“On the day, this flame will burn out!””””


“We will continue to fight!” (Leowald)


“”””We will continue to fight!””””


And again at the end, an earsplitting loud voice was heard. Apparently, they seem to have already decided on their path. Leglos and Lenon, mutually nodded with each other as a sign that they have steeled themselves.


Although they didn’t know when 『Evila』 will move out , Legios believed that they needed to find a way to cross over to the demon world. With that in mind, he left the place to find a certain person in the army. 