Chapter 70: Little Girl – Strong? Weak?

This time she did not ask out of curiosity but out of wariness. Hiiro gulps and focuses his gaze towards her.

(Trying to fool her… might be dangerous) (Hiiro)

He felt that this time she might force an answer out of him. He was not at fault this time around, but to them there is no doubt that his existence is far too strange.

Also, for some reason or another, there was something else that was bugging him. He decided that until he knew what that was, he wouldn’t reveal anything about himself to them.

Before that, however, it was necessary to prepare some insurance for himself. Thinking that, Hiiro puts on a sharp expression and…

“You can’t even properly discipline your own servants. I can’t believe it” (Hiiro)
“What did you say…?” (Liliyn)

Just like he expected, Liliyn knits her brow with an unpleasant expression on her face as she glares at Hiiro.

“To think you’d express your hospitality towards a guest with this kind of poor behavior. I’m at a loss for words” (Hiiro)
“M-Muu1…” (Liliyn)

What Hiiro was saying was correct, and Liliyn couldn’t retort.

“And on top of that, you demand I identify myself? Are you sleep talking, oi2?” (Hiiro)

Hiiro and Liliyn were glaring at each other and Silva, who was standing nearby, had a complicated expression on his face.

“Giving me some half-hearted apology. If you’re going to apologize, how about you show me some sincerity” (Hiiro)
“Ku3…” (Liliyn)

Even though she was at fault, Liliyn began to get irritated being told off so much by the rude and younger Hiiro.
Hiiro stared at that girl, as if observing her. Then suddenly, Hiiro gets up and moves to the corner of the room, distancing himself from the two.

Not knowing what was going on, Liliyn holds her breath and follows Hiiro with her eyes. Ignoring her gaze, Hiiro concentrates magic into his fingertip.
Obviously taking note of his actions, the glint in Liliyn’s eyes grow stronger and Silva immediately stands in front of her as if to shield her.

(Oh, this went rather well) (Hiiro)

Smiling internally, he begins writing a word while making sure they can’t see it. The word he wrote was [Transfer]. With this, it became possible for him to escape at any time.

In truth, his actions were a gamble. It would have been dangerous if they had assaulted him as soon as they sensed his magic. That is because he does not understand how strong Silva and Liliyn are. No, Liliyn is strong without a doubt. She carries an atmosphere similar to that Rarashik. If they fought head on, Hiiro would have no chance of victory.

That is why Hiiro was extremely nervous while he was writing that word. Only he himself knows how relieved he was after he had finished writing it.

(Now, I’ve got my insurance. Next is…) (Hiiro)

Hiiro wanted to ask about what was on his mind earlier. He shifts his gaze towards the two who are watching him make as little effort as possible.

“Oi, isn’t there something you should be saying?” (Hiiro)

If she didn’t he would act according to the plan that he just came up with. His plan is that if they still emit hostility towards him and refuse to talk he will use [Transfer], go to where Mikazuki is, then escape from the island.

It seems that outside there are many monsters around Rank S loitering around, but Hiiro decided that would be much safer than staying here.
Hiiro puts himself on guard while observing the two. And then…

“Nofofo, it is exactly as you say” (Silva)

With a faint smile, Silva gets out of his battle stance and lightly bows.

“O-Oi Silva…” (Liliyn)

Watching his actions just now, even Liliyn’s eyes became dots4.

“Ojou-sama5, everything that Hiiro-sama6 has said is true” (Silva)
“Ha7?” (Liliyn)
“That individual is our guest. He is also the benefactor who saved my life. Return favor with favor, and return life with life. It would not do to forget that” (Silva)
“…” (Liliyn)
“We invited him into this mansion, then we did something to test him, and after that a servant of the mansion wounded him. Considering all of that, for us to behave in such a manner, it would do no good for Ojou-sama’s future nor Ojou-sama’s ambition” (Silva)
“…” (Liliyn)
“Ojou-sama, I believe you know what it is that you must do” (Silva)

Liliyn became silent for a while after listening to Silva’s words. Hiiro stared at Liliyn without changing expression, but suddenly had a look of shock on his face.
As for why…

“Uu…” (Liliyn)

Liliyn’s eyes were teary.

(Eh? …Ha? She’s crying?) (Hiiro)

Even Hiiro had not expected this situation and was taken aback.
It hadn’t gotten to the point where she was shedding tears but she had the look of an upset child that was trying to hold it back, and…

“S-Shwut up! I know!” (Liliyn)

She throws the doll in her hand towards Silva with a *dosu*8 and sharply turns towards Hiiro.

“I’m sowwy! I was wrong! Forgib me!” (Liliyn)

She doesn’t bow her head, but looking at her pouting and her semi-desperate apology, she seemed to have lost all sense of maliciousness.

(W-What’s with her… It’s almost as if…) (Hiiro)

“Nofofofofo! You are like a small child, crying in such a manner” (Silva)

Silva said what Hiiro wanted to say.

A fist sank into his gut *dosu* and Silva groaned and crumpled to the floor.

But looking at her, Hiiro thought she really was like a child. He had heard that she has lived a long life. By Silva’s manner of speaking, it seems to be true.
However she didn’t get what she wanted, moreover she was scolded by one of her servants. The girl ended up showing an emotional side that didn’t match her age.

Looking at her, feelings of shame starting to build within Hiiro for fearing and being defensive against such a girl.
Though she is much older than he is, it seems like she is still a child. She demands someone to tell her something because she wants to know, and she becomes emotional because someone scolds her. That kind of child.

(Oi oi, what about my resolution up until now…) (Hiiro)

He became less tense and his set word almost faded, so he hurriedly concentrates on it again. If he were to let it disappear without activating it, he would suffer a <<Rebound>> and for six hours he would be weakened and unable to use <<Word Magic>>. He definitely did not want that to happen.

However, he thought with this he’d be able to ask about what was on his mind earlier, so he begins to talk.

“Can I have a moment?” (Hiiro)
“W-What is it!” (Liliyn)

She looks at him with slightly inflamed eyes. He isn’t scared of her at all anymore. If anything, right now the atmosphere around her would make most people want to pat her on the head and make her feel safe.

“Haa9, well, what. I’ve accepted your apology for now, so about the topic from before…” (Hiiro)
“Are you going to tell me!?” (Liliyn)

She didn’t suddenly change expression, but she spoke with a smile on her face. Seeing that, Hiiro made a bitter smile and…

“…Before that, tell me one thing” (Hiiro)
“Mu…” (Liliyn)

She knits her brow with an unpleasant expression, as if to say “you have more you want to say?”.

“Ojou-sama” (Silva)

Silva, who had suddenly resurrected, spoke out to calm Liliyn. She sighs and waves her hand.

“Okay okay, I understand. However, after I answer your question you will tell me about yourself, right?” (Liliyn)

She glares at Hiiro with a look that could kill.
It may have been because he had seen her crying just now, but Hiiro does not feel intimidated at all. He could only see it as a child putting on airs.

“That depends on how you answer my question” (Hiiro)
“…Fun10, very well. Unlike others, I am open-minded. I will let you ask your question first” (Liliyn)

Liliyn says so pompously, and Silva apologetically bows his head.

“Then, my question. Akachibi11, do you…” (Hiiro)
“Wait” (Liliyn)
“What is it?” (Hiiro)
“W-What did you say just now?” (Liliyn)
“Ha?” (Hiiro)
“W-What do y-you mean by A-Aka-Akachibi?” (Liliyn)

Silva, who is standing next to her, is dripping sweat like a waterfall. He is panicking because it seems like all Hell will break loose.

“Of course I’m referring to you. You have red hair and you’re a pipsqueak, therefore Akachibi” (Hiiro)
“Hohou12… It seems like you want me to pulverize you into pulp…” (Liliyn)

One can sense an odd aura rumbling behind her *gogogogogogo*13, but the only one that notices it and breaking into a cold sweat is Silva.

“What, you don’t like it?” (Hiiro)
“Of course I don’t! Just who do you think I am!?” (Liliyn)
“A crybaby brat” (Hiiro)
“Ugu14… Y-You…” (Liliyn)

Liliyn’s face turns red and her eyes are full of rage.

“You hate it that much? Then how about Akaloli15?” (Hiiro)
“Ha? Hm? What did you say? ‘Akaloli’? What is ‘Akaloli’, Silva?” (Liliyn)
“Huh? H-Ha! Um, that is… Err, Akaloli is…” (Silva)

In his heart he believes that it most likely refers to a red lolita, but he doesn’t put it into words. Not knowing what to do, he desperately shifts his train of thought 180 degrees and thinks deeply.

“Mumumu, yes! It’s that! ‘Akaloli’ is a compliment, Ojou-sama!” (Silva)
“Mu? I-Is that so?” (Liliyn)
“Y-Yes! According to rumors, ‘Akaloli’ is a name granted to sweet, beautiful ladies!” (Silva)
“Hou, such a word is becoming popular outside, is it” (Liliyn)

She nods in understanding. Hiiro stares at those two with a blank look on his face.

“T-Therefore, would it not be good to accept it?” (Silva)
“M-Muu… Is that so…” (Liliyn)

There is not such word as Akaloli. Why? Because Hiiro just made it up on the spot. It is also not a compliment. It was nothing more than an easy nickname for him to remember. Liliyn, ignorant of the ways of the world, had no way of knowing that.

“Umu16, then I shall allow you to call me Akaloli! Because I am tolerant! Ahahahaha!” (Liliyn)
“I-Isn’t that nice! Nofofofofo!” (Silva)

Silva could not help but think that he was glad Liliyn was so simple.

“Oi, can I ask already?” (Hiiro)
“Mu? It’s fine, go ahead and speak” (Liliyn)

Hiiro feels relieved finally being able to return to the main topic.

“…Question. Are you the type that concerns yourself with and discriminates based on race?” (Hiiro)
“Ha? What nonsense are you saying” (Liliyn)

Hiiro remains quiet and looks into her eyes. Liliyn receives his gaze and makes a serious expression.

“Fun, race doesn’t matter. I just like the strong. If there is any discrimination, it would be based only on strength and weakness, kozou17” (Liliyn)

Suddenly a cunning expression full of experience could be seen on her small face. Hiiro understood that as the answer she came to, which she could state with confidence. He couldn’t believe this was the same girl that was teary due to getting scolded earlier.

“…I see” (Hiiro)

It wasn’t anything difficult. She was just a person that either had an interest in someone or didn’t, and whether that person was strong or not. She was a person that made decisions based only on that.

Silva looks at her with a gentle expression. It seems like an expression one would have looking at his own daughter. Hiiro thought that it would be nice if he always acted like a regular adult but also thought it was extremely unfortunate that Silva had a tendency to suddenly transform into a pervert.

(They’re a bit different from Ossan18 and Muir, but it feels like it’d be fine to speak a little with these people. However, I’ll have to have them promise fist) (Hiiro)

Thinking that, Hiiro places his hand on the bandages Silva wrapped.

“Oi, before I tell you about myself, promise me that you won’t tell anyone” (Hiiro)
“Don’t look down on me. Do I look like that kind of loose-lipped woman, you insolent peasant!” (Liliyn)
“Nofofofofo! If Hiiro-sama demands it, I will take it with me to my grave” (Silva)
“…I got it” (Hiiro)

Hiiro slowly cuts off the bandages. The other two look at him with a confused expression on their faces.

“W-What are you-” (Liliyn)
“Shut up and watch” (Hiiro)

*Pishun!*19

Hiiro disappeared in an instant and Liliyn and Silva’s eyes were wide in surprise.

“Where are you looking?” (Hiiro)

The two gasp and look towards the direction his voice came from. For some reason Hiiro, who was standing in the corner of the room just now, was sitting on the sofa.

(This should be enough, but I may as well fix this while I’m at it. Their surprised faces are fun to look at too) (Hiiro)

Putting aside those two and looking at his shoulder which was still bleeding, Hiiro once again concentrates magic into his fingertips.

(If it’s a wound of this level I won’t need two characters… No, before that…) (Hiiro)

He writes some word into his arm. That word, however, seemed to be a trigger word and disappeared as if it were absorbed.

Afterwords, he writes [Heal] and activates it on his shoulder. The bite marks on his flesh slowly start to fade and returns to its normal state

Liliyn and Silva watched the scene in front of them frozen in surprise, as if time had stopped. Liliyn gasps and brings her hand to her chin with a deep look on her face and begins muttering.

“Healing magic…? No, healing magic is light attribute magic. There is no way Evila can use it. I can’t feel the power of light either. And that movement technique just now… What does it mean?” (Liliyn)

As she was muttering, Hiiro’s wound completely healed. Liliyn speaks to him with her hand still on her chin.

“Explain. What… was that just now?” (Liliyn)
“Even if you ask me ‘what’, it was my magic” (Hiiro)
“…I mean, why does an Evila have… no, wait. I see. You, that’s a Unique magic, isn’t it?” (Liliyn)

Silva, as if he had already realized, looks towards Hiiro and lightly nods.

“Aah20, that’s right. It’s called <<Word Magic>>. I can’t explain it in detail, but it’s omnipotence is its selling point” (Hiiro)
“Explain it to me” (Liliyn)
“I refuse. I don’t have any obligation to tell you any more. Even this much is a great service” (Hiiro)
“Mu, you…” (Liliyn)

Liliyn looks at him with a face obviously full of rage. Hiiro, however, ignores her and answers.

“Do you blab about your abilities to strangers? I only told you this much because I felt like it. I won’t tell you anything about my power beyond this. That’s normal, isn’t it?” (Hiiro)
“Mu…” (Liliyn)

Possibly because she realized that Hiiro’s decision was correct her rage lessened, but apparently still wanting to hear an explanation she looks at him with impatience in her eyes. He can understand her curiosity, but he didn’t plan on saying anything further.

In fact, he had only planned to speak up until that point. He had thought that if Liliyn was the type that was concerned over race he would just teleport and leave.

Like the Gabranth that sought war, if she was the type that thought her race was superior and had thoughts of destroying the other races there would definitely be conflict if he were to stay with them.

The result, however, was that Liliyn had no interest in races. She only thought of strong and weak. It’s simple, but it is an easy-to-understand answer, one which may give a favorable impression.

This is what Hiiro had wanted to know after hearing about Liliyn from Silva. He wanted to know her way of thinking and wanted to ask her while he was here.

“In other words, your magic is a Unique magic. You can create fire, you can increase your physical capabilities, and you can even heal. In that case… I see, that appearance too?” (Liliyn)
“Who knows?” (Hiiro)
“There’s nothing wrong with answering that much” (Liliyn)
“I already told you, I’m not an [Imp]. That should be enough, right?” (Hiiro)
“M… Mu” (Liliyn)

Liliyn looks up at Hiiro while pouting. Unlike before, she looks like a child that just got her toy taken away from her, causing Hiiro to unconsciously feel less serious.

“Nofofofofo! I did not believe you to be an average person, but to think you were a user of Unique magic! Nofofofofo!” (Silva)
“Eei21 shut up, Silva! Aah, I want to know! I’ll use force to-” (Liliyn)
“That is no good, Ojou-sama. He is our guest. We have also caused him quite a bit of trouble” (Silva)
“Mu…” (Liliyn)

Liliyn can’t retort to Silva’s reasoning.

“Isn’t it fine! This place is occupied by odd and eccentric people. Isn’t that what you always say, Ojou-sama? Isn’t is useless to inquire about every stranger you meet?” (Silva)
“That’s true, but… Mu” (Liliyn)

However, it seems like she can’t help but be interested in someone as odd as Hiiro. She stares at him while biting her nails.
At that moment, Hiiro was smiling on the inside.

(Alright, seems like it went well) (Hiiro)

From his point of view, he could see something about Liliyn and Silva. It was their <<Status>>.

When he used [Heal] he immediately activated [Pry] which he had set beforehand. He couldn’t find an opening to use it during the meal, however he used the opportunity earlier to activate both words. That way they wouldn’t doubt him when he used [Pry].

On another note, when he activated the two character word the words that he had set previously disappeared, but this was the word that he had set prior to activating [Heal]. It was so that they wouldn’t sense anything odd when he activated it.

Liliyn Li Reysis Redrose
Lv: 148

HP: 6733/6733
MP: 5876/5876

EXP: 2796139
NEXT: 98022

ATK: 977
DEF: 944
AGL: 1159
HIT: 1220
INT: 1476

<<Magic Attribute>> : None

<<Magic>>: Dream Illusion Magic (Dream Eater, Bewitching Confinement, Ethereal Construction)

<Titles>>: One Who Lives Among Illusions | Little Girl | Crybaby | Connoisseur of Wine | Seeker | Manipulator | Monster Slayer | Killer of the Unique | Demon Who Shreds | Transcendent | Permanence Devil | Redrose Witch | One Who Seeks the Strong | An Unusual Person

Silva Plutis
Lv: 80

HP: 1250/1250
MP: 6000/6000

EXP: 604441
NEXT: 23000

ATK: 430
DEF: 355
AGL: 490
HIT: 333
INT: 1000

<<Magic Attribute>> : Dark

<<Magic>>: Pool Ball (Dark - Attack) | Dark Gate (Dark - Movement) | Black Out (Dark - Attack) | Fear Cremation (Dark - Attack) | Shadow Create (Dark)

<Titles>>: Spirit of Darkness | Tribe That Sees | Heretic | Perverted Butler | Immortal Lolicon | Playboy | Feminist | Old Man of Sexual Harassment | The Enemy? Ally? of Women | Sage | Omnipotent | Pacifist | Faithful | White-Haired Demon | The Strongest Shield 