Chapter 47: Arnold’s Other Objective

 Hiiro, who had decided to leave the [Beast Kingdom Capital: Passion] in a week, was currently being guided through the city by Arnold. Obviously, Muir was also with them. Incidentally, Rarashiku was currently secluding herself in her lab, panting profusely as she said she was going to use the honey she got from Arnold to make her sake.


 Arnold: “It looks like the citizens still don’t know, huh.”


 Looking at the state of the people scattered around the streets, Arnold opened his mouth. The Suspension of War. Actually, it might be more befitting to say that the war had been ended. It seemed that the civilians were still unaware of what had transpired.

 Of course, if they knew this, it would become quite a significant issue. Yet, commotion was absent from within the country.

 After what happened, Rarashik’s [Spirit] Yuki went to investigate the full details about the war. Hearing about this, Rarashik then told Arnold about the circumstances.


 Arnold: "But to think that they would really demolish the bridge. What the hell is the Demon Lord thinking…?"

Muir: "B-but this way, nobody ended up getting hurt, right?"

Arnold: "Nn~, certainly it’s as Muir says. However, to the Gabranth, it would be as if their Pride was uprooted and denied. I’ll bet that they’d be pissed. Well, I’m also a Gabranth though..."

Hiiro: “I don’t really get it, but if nobody died, then I’d consider it to be a good thing…”

Arnold: "Well, its fine to have those thoughts. But the main problem is for what purpose did the Evila do such a thing? If it was the Humas that broke the bridge then I could sorta understand. If they did so, the Evila wouldn’t be able to cross into the Human Continent so easily. However, right now the Evila are fighting the Gabranth. The Humas have got nothing to do with this.”


 Unable to comprehend the Demon Lord’s motive, Muir also tilted her head to the side. Then, as if she suddenly realized something, Muir posed Arnold with a question.


 Muir: "Nee, Oji-san. Speaking of which, why doesn’t the Humas destroy their bridge?"

Arnold: "Nn? What’re you talking about?"

Muir: "Hora, didn’t you just tell us? Even though they would be able to prevent the Evila’s advance, why haven’t they destroyed the bridge yet?”


 It was exactly as Muir had indicated. Between every two continents lied a single bridge that would connect the two. Obviously, this meant that a bridge existed that joined the Humas continent and the Evila continent.

 Being reffered to as the [Schlecken Bridge], both it’s size nor its durability could be compared to the [Gedult Bridge]. It was a structure that even the Humas were able to break if they wanted to.


 Arnold: “It’s not that they didn’t want to destroy it, they were not allowed to."

Muir: "......how come?"


 Muir tilted her head to the side with a *koku. (TL: Not sure how to translate this. Think of it like a sound effect that indicates confusion. You’ll probably encounter it in anime as wooden block sounds.)


 Arnold: "The bridge always has an Evila guard posted there."

Muir: "Ah, is that so?"

Arnold: "Also, it seems that the person protecting the bridge is a troublesome fellow."

Muir: "Troublesome?"

Arnold: "Aa, I’m pretty sure their name was............Iraora?"

Muir: "Is that person strong?"

Arnold: "Aa, they’re ridiculously strong, or so they say. Once, Judom-san also affrimed that."


 While overhearing the conversation as if it didn’t concern him, Hiiro narrowed his eyes as he continued walking. If the SSS Ranker Judom Lankars had said so, then this Iraora’s strength was probably the real deal.

 To begin with, if they were entrusted with protecting such an important bridge, there is no doubt that they must be a person with substantial abilities.


 Arnold: "Moreover, it seems that they used to be a member of the [Cruel Brigade]. They’re probably really strong."


 The Demon Lord’s private bodyguards, the [Cruel Brigade], were a squad that consisted of only 6 people. It went without saying that those 6 possessed exceptional strength. Even if you gathered ordinary adventurers in hundreds, they wouldn’t even be called an adversary for such veterans.


 Muir: "I see, if such a person was protecting the bridge, it’d be hard to try and destroy it."

Arnold: "Well, even if the bridge was destroyed, the Evila would probably just cross the sea so I doubt it would make a difference..."


 In a sea that contained horrifying monsters and treacherous whirlpools, trying to traverse such a location would undoubtedly be perilous. Even so, Arnold assumed that the Evila would cross the sea anyway.


 Hiiro: "By the way, where are you planning on going from here?"

Arnold: "Ah? Obviously, I’m headed to the Guild. We got something that we can hand in for some cash, right? You know, like what we got from that damned snake."


 Hearing Arnold’s words, Hiiro paused as he suddenly recalled what had happened.


 Arnold: "Nn? What’s up, Hiiro?"

Hiiro: "......"

Arnold: "Oi, why’re you looking away?"

Hiiro: "......"

Arnold: "............ you, did get the spoils, right?"

Hiiro: "......"

Arnold: "Yo~sh, yosh, yosh. Let’s calm down a bit. At that time, thanks to the cute and angelic Muir, we were able to successfully kill  the Clay Viper, right?”

Hiiro: "......"


 Being called cute, Muir became flustered. However, Hiiro remained still as he continued to avert his gaze.


 Arnold: "After that, Muir and I exited the [Gree cave] first, leaving you to go and get the spoils, right?"

Hiiro: "......"

Arnold: "You took quite a long time when you were in that cave............ you, what exactly were you doing in that time?"


 Hiiro was met with a gaze full of suspicion. As the blistering stare was approaching him, Hiiro reluctantly used a scissors gesture to puncture Arnold in both eyes.


 Arnold: "Angyaaaaaaaa!" (TL: Scream of agony.)


 Arnold began rolling around on the ground while covering his eyes. Arnold’s sudden strange behavior garnered the attention of all the surrounding people.


 Hiiro: "Yosh, let’s go Chibi."

Arnold: "Hold it, kora Ooooooooooooooiiiiiiiiiiii!"


 Seeing Arnold’s swift recovery, Hiiro was impressed.


 Arnold: "Tenmeeeee! How bout you give us an explanation!?" (TL: An elongated ‘Tenmee’ which is a morphed version of ‘Temee’ which roughly translates to ‘You Bastard’.)


 As if he was a Yakuza, Arnold glared at Hiiro with bulging eyes. Hiiro considered poking Arnold’s eyes again. However, Arnold was at a fair distance from Hiiro. Whether he consciously took such action due to his previous experience or not remained to be seen. Either way, Hiiro had no choice but to explain what had occurred.

 (TL: In regards to the bulging eyes. This would be directly translated as ‘googly eyes’. For those who can’t get a good picture, think as if the eyes are wide open and the irises are small and convulsing wildly. That’s what Arnold is doing.)






 Arnold: "Did some weird fellow take all of the snake’s spoils?"

Hiiro: "Aa."

Arnold: "Name?"

Hiiro: "............ no idea."

Arnold: "You forgot, didn’t you."

Hiiro: "I just didn’t have any interest in it."


 Hiiro puffed his chest out, as if saying he didn’t do anything bad.


 Arnold: "Nn~ But is there such a fellow that’s capable of doing that?  In addition, Hiiro was their opponent, you know. In the first place, why the hell would they be gathering the snake’s corpse?”

Hiiro: "-the hell would I know?"

Arnold: “As I thought. What do you think, Muir?”

Muir: "Etto...maybe they want to make them a grave...or something?"

Arnold: "Oo~! As expected of Muir! What an adorable idea!"


 Being hugged tightly, Muir painfully struggled slightly. Looking at the two, Hiiro spoke a single word.


 Hiiro: "Lolicon."

Arnold: "At least call me Parent Baka you Boke~!" (TL: Boke is a variant of Baka which is usually used by, but not exclusive to, people who speak the Kansai Dialect.)


 Arnold roared as if to  say ‘How are you going to compensate me if I get another title attached to me?’


 Hiiro: "Well, to summarise, what happened to me was basically robbery. I had to give up on collecting the Snake’s loot. Besides, we’re not really in any financial trouble so you don’t really care, right?”

Arnold: "...well, I guess it’s fine. Actually, tell us about stuff like that sooner!I Actually, tell us the moment it happens!"


 Arnold was absolutely right. (TL: The kanji for ‘that street’ is used here, yet the content seems to indicate that it meant ‘it was as he said’. As I translate context, I have opted for this translation.)


 Arnold: "In any case, let’s go to the guild and redeem the rest of the loot from other monsters.”


 The trio carried their legs towards the guild.







 The guild was devoid of any adventurers. As expected, it seemed that everyone had been deployed off to the front lines. Seeing Hiiro and company, the staff were completely taken aback. After saying that they had just returned from a journey, the staff were somehow convinced.

 As they finished redeeming their spoils, they immediately left the guild.


 Hiiro: "What’re we doin next?"

Arnold: "There was another reason why I came to this country, you know."

Hiiro: "Aa, speaking of which, you did mention something like that. That you had plans to go somewhere..."

Arnold: “Ou, I just need to go to the [King’s Tree] for a little bit."

Hiiro: "[King’s Tree]? Do you know somebody from the noble family?"


 [King’s Tree] refers to a large tree that houses royalty. Because this country has been built using trees as a base to support life, there are no such houses constructed out of stone like those in the Humas Continent. All of them are built out of trees.

 As such, the [King’s Tree] held the same meaning as a King’s castle. It was a place where the Gabranth Royalty resided.


 Arnold: "No, no. I’m not going there to see the royal family. I have business with a guy that I know who’s working there. If he found out that I came here and didn’t give them a greeting, it’d turn into a pain in the ass."


 Even though Arnold said it would likely be troublesome, the end of his words seemed to bleed with nostalgia. It seemed like he wasn’t all that opposed to meeting them.

 At the entrance of the [King’s Tree], two people who looked like soldiers were standing guard. In short, they were gatekeepers.


 Arnold: “Excuse me.”


 As Arnold called out the them, the two gatekeepers pointed their spears at him, piercing him with their sharp stares. The gatekeepers’ gazes then moved on towards the other two.


 Guards: "Who goes there?"


 Of course, seeing as this was the King’s residence, they had no choice but to be wary. However, as they were in the middle of a war, they were considerably on edge. It was possible that they had yet to be informed about the war’s conclusion.


 Arnold: "I’m nobody suspicious. I just came here because I have something I wanna ask you."

 Guard: “Something to ask...... you say?"


 As if they were appraising Arnold, they carefully scanned him from head to toe with their stares. After which, a guard opened his mouth.


 Guard: "We are currently in a state of emergency. You are aware that we are unable to permit any sort of meetings with the people of [King’s Tree]?"

Arnold: "I’m already aware about the current emergency. However, even if it’s just out here, I was kinda hoping to talk to somebody."

Guard: "......who were you hoping to interview?"

Arnold: "Well, about that..."


 As he said that, heavy footsteps carrying a tremendous momentum could be heard from far away. Following which...

 Dokka-! (TL: *Thud/Bang/Slam/etc.*)


 Arnold: "Gube-!?" (TL: A sound which signifies Arnold receiving a strong impact.)


 Arnold suddenly soared due to receiving an impact from someone who had abruptly appeared. He began rolling across the ground with exceptional velocity. It was as if Arnold had been struck by a speeding car. Following this, Arnold’s body had collided with a tree that was up ahead, finally causing him to stop.


 Muir: "O-Ojisan!?"


 Muir’s eyelid shot open wide as she screamed. The two soldiers were completely stunned, unmoving as if they had their time stopped.


 Hiiro: "Did he croak?"


 A sinister thought came out of Hiiro’s mouth. However, after sparing a glance at Arnold, his line of sight transferred to the one who had appeared. That person was grinding their teeth, revealing their frustration.


 ???: “I can’t believe you! How long are you gonna be daze for?! You Ou-Baka Arnold!" (TL: Ou-Baka = Big Idiot.)


 From the tobacco held in their lips, smoke rose towards the sky. From what could be observed, this person was a Gabranth woman who seemed to belong to the same race as Arnold. The woman was an astounding beauty and had the same unyielding eyes as Arnold. And yet, her physical stature was very muscular. Furthermore, as the woman wore a Maid Outfit, her figure evoked an extraordinary sense of incongruity.

As Arnold’s consciousness faded, he held his arm as he saw the figure of the woman glaring at him. Arnold answered with a trembling voice.


 Arnold: "......Ne...... nee......-chan ......" (TL: For those who don’t know, Nee-chan = Big Sister)


 As the distance between Arnold and him was substantial, Hiiro was unable to hear what he just said. However, Muir had clearly heard him, alternating her gaze between the two siblings.


 Muir: “Eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeehhhhhh!?”


 ...was how Muir expressed her shock. The two soldiers also displayed similar astonishment. 