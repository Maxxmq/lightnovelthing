Chapter 199 – Permanent Alliance

‘What do you mean by that Demon Queen?”

Leowald asked what the Demon Queen meant with her declaration some time ago. Behind Eveam were the many races of the 『Evila』 waiting upon her.

“I mean what I said. We, the 『Evila』, have no intention of depriving your territory.”

Even the beast men were now affixing their gaze at the Demon King. Of course they would. The 『Evila’s』 gambled on this duel to the point that they risked their life on the line.

Everyone thought that they were going to seize everything from 『Gabranth』. Even the beast men were planning to subdue 『Evila』 supposing they won.

It was for this reason that Leowald was confused about the Demon Queen’s true intention on why they started this fight all along.

But as expected of Beast King Leowald, despite his confusion on her intentions, he received the words of the Demon Queen with a serious face, and asked,

“….Then, what do you hope for?”

Indeed, if they were not intending to seize their territory, they still have a big demand to raise.

Eveam paused for a bit as everyone’s eyes were focused on her.

“….an Alliance!”

Her transparent voice resounded to everyone in vicinity. Leowald for that matter, narrowed his eyes as he looked at Eveam.

“An alliance….you say?”

“Indeed! However, it’s not just a simple alliance! It’s the eternal peace that I hoped for! Therefore, I would like to establish a permanent alliance!”

“Permanent….Alliance….”

Leowald reflected upon Eveam’s declaration.

“We do not hope to go against your people. Supposing our people 『Evila』, crosses the borders of 『Gabranth』, even with the said contract established from this duel, hatred and anger will surely come out without fail. But I do not hope for such a thing to happen! I want the races from 『Evila』 and 『Gabranth』 to respect each other, and struggle hand in hand for peace! Therefore, the race who lives there should govern their own respective continents. However, I want to have a relation where each town’s trades mutually for the development of both countries, or in short an alliance!”

“An alliance that continues for eternity…. you mean that kind of alliance?”

“Yes. That is the demand we hope from this duel!”

Beast men also looked at one another, puzzled in the same way as their king. Though they assumed that the 『Evila』 were going to conquer them, even determined themselves the moment they lost the fight, but once they were faced with this kind of bewildering request, they didn’t know how to answer it, and simply focused their eyes on Leowald.

The Demon King and Beast King mutually looked at each other without turning their eyes away. Then, Leowald spoke first.

“Permanent…..Eternity….Alliance…. Do you think that it’s possible to continue doing this kind of thing?”

“It’s possible.”

“Have you ever thought why your predecessors never tried to do it in the past?”

“….”

“People eventually fight one another, this is what our current world had become. Certainly, the alliance you propose is one step to peace. However, the eternity you hope for will never come.”

Eveam quietly closed her eyes when she heard those words.

“You’re exactly correct. The eternity won’t last even if we establish this alliance. History has proven us of that.”

“Then, why do you still hope for it?”

“That’s simple!”

Eveam opened her eyes wide.

“Hm?”

“We only have to make a new history if it doesn’t exist yet!”

“…..!?”

Her straight forward eyes were turned to Leowald.

“I don’t want to my dream to end up a dream. I may be naive, but if it’s about my idealism, I’ve always hope for the best of it! This is why it isn’t just an ordinary alliance, because what I hope for is an eternal alliance!”

Eveam’s aspiration gushed out from her. The atmosphere trembled from her words, as the beast men have their breaths taken away.

“……kuku, you really are naive Demon Queen.”

“……..”

“This alliance won’t last throughout all eternity.”

“……..”

“But…..kukuku… To make a new history that hasn’t existed yet….kuku, gahahahahaha!”

Everyone’s glance turned towards Leowald who suddenly burst out into laughter.

“…. though I had been told the same thing when I was appointed as the ruler, you however is an exception. To even propose a helping hand to the ones who aimed for your life….”

Eveam loosened her cheeks, and then

“Of course, I am what I am! Beast King Leowal—ah no— To all the 『Gabranth’s』！Do you not wish to take this hand of mine?”

As Eveam said so, she presented her extended hands forward. Leowald watched that hand for a while before he straddled to his feet, and turned his eyes towards the beast men.

“……the country cannot exist without its people. As a ruler, it is my duty to love and defend my people, and let no one take their future away from them.”

“……”

“My father told me those words all the time.”

Leowald confirmed the faces of his people.

“……my people, will you follow me together?”

Leowald’s answer seems to have gone out already. Everyone noticed that, and

“Gladly!”

“Of course!”

“We will accompany our King no matter what road you take!”

As Leowald uttered so, everyone happily returned his words back at him. And because of the flood emotions from them flowing through him, he couldn’t help but make a subtle smile.

“Father, this is the answer of your people.”

Leglos gently smiled, and nodded. Not only him, but Lenon, Kukulia, and Mimiru similarly nodded as a sign of their agreement towards Leowald’s decision.

Leowald once again faced Eveam. He saw the hand still presented to him, and slowly turned his glance to Eveam.

“Demon Queen Eveam.”

“Yes?”

“In the name of Beast King Leowald, we 『Gabranth’s』, form an alliance — no, we hereby establish a permanent alliance with the  『Evila’s』.”

And the moment they shook their hands, a shout of joy roared on both sides of their respective race.

“Let us make a new history 『Gabranth’s』 ! Here is the first step towards peace!”

 

Thus, the duel between 『Evila』 and 『Gabranth』 finally reached its conclusion. Although the result ended with 『Evila’s』 victory, Demon Queen Eveam was able demand a permanent alliance with the 『Gabranth’s』 who lost the fight.

The Beast King accepted her proposal. They judged that instead ruling one another, an alliance between two nations wasn’t a bad thing to establish.

By the favor of the duel where both race’s fought with their very best, the human beast were able to see 『Evila』 in a different light compared in the past.

 

Demon Queen Eveam was certainly someone full of idealism, a naive lass who has a habit of speaking nonsensical things. But because of her pure earnestness to pursue her dreams, she was able to grab hold of Leowald’s heart.

Although Leowald did not trust her completely, he thought that their future from now on could be made anew with the help of this Demon Queen.

It couldn’t be denied that both races lacked something compared to the other. However, if both races were able to fill each others needs, they would able to take one more step towards peace.

With the thought of not being ruled by the other, each races might be able to spent their days peacefully.

(Kuku…. so this is what they meant about being an idealist idiot….)

Leowald also had a dream. He wanted to obtain a world where everyone can laugh one another without any worries. Hence, he decided to overthrow and control 『Evila』 and 『Humas』 who were supposed to be his enemies.

The reason was because he couldn’t trust those people. However, once he matched fist with Eveam, her pure intention was transmitted to him.

As they fought with their lives on the line, they were able to gain respect and reconcile with their opponent. The two races that should have been dominated by hatred, were able to obtain a different kind of connection.

Of course, not all of the people would appreciate this alliance. However, he didn’t actually felt that from the expression of the people who fought in that place.

Leowald thought that it’s possible to struggle hand in hand with these kind of people.

(Though this is going to be difficult, what’s important is that we’ve taken the first step……. making brave decisions is another role of a ruler…right Father?)

His deceased father suspended in his mind. He was more rigorous than Leowald, and was a true ruler who regarded his people no matter what circumstance he had to take. Leowald remembered how jealous he was to his father as a ruler, and also how he yearned to be someone like him.

Leowald smiled unintentionally as he felt the warm hands of his sworn friend Eveam by his side.

“What’s wrong Leowald-dono?”

Eveam puckered up her brows, and asked him.

“Ah, sorry about that….I just noticed something. You 『Evila’s』 has the same hot blood flowing just like us.”

“Because we are alive.”

Even though she was still a young girl, she wore a very sophisticated smile on her face.

“Gahaha, I agree. It’s because we’re alive….. by the way, why did you add honorifics all of a sudden?”

Leowald felt uneasy when Eveam suddenly added honorifics in her way of speaking. Then, her cheek blushed from embarrassment,

“I-it’s because you are not an enemy anymore but a sworn friend. Moreover, Leowald-dono is a remarkable personage, of course I would naturally add honorifics.”

“Gahaha! You don’t need to mind those details! All the more if it’s for a friend!”

“B-but it’s impossible! It’s too unreasonable for me to talk like that to such a splendid ruler!”

“Fumu, you’re quite unexpectedly stubborn… wait, maybe it’s because you’re like that, huh? Anyway, you’ll eventually get used to it in the future. After all, we have a lot chance to see each other.”

“Y-yes! We have enough time to do that!”

Eveam replied gladly.

Then, he heard horse footsteps coming from somewhere. Leowald looked around trying to confirm its origin.

And then he saw soldiers riding on Raidpic’s towards them.

“Father….”

“Ah.”

Leglos leaked an insecure voice as Leowald noticed the heavy atmosphere from the soldiers coming towards them.

Suddenly, as the approaching soldiers gradually reached them,

Dogoooooooon!

A huge torrent of water appeared from the feets of the Raidpics, and the soldiers as they were washed away from it.

The water changed shape, this time in a form of a tentacle, and it entered through the soldier’s body. In that moment, the soldiers body swelled up, and

Bon!

……….. exploded.

 