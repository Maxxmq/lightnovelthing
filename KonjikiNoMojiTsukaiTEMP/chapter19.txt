Chapter 19: Encounter: a Unique Monster

At that time, just as the other two had suspected, Hiiro was on top of a nearby hill. From it, he could view the bridge without being discovered.

In truth, he had been forced to land on top of the bridge. Going at his slow pace, it cost way too much mana to get all the way to the other side.

There were a number of people on the bridge when he landed. Hiiro was forced to cover his entire body in his red robe to conceal his identity, and run for cover. However the Beast People on the bridge just thought that he was a flying demon swooping down hunting for food, so they paid him no mind.

(This bridge is way too long. Just how far does it run…)

Unlike in games, if you get too tired your HP will drop. So while running long distances, it will drop quite a bit. However is also recovers slowly if you rest.

The Beastman continent was covered in green.

The main difference between this one and the Human one was the amount of untamed wildlife. Of course, the human continent was also home to quite a bit of vegetation and mountainous environments as well.

However, here in the soil and the forests; in the lakes and rivers, the surrounding wildlife had much more energy.

(So each continent has its own merits)

The main selling point of the human continent was its human-made ecosystem. Manufacturing and trading; import and export. It was a land of business.

(I wonder what the Evila continent has… I guess I’ll have to see it with my own eyes sooner or later.)
(TL: Evila, written as magical being / demon, read as Evila)

As I think this, a carriage stops in front of me. From it comes a man who seems to be a peddler. Of course, he’s a beastman.

(This is bad… I can’t be found out here.)

Arnold said that unlike the human continent, this was a land where you could be randomly assaulted out of the blue. A war-ridden country. There’s a possibility of being attacked just because of one being from the Human race.

If that happens I don’t plan on losing, but I can’t cause a commotion here. I don’t want to get separated from Arnold when I can still get more information out of him.

(… I just thought of something.)

I write a character on my body, while thinking of something a certain person had.

“What are you doing here? Do you need a ride?”

In response to the man’s frank questions, Hiiro takes off his hood.

“Oh, you have some splendid ears there!”

The man smiles happily.

“I apologize. My companions should be here soon. Thanks for the concern, but don’t mind me.”
“Oh? I see. I wish you safe travels.”

And the man returns to his carriage.

(I somehow got through it. But that beastman was quite amiable)

I touch the ears that had sprung up on my head. They’re quite soft to the touch. But the ears weren’t the only thing that changed. Hiiro’s hair had changed to a silver color. It was the same color as Muir’s.

Using his 《Word Magic》 and holding an image of Muir’s race in mind, Hiiro had written 『Copy』 on his body. However if he had been thinking of Muir herself then his facial structure would have been greatly altered and it would be hard to balance, so he tried to imagine something else. However he had only ever met 2 Beastmen in his life.

Hiiro’s psych would not be able to handle turning into an Old man like Arnold, so he stuck with thinking of Muir’s race.

And here marked the birth of a silver haired, bespectacled boy. The words only changed the basic qualities of its target, so the effects remained over time. Hiiro would have to write 『Return』 to turn back.

(It’s weird to have a tail. My body feels oddly light, and my canines have gotten strangely strong.)

After waiting for a while, the other two finally got here. They were able to locate me by my red robe. But then…

“What’s with that haaaaaaiiiir!”

I had expected this to happen. Even Muir lets out a sound.

“Now then, let’s depart.”
“Oy, wait, wait, wait! Why the hell are you acting like nothing’s off!? There’s many thing’s I’d like to retort at right now! ”
“As always, you’re being quite fussy. Just be quiet for a bit, like that shorty over there.”
“Muir is in too much shock to speak! Just explain yourself! Why is your hair the same color as my beloved Muir’s!?”

And so I began explaining against my will. Muir stares at me in excitement. She touches her own hair and stares at my ears.

“For you to be able to do something like that…. How broken are you?”
“Koku Koku”

I’m already bored of dealing with these two. I let out a yawn. I had set out early today so there would be few people to witness me, so I want to sleep.

“Well, it’s not like I’m the real thing. Only my form has changed.”
“Hm? So your physical abilities are still human?”
“Pretty much.”
“I see, but still, your magic is quite amazing.”

The two seem to have seen《Word Magic》’s true usefulness. They let out sighs.

“Anyways, where will we be heading from here?”

As Hiiro asks, Arnold points his finger.

“If we head due west from here, we should find the village of 【Doggam】”
“What sort of place is that?”
“It’s a town of the 『Bearnt』 clan.”
(TL: Written as Bear People, read as Bearnt)

(『Bearnt』. I read about them in an encyclopedia. Apparently they’re quite a peaceful race, unlike bears.)

In the human capital, I tried to gather knowledge by reading the books stored at the guild.

“Well, the 『Bearnt』 are gentle folk, I don’t think you’ll be attacked even if you’re found out.”

So the information I attained was correct.

“Also the honey they produce is said to be quite a delicacy.”
“Ho? I’ll look forward to it.”
“Yeah.”
“What? The shorty’s never been there?”

Muir’s reaction is similar to my own, so It can be concluded she has never been to that village.

“Y-yes. T-the truth is, well…”

It seems to be difficult to say, but I can sense that she believes that it is something she has to mention. But Hiiro waves his hands.

“Ah, if you don’t want to say it, you don’t have to.”
“Eh?”

Muir stares blankly at Hiiro. Arnold follows in suit.

“I have no interest in you people’s past, so there’s no reason to force yourselves to tell me anything.”
“… T-that’s not it, but…”

Arnold looks towards the sad Muir, and tries to say something to lighten the atmosphere.

“As long as Hiiro’s fine with it, I guess we can leave it at that, Muir!”
“Uncle…”
“Let’s just go already.”

Hiiro begins walking off. Arnold begins patting Muir’s head, and then whispers into her ear.

“I know that you’re anxious, but Hiiro being Hiiro, if you tell him, he’ll probably just respond with something like, ‘and so what?’”
“Y-yeah… that’s true.”

Upon hearing Arnold’s words, it seems that a burden has been lifted from Muir’s chest.

“I hope that you will be able to tell him one day.”
“Yeah!”
“Now then, let’s get going so we don’t lose him!”
“Yeah!”

As he watched Muir running after Hiiro, he thought something like, ‘how cute’. If Hiiro had seen Arnold’s expression, he would have dragged him off to court already.

After walking for a bit, they come across the first monsters they have seen in a while.

“I think this was… a BukBuk?”

BukBuks are monsters that exist in the shape of books. It’s size is quite large. Moreover, this monster’s power comes from…

Snap Snap!

“Uoh! Quite a powerful bolt of lightning there.”

Arnold shouts out as he avoids it.

Right, BukBuks are monsters with the ability to use magic. Whatsmore, different species of the monster could learn different varieties of magic .However it is difficult to tell apart BukBuk subspecies.

“It’s been a while since I’ve met a monster worth fighting. Old Man, I’ll leave that one to you.”
“Perfect! Watch Arnold-sama’s brilliant swordplay! Muir, back off!”
“F-fine!”

We each take them on one at a time.

(The old man’s one uses thunder. Mine is…)

Bang!

Suddenly a large hand emerges out of the ground, trying to grab Hiiro.

“I see, so your specialty is earth!”

Hiiro takes out Piercer and tears apart the earthen hand. But the BukBuk’s magic causes several small fissures to open up. The fissures advance towards him, halting his movement.

“Don’t be so conceited…”

Hiiro concentrates magic into his finger and writes 『Quiet』 in the air. He throws it at the ground and it activates.

The ground calms down, and the fissures stop in their tracks. The BukBuk seems to be quite confused, and it hesitates in casting its next spell.

“Perish!”

In that space of time, Hiiro quickly closes the gap between them. Quickly erects a wall of dirt infront of itself.

“That isn’t nearly enough to stop my advance!”

Without stopping, Hiiro thrusts his sword into the wall. And the blade passes through it quite easily. The BukBuk behind the wall is also impaled. And after letting out a sound like the sound of stacks of paper falling to the ground, the monster stops moving.

“And now, the Old Man is…”

He seems to have already finished up. After dodging the thunder and closing the distance, it only took a single swing of his massive sword to end the BukBuk’s existence.

“Did you see that!? That’s my power! Nahahahahah!”

He seems to be in ecstasy. It may have been a troublesome monster, but it was definitely at a level we could have handled. Is it really that great to have beaten one of those? I let out a sigh.

After that battle, we met more monsters as we headed down the path. After overcoming numerous battlefields, we end up in a forest.

“If we get through this forest, we’ll arrive at 【Doggam】!”
“Don’t you find it odd?”
“What?”
“There were so many monsters before, but once we entered this forest, we haven’t seen a single one.”
“Aren’t they just afraid of us?”
“You sure are an easy going old man.”
“S-sorry, whenever Uncle gets conceited, he becomes like this.”
“I’ve already given up on him, so don’t worry.”
“Y-yes… *sigh*”

Muir looks a little embarrassed as she stares at Arnold. It’s as if she’s a kid observing her father on Parent’s day. (TL: event where a parent observes how their children do at school)

Arnold, who is strutting triumphantly suddenly halts his march. Hiiro becomes worried, and calls out to him. He is giving off a nervous expression.

“Oy, what’s wrong?”

Hiiro asks that as he looks ahead, and in front, he sees a single boar. Hiiro thought it was just another monster, but strangely, it was a monster that wasn’t in any of Hiiro’s books. The boar had yet to notice the party’s approach, and it peacefully eats grass.

He had read about a similar monster, so he asks.

“Is that a Big Boar?”
“N-no, it’s…”

Arnold is acting strange. He seems to be afraid. The Big Boar Hiiro knew of had short brown hair, however the monster in front of them was red.

“Hiiro… We’re running away.”
“Wha? What are you saying?”
“Don’t worry, just try to move without making a sound.”

Hiiro frowns upon seeing Arnold attempt to evade the quietly boar.

“What exactly are you doing, you pervert?”
“I keep telling you I’m not a pervert! …ah.”

Arnold lets out a large voice, and turns pale. Hiiro looks back at the boar and finds it staring angrily at them.

“D-damn… This is your fault, Hiiro!”
“And so what? Is that monster something special?”
“T-that’s a Unique monster!”
“Unique?”

I believe that the books said that Unique monsters were exceedingly rare. And that they were exceedingly ferocious. Arnold’s frantic expression seems to confirm this.

“T-that’s a Red Boar. A Rank S Monster!”
“Hmm.” 