Chapter 220 – Conditions for a Spirit Contract

“Contract? What did you just say?”

Hiiro exclaimed to Hoozuki. Well, suddenly saying something like contracting a spirit really makes one be taken aback.

However, Hoozuki’s speech isn’t ended yet.

“Yes. A contract. With a Spirit. Here”
“……What the heck? If I remember what you said a while ago, the vessel of an ordinary Humas and Evila is too small so contract is impossible, right?”

If they make a contract with a spirit, they will be crushed by its power and will be crippled for life.

“Certainly, if it’s a normal person then he may not be able to endure the load completely, and will meet a spiritual death. If we are talking to normal people that is”
“……Explain it more”
“Still don’t get it? It’s really simple. While using Red Qi, you manipulate two different kinds of power. The Red Qi is originally for half-breeds…… In other words it is impossible to use if the person doesn’t contain the soul of the two races”

Hiiro knew this already. He acquired the information upon learning the “Four Word Chain” in his head.

“Mixing two different powers is very difficult……even for me, using it normally is hard. Being a half-breed really has an advantage”
“I knew that. What I wanted to hear is, why are you suggesting to me, who is not a half-breed and is lacking a vessel for another soul, to make a contract?”

Hoozuki makes a long sigh while indirectly feeling his beard along.

“……you who is originally a pure human being can handle Red Qi. That signifies that, you have the same disposition equal to a half-breed”
“……Huh? Wait a minute. Then, a half-breed can also be able to make a contract with a spirit too?”

Hiiro reached that conclusion based from the current story.

“Only if the host has no blood of the beastmen in their veins”
“……I see. Because the person who has the blood of the beastmen has already a Spirit sleeping inside them, so it will be impossible for them to make a contract to another, right?”
“Right on. So it is possible if the person is a half-human and half-demon. Of course, it’s not like all of them can make a contract. They are only limited to those who have mastered Red Qi at the very least”
“I see. That’s a very interesting story you got there”

 

The half-breeds are being considered taboo and has been looked down till now because they weren’t granted Binding Techniques nor magic. An exactly different existence. And for this reason, they are despised.

But with this, the existence of half-breeds can now be seen in a new light. That is the Red Qi. So to say, if they were to have complete control of their power, then they’ll be promising.

Also, if they were able to master Red Qi to a certain degree, then they could make a Spirit Contract. If they were to make contract with a high-ranking spirit like Shishiraiga and Yukiouza, it is even possible for the half-breeds to obtain power and exceed other races.

(TN Note: It seems that Shishi’s full name is called Shishiraiga, as Yuki is Yukiouza. The previous translator Anri translated it as Shishi Liger but I’m keeping Shishi for the nickname as it is. This is open to suggestions)

(Well, there are merits and demerits in each of them, but this is really interesting. However, still, the face of the group of half-breed rejects will surely turn pale when they hear this story)

Hiiro met such party while in the midst of travelling. He was made to hear that half-breeds are a poor and dirty race that are supposed to have nothing. Shamoe which is also their brethren was made to hear that such an experience has been done all too well in the past.

But in actuality, the half-breeds actually possessed power exclusively for them. Of course, it cannot be done if they don’t bring up their power, but still, it is a big difference for them who do not have anything to begin with.

“……well, I’m good about the half-breed part. What I’m concerned is, whether I who isn’t even a half can really make a contract or not”

Because he hasn’t heard yet how it would turn out if he made a contract, Hiiro became uneasy.

“What, you don’t believe in the words of the Spirit King?”
“Can I really trust the words of the person whom I just met?”

Quit it with the sleep-talking is what he want to say. If someone believed the words of a person whom he just recently met, there is surely something wrong in his head.

 

“Hohoho, You are certainly right about that. Then, how about a test?”

After he said that, he left his place in a slow pace. Hiiro also stood up, and turned his gaze to Nikki and the others.

Don’t tell me, the one I’ll make a contract is you?”
“Hohoho, it would be much appreciated if I do”
“Why not?”
“The current me only has enough power left just to maintain the contract……”

Hiiro could tell situation as he took a glance from Nina’s face tinged with loneliness, similar to what the old man is having right now. Sensing the deep circumstances involved, and because it didn’t matter to him, he changed the topic.
“So, assuming that I can make one, who will be the one that will make a contract with me? Surely, you aren’t talking about that noisy snake woman a while ago, right?”

Hiiro said in detest. He is thinking that he and the princess are somewhat incompatible, to the extent that they will surely quarrel when they come face to face.

“Hohoho. Well, as long as she likes it, then why not?”
“Give me a break”
“Feel relieved. Your partner is not the princess”

Hiiro felt relief hearing those words. If he were to make a contract with the princess, and be together with her for a long time, she will definitely scold him more times than Lilyn.

“Who is it then?”
“It’s Tenn over here”

As he said so, he put his hand on the head of the preoccupied monkey. It seemed that my distance to Tenn lessened, not that I approached him myself.

“……Eh, Eeeeh!? M-meee!?”

 

It seems that Tenn didn’t expect this, as he looks more surprised than me.

 

“You don’t like it? Didn’t you tell me something like wanting to go outside and grow stronger a long time ago?

Eh………ah, well, I did say that but……”

Seeing him catching glances at Hiiro, he seems to be evaluating him.

“Besides, this opportunity may not come again in the future. Will you take it or not?”
“uu……”
“Furthermore, this is Hiiro who is recognized by the whole world  as their shining light?”
“Uuu～”
“You even, didn’t you vaguely feel it?
“……”

As I thought, being praised in this situation is too embarrassing for me. Still, I need to say this one.

(TN NOTE: As you noticed, the POV changed again. The raws, as you have heard, is famous (or should I say notorious) for its inconsistency in POV’s that makes the translators want to curse the author. And if you trace back the previous chapters that I’ve translated, all italicized paragraphs are in first person POV. I usually change these to third-person POV as long as it matches the flow, but for some are even too hard for me so I leave some to first person POV from time to time. Again, I apologize for this)

“Oi, I haven’t said anything about agreeing to make a contract yet”
“Hmmm, you don’t like it?”
“no. ……rather than that tell me first what will happen if I made a contract”

Besides, I won’t be able to take it if I were to be fused with some monkey either.

“Oh yeah, right. Speaking of the contract, what first comes in your mind?
“Don’t turn the question back to me…… Fine. A contract……With that, won’t you be able to gain the ability to handle the spirit like the Beast King?”

In other words, like <Binding Techniques>, you can now use the power of an attribute, in addition to summoning the spirit in itself.

However, Hoozuki denies Hiiro’s answer.

“Wrong. Actually, the meaning of the contract is different from that of Beastmen. Beastmen could already manifest their own power they have been endowed with right from the start. However, in this contract, it is necessary for the person playing the main role to present something which will act as a medium while making the contract”
“A medium?

In other words, in order to change the position of Hiiro and Tenn, First Hiiro shows his things at hand, then it is stamped with blood as a sign of the contract with each other. Then the life force and the magic will begin to flow through the blood at the same time.

What they should be careful here, is that the flow of their power must completely be equal to each other. And the person who is playing the main role must mix the power in a good balance. Putting it simply, it’s an enchantment. If it was enchanted successfully, the Spirit will be able to reside to the article and support the host with its power.

“I see. Then the contract will be established if I were to successfully enchant the powers through the medium well”
“Right on. And from that medium, it will be the linchpin that connects the Spirit to the contractor”

(TN Note: So you’re basically saying that it’s possible to make a wearable harem? Well, I personally have an armor fetish, so where can we start the negotiations with those spirits of yours ( ͡° ͜ʖ ͡°))
“and, what if I fail?”
“Hmmm～ well you’ll just die if you’re unlucky. Oh yeah, the medium will disappear too”

Don’t say something terrible as if you only dropped a candy! It’s death, you know! DEATH!

But if things go well, I’ll be able to obtain the powers of a Spirit. Moreover, a high-ranking one is surely worth trying.

Considerable concentration is needed in order to combine the two powers. Hiiro is confident in using Red Qi, but can’t utilize its utmost capabilities yet.

 

The contract must let someone whom you don’t know share their own power through a contract. The difficulty is absurd as it is. Hiiro understood the reason well why others can’t easily make a one.

“I forgot to ask, will anything be good as a medium?”
“Hmm? No way, right?”
“That is for the spirit to decide, but generally, it should be something that holds a great sentimental value to the host for the spirit to like”
“I see”

After that, Hiiro just remembered what matched his personal belongings for the time being.

“This sword!”

Everyone turned their eyes to the one who made that remark.

“Is there something wrong with the sword, Tenn?”

Hoozuki in behalf of everyone asks him in worry.

“No, what I mean is, if I were to make a contract with that person, then that sword is good for me!”

He seems to be pointing at Hiiro’s Rending Blade・Zangeki.

(TN Note: The sword 《絶刀・ザンゲキ》 was previously translated as Severing Sword・Slasher, then Extreme Slasher by Anri, the last translator before me. In my case, I used the coolest synonyms that I could think of on the respective kanji meanings, then kept the katakana which seems to be the proper name for the sword)

“I can feel my will throbbing to be transmitted to that katana. If it’s that blade, then you’ll have no problems in terms of compatibility with me!”
“Hohou, it’s just what Tenn said. How is it, Hiiro? Will you try it out?”
“Can you wait for a moment?”

Hiiro declared to Tenn and the others once again.

“What is it?”
“Aren’t you lot forcing this to me?”

Tenn then asks something to Hoozuki, then he nods in affirmation.

“Of course. But whether the host’s medium is suitable or not depends on your liking”

Tenn assented with a nod and stared at Hiiro.

“I’d like to go outside and run wildly whenever and wherever I want to. However, I don’t like to be entrusted to this rascal for life either”
“…………”

Hiiro stared back at Tenn, observing him. Seriousness is reflected in those eyes, without any signs of wavering.

“Well, what will you do?”

Strengthened his resolve, Tenn then declared to Hiiro.

“Prove it to me if you really deserve to be my master right here, right now and right away! So fight me, glasses bastard!”

His heart is finally prepared. He also understands his complaint too. Neither want to make a contract with a useless chap. If so, then the contract may be something that heavy for them.

Therefore, Hiiro didn’t bite Tenn’s taunt, and even Tenn didn’t budge too, judging that something trivial like that won’t prove anything but their worthlessness.

 

At first, Hiiro thought that they are forcing the contract with such a stupid-looking monkey, but he seemed to have piqued his interest unexpectedly.

Therefore the small image of him became big instantly, giving him a little surprise.

Obtaining the power of a Spirit is nothing but a rare chance. After the battle against that liger Shishi, with its composition made up of an ‘Anti-Magic Element’, he was looking for a countermeasure for it and found the Grand Red Aura, and if he were to obtain the power of a Spirit, then the possibility of mastering the Grand Red Aura to his body would be easier.

Because the Spirits play a great role in Red Qi, anyone can judge that this a large merit for him.

Even though it’s a little surprising that his opponent is a little monkey, he’s still undoubtedly a high-ranking spirit. Besides the fact that the Spirit King recognized his strength, he also said that he’s qualified to be his partner. For the head of the All-Seeing Race to act as his backer is truly an honor.

“……interesting. I’ll tear you to pieces, you damn monkey!”

Sparks came flying on the stares of the man and the animal.

Feeling that their minds have set and cannot be stopped anymore, Hoozuki stated,

“Since you’ll be fighting, I’ll just set up a barrier here”

 

Hoozuki guided them to an open place. Over there, was the princess who has gone away earlier because she had some business to take, asking them the reason why they came. Upon Hoozuki telling her the reason, the princess breaks into a shock then turns with a ‘Hmph!’ to Hiiro.

“When humans make contract…… they die if they fail you know?”
“Shut up. I’ll do what I want”

With the fact that gaining the Spirits’ power will make him gain the ability to use the Grand Red Aura, Hiiro was invigorated.

Hoozuki then raises both of his hands to the sky, spreading magic power in a dome-like shape.

“You can do it without holding back here”

It was probably a type of magical barrier that restores anything broken inside it as it used to be. Truthfully speaking, Hiiro secretly wants Hoozuki’s magical barrier power more than the power of the Spirit.

At any way, he don’t have the intentions of losing this fight at all. Hiiro doesn’t know how strong the ape was, but he regarded him as an existence equal to Shishi.

But no matter what kind of opponent he will face, he has his all-purpose Word Magic as his weapon. Even if the opponent can use any kind of attack, as long as he doesn’t make a mistake, he will win.

Showing his combat mode made Hoozuki say in surprise.

“Ah, by the way, you won’t be able to use magic inside the barrier you know?”

 

 

…………………………Eh?

———————————————————————————-
Yep, it wasn’t the princess……Still, it seems that it didn’t go as expected. 