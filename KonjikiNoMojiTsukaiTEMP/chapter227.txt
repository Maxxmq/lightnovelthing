Chapter 227 – Light of Naous

The black-robed figure is descending on a long spiral staircase. It was the person who appeared before Portnis a short while ago.

“That damned shithead. He better be watching over her properly”

 

Having gotten the whereabouts of the Light of Naos from Portnis, she was just on her way to find it. She left Bijohnny for the time being to watch over for anyone who may intrude just in case, but she’s very worried whether he could handle the duty seriously.

(But to hide a big sanctuary underground sure is surprising)

This is the hidden stairway right under the altar which leads to the sacred room which is covered by an incredible power that the hero especially left.

 

(For this, magic is disabled in this area. However, in exchange the Hero exchanged his own life for it……)

At the end of the stairs, was a big grandiose door.

“So this is the place……”

She approached the door and tried to push it gently, but seems that it won’t budge even if she pushed it with all her might with it. While she doesn’t feel any magic power in it, it seems that it was locked by something like a thick iron.

“……It doesn’t seem to be operated by normal means. However……”

She took out a single flower from her bosom again. The flower this time is a bud, but for some reason the flower opened wide immediately, revealing its sharp teeth.

“I’m counting on you, Iron Eater Plant”

Several Iron Eater Plants are thrown at the door. Then they began chomping it chunk by chunk, eating the door with great relish.

“Magic can’t be used here, but mine isn’t magic. Well, that’s why I was made to come here in the first place”

Thinking that it will take a while, she sat with her back against the wall.

(Still, it is too quiet in here)

Of course, it will definitely become a problem if there are people in this place, but with not even a soul in sight, her mind calmed down.

(A place far away from conflict……The Sacred Grounds, huh. If the hero really thought of such a thing, then there should be no reason for him to hide this land in the first place)

In the legend, when the hero died, it turned its own body to light and showered upon the ground. Originally, the ground was dirty, a marsh of poison which many ferocious demons wriggled. Wanting to make the land a clean place, the hero squeezed out the last ounce of its strength and purified the said dirtied land.

After that, many flowering plants started to grow in the land, and it became one overflowing in nature. As magic was not usable, for it isn’t even possible to release a tiny amount of magic power, demons were unable to approach the said grounds.

Judging that it was the pure intentions of the hero, the people began calling it Holy Grounds, and erected a building in praise of the hero. This became the Great Temple of Oldine.

(If this is really the hero I’ve been hearing about, then that person should have exerted more effort in widening the area rather than making this cramped space)

The black garmented person stood up slowly, and focused her attention towards a part of the door. There is now a hole enough for one person to fit in. Surrounding it were the Iron Eater Plants belching like an old man who had its full, lying on the floor.

“You all did well”

After passing through the hole she gave those words, and the Iron Eater Plants suddenly dispersed like smoke.

(Now then, why would someone like a hero protect such a place to the extent of using its remaining life, I wonder?)

She went inside, identifying the four stone pillars standing before her. On the center there is something resembling a big container that looks like an object of art, while floating in the air and emitting strange light.

 

(So this is the Light of Naos, huh)

It has an appearance of a fireball. A fireball placed in a round object like a soap bubble. She tried to approach it, but her black robe turned into ash.

“Hmmm, I see, even the weapons enchanted with magic power is useless”

The knives hanging on her waist were lost.

“……is this the dreaded power of protection?”

The black robe revealed an olive green bob cut haired girl with eyes filled with strong will. Furthermore the tip of her hair changes its gradation from yellowish to pale green.

(TL Note: So like Mayura’s hairstyle from Sousei no Onmyouji, only a little bit shorter?)

 

What’s noticing about her is the quite a number of transparent bottles hanging around her waist. Flowers and plants are packed in those bottles.

 

“But to burn the robe that his majesty have given to me with much effort, I’m soo gonna take it on that shithead later”

She put her hand on a small bottle, took out a flower, and placed it on the floor. The flower then grew exponentially, and the girl took a ride on a petal. She gradually approached the Light of Naos on no time at all.

She took another flower again and faced the Naos of Light. The petal then expands and wraps it up gently.

“Good, with this my mission is complete”

 

As if having freed from the midst of darkness, I opened my eyes.  Hearing voices of somebody, it stoke my consciousness awake.

As I raised my body in a hurry, there are the narcissist and a strange girl with filled bottles in her waist before me. While they have seemed to notice me,

“Aah…… can’t you wake up more gracefully? Like this!”

Without changing his smile, Bijohnny languidly sprawled on the floor, and holding his exposed chest,

“Nnn…… good morning. What a wonderful day, isn’t it, Mademoiselle?”

Frankly speaking, I don’t want to watch this anymore. It makes me want to puke a little.

“Just ignore this dimwit. By the way, you over there“

Upon hearing her voice, I found out that she’s the other one in the black robe earlier. For such a girl to do something like a thievery, I turned pale in my face.

“I received the light already”
“Wha!?”

Then she showed a flower. And from the gaps of the petals of the bud, light is gradually released.

“Don’t tell me……No way!?”
“Regretfully, I can’t show you the contents. If I were to show it to you in this place, it will become a problem in various ways, don’t you think?”

No, even if she didn’t confirm it, Protnis was sure. That’s the Light of Naos inside the bulb. The container which is entrusted to them to protect for many years. Her guts is telling her it’s the real deal.

“R-return it immediately!”

That’s why all the more it is to not hand it over to these robbers here.

“Nonononon. That’s not beautiful”

Bijohnny waved his index finger.

“Wha-what are you……”
“In this situation, do you think you who doesn’t know combat can do anything?  Well, it may be beautiful if you are……but, alas! You are inferior to me!”

By and by I felt disgusted. Even though I look like this, I’m still quite young, I always pay good attention to my appearance, and was even praised for being beautiful from the people I met. With this, I feel more or less irritated of the speech and behavior of this man as a woman.

“Sigh*, hey you, if you kept on making him occupied, you’ll just tire yourself out. Rather, you can’t even move from your place, right?”
“Eh?”

By the time she said that, I noticed it for the first time. My body became stiff again. I can’t move. And before I noticed, the girl is already holding a yellow flower in her hand.

“……Ah, this fragrance!?”
“Oh, you finally noticed? That’s right, the fragrance came from this flower. This is the Bewitching Plant. You’re in my illusion now. Ah, by the way, this isn’t magic. It’s just the effect of this fellow”

Seeing her smiling with that creepy grin made me shiver. It was a smile filled with chills even though she’s a little girl.

“Now that’s our business is already done here, let’s pack up. I’ll give you my name in honor of enduring the effect of my Bewitching Plant. One of the Matar Deus, Kainavi. Kainavi Fonia”
“If Kainavi introduces herself, then this one too! The one needed by the heavens, the earth……”
“You already gave your name, shithead!”
“Ahaha! No matter how many times I introduce myself, you can’t deny its beauty! So Kainavi, are you ready to make your disappointed face smile?”
“Huuh!?”
“You see …………This Bijohnny can’t lose to a beau-jenny…… No matter what, Bijonny won’t give in……fufufu! Ahaha! Get it? You have witnessed rumored beautiful joke of Bijohnny!”

(TL Note: I know, I know, the beau-jenny joke is as shit as it is, but it tried my best. The raw is ビジョニーはビジョニ負けない…, (Bijoni wa bijo ni makenai) where the 2nd one (bijo) means beautiful woman while (ni) is just a participle. Basically he just made a knock knock joke. A bad knock knock joke, without the knock knock. Well, every knock knock joke is shitty in the first place)
“……okay okay, we get it. Just don’t make my headache worse”

 

Portnis agreed to Kainavi from the bottom of her heart. Leaving aside Bijohnny dancing gleefuly, Kainavi straightened her appearance and turned her gaze back to Portnis.

With this we’ll part ways. When you open your eyes, the common sense far from the norm awaits”

Unable to resist the siege of drowsiness, her eyelids drooped, and her vision was wrapped in darkness.

 

 

 

 

 

When I woke up, I was lying on a chair. Was it all just a dream? I heard the noises of the usual construction and the voices of people from the outside.

However, my consciousness is warning me like a loud siren upon this overwhelming sense of reality.

While cold sweat is sprouting in my whole body, I went down the stairway leading to the Sacred Room immediately.

A hole on the door as if a fruit eaten by a worm. And the object of art which they had protected in their entire existence……was lost.

(Aaah……I’m very sorry, Ronise-sama……)

I can only hung my head down in regret.  The scenery which is always constant outside, projecting their daily lives.

From now on, the common knowledge of this scenery will change. Being protected by the power of the hero doesn’t change. But the one the hero was trying to protect? It didn’t exist here any longer.

As Kainavi had said, only Portnis who knows this will pass this hidden truth from now on. 