Chapter 228 – A man named Cruzer Gio

Shanjumon Cave. Its location isn’t far away from the Demon Country Xaos, but only a few come here. The reason is that the topography of the interior changes whenever someone enters.

 

“What’s with this strange dungeon?” is what is asked often. Rumors say that the reason why the geographical features change is due to the work of a magic tool.

 

It is said that the magic tool does it in order to prevent its intruders from getting closer to it.

 

Right now, an odd group of three people and one animal had set foot in the said complex cave.

 

“Really, this place is as gloomy as ever”

 

Lilyn clicked her tongue in dissatisfaction. She walked in a uniform speed while her long red hair swayed to and fro.

 

“Nofofofofofo! Careful milady, it’s pretty dark so watch your steps”

 

Leading the three was the perverted butler Silva. Next to him is the pink-haired twin-tailed maid Shamoe.

 

“Aren’t you tired, Mikadzuki-chan?”

“Kui!”

 

Yes, the animal, or you say the bird, is Mikadzuki. With luggage tied on her back, she walked the cave with strong steps.

 

“But it was a good call to bring that bird! With this, we can carry a lot of food!”

 

As Lilyn said so, Mikadzuki raised a cheerful cry as she was being praised.

 

“When Hiiro-sama person-ized her, I was surprised, but I never thought that she could also change into her demon form freely”

“KUIKUI!”

 

As Silva had said, Mikadzuki who was originally a Rydepic obtained the ability to transform into a human through Hiiro’s word magic, but it seemed that she also has a kind of ability to return into her original form.
(TL Note: Mikadzuki’s species was previously named as Raidpic, but since the original is spelled ライドピーク (Raidopiku, pronounced as ride-o-pi-qu), I made it Rydepic, as it sounds closer)

 

Therefore, Lilyn who parted ways from Hiiro and went somewhere brought Mikadzuki as their luggage carrier.

 

She doesn’t say a word of complaint, probably because she’s the same type as Samoe. What’s more, she is even happier of being useful to them.

 

“Still, even though I come here for many times, this mysterious structure never ceases to amaze me. Isn’t it, Silva?”

 

“Yes, milady. Though if the hidden treasure of this Shanjumon Cave is located, it will turn back to being an ordinary cave”

 

The cave is simply too vast. Underground, many twists, turns and branching roads appear like an ant nest. If you lose your way even once, you’ll never be able to return back alive.

 

“Even for me who is a ‘defective spirit’, no matter how complex the path is, we’ll never lose our whereabouts, but I can’t see where the treasure is at all”

“Hmph, That guy really likes living in such a troublesome place”

“Agreed. We are getting close now, milady”

 

In order to soothe the bad mood of his master, he pointed out on one of the passages.

 

“Straight ahead and we’ll be on our destined residence”

 

“Well, what are we waiting for? Let’s hurry”

 

When the three people and one animal went through the passage,
“Kishaaaaaaaaaaaaa!”

 

An eerie cry was heard all of a sudden, and something fell from the ceiling above.

 

“Oh, a swarm of red spiders”

 

“Hmph. It is common sense that a red spider which is classified as a unique monster does not act in groups. Is this because this place is also twisted by that mysterious power?”

 

“How would I know? Still, this is a waste of time. Step back”

 

Lilyn said so as she stepped in front of everyone.

 

Like its name, the red spider is a spider dyed red like blood. However, its size is bigger than a human being. If an ordinary person were to meet this thing, he/she should make escape his/her first priority.

 

But Lilyn twisted her lips to a grin.
“Now then, let’s get this done in a minute”

 

Said Lilyn, with eyes glowing in red. The ferocious demons encircling her began to step back, as if fearing the existence if the small girl.

 

However, one didn’t give in to its fear and with a piercing screech, jumped at Lilyn.

 

BUSHU!

 

In just a split second, a huge golden spike pierced the body of the red spider, skewering it on the ground.

 

“Gi……Gyaa……ga……!?”

 

It was still alive, but one after another, countless spikes which came out of nowhere pierced the whole body of the red spider. It breathed its last in no time.

 

Lilyn who was just standing doing nothing sneered and crossed her arms. Frightened by the scene, as the other red spiders begun to retreat all at once,

 

One of them was wrapped up in flames, and the other was its entire body coated with ice, all at the same time. That strange phenomenon happened to the other spiders one after another, and one by one the demons died.

 

“What, one minute hasn’t passed yet, you know?”

 

She pouted to the unsatisfactory result, and with a short sigh,

 

SNAP!

 

A snap of a finger resounded. Like a glass shattering, the disastrous scenery of the swarm of red spiders vanished in front of Lilyn.

 

CLAP CLAP CLAP CLAP.

 

 

Someone’s clapping was heard. It came from the passage the one they intended to enter a while ago.

 

“As expected of the Red Rose Witch. Even my Fantasia Magic is just a piece of cake for you”

 

A slender man with yellow hair stood there, smiling. Not a single trace of strength could be felt from him, to the extent that makes someone think “why is a weak man like him doing in this dangerous place?”, if not for the katana hanging on his waist. But what’s really standing out are the beast ears on his head.

 

“Can you just quit living in this place already……Cruzer?”

 

 

 

 
The said man is Cruzer Geo, a strange fellow living in the strange place called Shanjumon Cave. According to him, he came and live here in order to avoid disturbance from the outside as he does his favorite things.

 

Cruzer is a blacksmith. Only a few knew his appearance, but his name and creations are famous in all parts of this world.

 

Master Blacksmith Cruzer. There is no one in the armament working population who doesn’t know his name and his weapons. It is said that his weapons cannot be matched even by first-class blacksmiths in the world.

 

His creations are so highly appreciated that no one in this word doesn’t desire them. It is so rule-breaking that in this age, the camp who gets the most of his works will surely bring victory.

 

Therefore all the races sought him with bloodshot eyes. Though the fact of three kings of the world chasing after one beastman is strange, but considering his ability, it was convincing enough.

 

Let’s give an example. Can you cut iron with a sword? This could be a yes or a no. If a person with considerable skill handles it, he/she could even cut it in one swing.

 

Then how about a namby-pamby old man? A kid? Is it possible for their trembling arms to cut that hard metal with a sword? Nearly impossible. No matter how well made the sword is, it won’t be able to achieve its full potential if the one who handles it isn’t skilled enough.

 

However, Cruzer’s blades are a different league of their won. The user doesn’t need any skill. Just make the sword touch the iron, let it go and it will cut through it just by its own weight like tofu.
No matter how exceptionally talented the others be, no one was able to reproduce that incredible sharpness to a blade like Cruzer.

 

Cruzer’s swords were then succeeded by whips that can be moved freely depending on the will of the user, then axes that produce explosion just by touching, and brought the world to a storm.

 

 

However, after witnessing the tragic consequence of the weapons he had made, he tried to stop making weapons anymore. However, calculating how disastrous it is if they’re to lose him, the kings chased after him one after another, and as a result of running around, he ended up in Shanjumon Cave.

 

Now, he makes a living by making and selling things used for housework such as kitchen knives. Of course, he comes out disguised while selling his works in Xaos.
(TL Note: Does he also make a set of orihalcum kitchen knives?)

 

“By the way Cruzer, I need you to do me a favor”

 

Lilyn and the others went to his home to see him face to face. Although it’s more of a cave than a home, in order to be easier to live in, he only finished one cellar. Perhaps the other areas around here are specialized for his personal manufacturing.

 

In the room, a mat similar to a tatami was spread on the floor, and there stood a low dining table and a bookshelf. Lilyn, who hasn’t even touched the served tea thrusted the main business at hand immediately.

 

 

Sipping the tea in a calm demeanor without breaking his composure, Cruzer began to talk.

 

“Lilyn-san, as I have said before, I refuse”

“Why? This isn’t unprofitable even for a bastard like you, you know?”

 

She bluntly told with raised eyebrows, but Cruzer who’s expression didn’t even change a little,
“You said you’ll make “a place where all people can enjoy”. Sure, it’s an attractive proposal, but that will be impossible”

“Y-You can’t say it’s impossible if you don’t even try!”

 

She beat the desk in reflex. If it was the ordinary, a crack would be made, or worse, it may get destroyed, but not even a scratch was made on the table. This only proves that its strength is so excellent that it’s aggravating. Or as expected of a Cruzer product?

 

“Though I’m a little ignorant of the present situations, I can still understand that the world doesn’t yet desire peace.”

 

Which means that they still desire for war.

 

“And also, the children that I’ve made are also being taken good care of for that war. Well, whether they can take care of it or not depends on their users”

 

His smile turned to something shaded with self-ridicule. The “children” are referring to the weapons he created.

 

“I realized how stupid I was. Of course, I don’t regret that I made those children. However……about how they handle my children, and the places they have gone through……I can’t bear to see it anymore”

 

Regret and sorrow filled those words. Though he doesn’t regret making them, he probably regretted the fact of handing it to the people who came to buy the weapons.

 

After selling it to those merchants, he who was young that time thought that his weapons will become the foundation to acquire peace in the world. Well, it could be said that he only wanted to see his children’s figures fly and reach world.

 

However, the merchant who couldn’t find the true value of the weapon simply sold it off for a sum of gold. And as a result, the weapon reached the eyes of the king, and was utilized for war.

 

“I was really stupid. If I just think about it even a little, I should have understood that my children will surely be used for war. However, I was raising my head too high and didn’t see what’s beneath me” 