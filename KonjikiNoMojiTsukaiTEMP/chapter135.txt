CHAPTER 135 - THE START OF HIS DREAM

As Hiiro left the room, someone spoke to him out of the blue, while wearing an amused smile.


“You were quite the bully out there, you know? To the baby chicks who have yet to know anything of this world” (???)


“.....So you have returned” (Hiiro)



The person who called out to him was none other than Liliyn, AKA Aka-Loli



“Why do you look so irritated? Usually, you would have left them without even hearing a single word from them.” (Liliyn)


“.....I guess so.” (Hiiro)


“..... By the way, how long are you going to keep that appearance?” (Liliyn)


“Huh?” (Hiiro)


“Well as far as I’m concerned, I don’t mind it either way. Kukuku” (Liliyn)


Then, Hiiro finally remembered that he was still in his human form, which he seemed to have forgotten about , since he had become a little emotional. In regards to Liliyn, he did not have any problems about his form, as she had seen it half a year ago. And he was glad she mentioned it, things would get troublesome if he were to go out like this. 



Hiiro used the character 『Change』|『化』 to transform into an 『Evila』 again, and went straight outside the inn. Although he did not know when they returned, outside the inn was Mikazuki, the Hentai gentleman known as Silva and the clumsy maid Shamoe.


“Nofofofofofo! Well, well, it’s been a long time Hiiro-sama!” (Silva)


“I, I-i-i-i-i-i-i-i apologize for my long leave!” (Shamoe)


“Ah” (Hiiro)


He gave a half-hearted reply, the two people who looked at him seemed puzzled. And Silva approached Liliyn, [Did something happen? ], and then heared about the circumstances.


“I’m amazed at the cowardliness of those people from your district” (Liliyn)


Liliyn approached Hiiro as she spoke.



Hiiro inhaled and exhaled. Even he was not able to understand why he felt resentment towards those two women. Maybe because a part of his hasty inference still lingered, or maybe he just felt sick when he considered the fact of being summoned alongside such people.



He merely wanted to release the stress he was holding in from that time. Hiiro chose to tread along a dangerous road, live that way, and he found pride in it. So much that he said to them “I can live by myself.”


They were protected by the country, respected and trusted by its people, only to be betrayed by the very same people. This situation was far too similar to a tragic heroine tale, stripped of all grounds to stand on, and so for them to request for help was naturally inevitable.


Hiiro was unable to comprehend their naivety. Those words “ we need help”. They honestly believed that someone would help them just cause they asked. 


They didn’t even consider the fact that there are some who would not aid them even if asked for help. Even if they shouted for help, they simply would be ignored, or their plea’s would not even be registered. But still, they did not recognize those people whom they could possibly cry for help.


They had not even experienced failure, neither despair, but the easy life for them ended now. If they didn’t change, they won’t be able to pay their bills for living that easy life they had. Hiiro never expected them to ask him for help.


“ I thought that those idiots were fools, to think they were this incompetent.” (Hiiro)


“Guess there is no helping it, those little seedlings haven’t experienced our journey. They do not understand the world we live in, not to mention the fact that we are at war right now.” (Liliyn) 


Kukuku, Hiiro furrowed his eyes as he stared at the woman who was happily laughing, sighing as he thought that these guys have the same bad taste for their laughter.


“Well, just leave them be. The Demon Lord here is gentle. Even if they are found out, they won’t be killed immediately. ” (Liliyn)


Hiiro thought over Liliyn’s words. He did indeed truly felt that Eveam was an unusual Demon Lord for a war. Showing mercy to the enemy that instigated it, even Hiiro did not understand her way of thinking. Although she said that she would use them for negotiations in the future, but to Hiiro it sounded like an excuse for not dirtying her image.


Even so, Hiiro still respected anyone’s way of life. It was one of his noble virtues, that was only if the situation was at his favour. In fact, if he had simply delivered the finishing blow to Crouch that time, Crouch would not have had a chance to use his last-resort on Hiiro.


“The rulers of this world are all idiots.” (Hiiro)



The 『Humas』 ruler Rudolf was a completely fool of a King. The Demon Lord was an inexperienced bunhead. Although, he had confronted the Beast King, Hiiro understood that he was nothing more than a foolish Battle Junkie.



“Kukuku, in any country, all the rulers and it’s colleagues are fools. I would to establish a new country rather settle in one of these.It might be good, right? You can even become a ruler yourself.” (Liliyn)


“Are you kidding me? I’m not interested in that kind of thing” (Hiiro)


“Kukuku, I thought you would say that. But, frankly speaking, it’s not such a bad idea, you know?” (Liliyn)


“Ha?” (Hiiro)


“Right now, none of the countries are upright. And I believe, you know the reason why it happened, right?” (Liliyn)


“The rulers are stupid….rather, the existence of races.” (Hiiro)


“ Thats right, everything starts with different races getting involved in a big cat fight, the reason behind the argument being, different races refuse to recognize each other, and it gets to a point where they could not control the problem itself.” (Liliyn) 


Although in olden times, everybody was struggling side by side.


“Even the current Demon Lord, she is only concerned with the well being of the 『Evila』. Even though her mind is set on, everyone should get along. She is still focused on the wellbeing of the Evila.” (Liliyn)


“I believe that is the most natural thing to do.” (Hiiro)


Everyone prioritises their own rather than someone from another race. This is the way of life in this world.


“But what if there was a country that treated all races equally?” (Liliyn)


“....What did you just say?” (Hiiro)


When Hiiro looked back at Liliyn, she had a content look on her face.


“....Didn’t I say it before? That I have an ambition.” (Liliyn)


“... Nope, I only heard it from Jii-san and Doji-maid a few times.” (Hiiro) 


“ I see” (Liliyn)



Liliyn unexpectedly gazed at the clouds in the sky with a distant look in her eyes.


“This ambition.... The ambition I have for myself is….” (Liliyn)


“..........” (Hiiro)


“.....I want to establish 【A Place for all to Enjoy】.” (Liliyn)


“ To Enjoy?” (Hiiro)


“Yeah, everyone has their own preferences right?” (Liliyn)


“Yeah.” (Hiiro)


“I want to make a place that anyone can enjoy without getting bored.” (Liliyn)


Honestly, Hiiro was surprised. Liliyn was pretty much a selfish person, he did not expect the notion of her being considerate of others.


“You bastard, you just thought of something rude, didn’t you?” (Liliyn)


“....Who knows.” (Hiiro)


He thought, this fellow is as sharp as ever.


“Okay then? What is the reason for you to establish a place like that?” (Hiiro)


“Ha? It’s because the idea itself is very interesting.” (Liliyn)


“.....Ha?” (Hiiro)


“Think about it. We are talking about the whole nation, right? 『Humas』,『Gabranth』,『Evila』 and 『Pheom』 gathering in one place to endeavor in pleasure and amusement! Such as magic tournaments, physical strength matches, wisdom competitions and fast eating contests. Don’t you think it’s interesting-?! “ (Liliyn)


Hiiro received the impact of her idea. Certainly, it will surely be something like a dream-like story in the present situation. However, if that ambition were to come true, Hiiro would surely want to see it happen.


“Then, I recommend a marathon and cooking showdown as well.” (Hiiro)


“OH! As to be expected fromof Hiiro! That is an interesting idea! I surely should surely reward the finest cooking that I have yet to seen yet!” (Liliyn)


Kuhaha, Hiiro glaredeyed at the woman who was laughinged quite happily., Iit was probably the first time Hiiro felt a sliver of respect  sprout out in him for Liliyn.


“.... I see, 【A Place for all to Enjoy】...huh?” (Hiiro)


“Well, I thought that, it would beis more interesting to let all the people live in a rather big land, without establishing something troublesome as a country, kuhahahaha! ” (Liliyn)


“.....I just realized something for the first time too.” (Hiiro)


“Hm? What do you mean?” (Liliyn)


Why did Silva and Shamoe trust Liliyn? Both of them are a existence treated as a black sheep in this world. But, after Hiiro heard Liliyn’s ambition, he understood that they might have been attracted to her and felt a glimmer of hope for their future.


Moreover, she said the whole nation…. in other words, the 『Demon Beasts』 or the taboo race of halves, and even the heretic 『Spirits』 were all the people included in this category, and Liliyn looked at them without discrimination. This was probably the reason why Silva and Shamoe were attracted to her.



“.... It’s nothing.” (Hiiro)


“Is that so? Well, leaving that aside, about my story awhile ago. Supposing that country is established, Won’t you become the King, Hiiro?” (Liliyn)


“I already told you, right? I have no interest in becoming a King” (Hiiro)


“Mu….Mu.” (Liliyn)


She sulked a little and glared at Hiiro.


“If you found a nation, then shouldn’t you be leading it?” (Hiiro)


“Why would I become such a person? It’s quite tiresome.” (Liliyn)


“Then do not force your ideals onto me, if it’s tiresome, idiot!” (Hiiro)


“EhNa?! Who is the idiot huh?!” (Liliyn)


Hise jaw droppedloosened his cheek a little as he glanced at the indignant face of the woman.


“But, you know..…. I kind of want to see what becomes of your dream” (Hiiro)


“Hee….Ah… Is that so?” (Liliyn)



Liliyn turned her flushed face in embarrassment as she suddenly saw Hiiro’s smile.


(Interesting. The way this guy thinks is really interesting. If such place really existed, then I want to see it) (Hiiro)


However, her ambition was not something that would come to being overnight, it takes years to be established. If they managed it untactfully then Hiiro might not possibly witness it while he was still alive.


(Oh crap. I’ve began to seriously consider about perpetual youth) (Hiiro)


Although the thought of becoming inhuman is still out of the question, if possible, he wanted to see the beings of this world work hard for Olympics and sports-like events similar to the people who lived in earth.


Her idea was really interesting. He ascertained himself to become one of the judges for the cooking showdown. He had a relieved expression as he found a hope he would look forward to in the future.


However, the road would be a long and steep one, he thought as he looked around the tattered town, damaged by the war brought to the country.


However, the road would be a long and steep one. Hiiro surveyed the worn-out town caused by the war again.


“A Dream… huh?” (Hiiro)


“N? What’s wrong?” (Liliyn)


“Hm, after coming to this world, this is the first I have considered about something like a dream” (Hiiro)


“A Dream? Do you also have a dream?” (Liliyn)


“That’s rude. Besides, I told you a while ago you know?” (Hiiro)


“Eh?” (Hiiro)


“My dream is to see your dream come true with my very own eyes”  (Hiiro)


“.... A-Are you alright with that? That sort of dream” (Liliyn)


Hiiro never thought of the details of his dream. He only felt the trance of wanting to dream.


“I do not mind it, I only wantto see it. The sort of 【Edea】 you had in mind” (Hiiro)


“I-I see….. I understand!” (Liliyn)



Liliyn who looked like an enthusiastic child expressed a full bloomed smile



“Nevertheless Aka-Loli, this path is absurdly difficult” (Hiiro)



For a moment, she stared at Hiiro’s word in puzzlement, then she immediately laughed fearlessly while she crossed her arms.


“Humph, That’s where I want to be! The word impossible is not in my dictionary! kuhahahahaha! “ (Liliyn)


“..... I see. So you are prepared for it?” (Hiiro)


“Of course!” (Liliyn)


“Then, I will lend a hand as well” (Hiiro)


Thereupon, Liliyn became flabbergasted. She did not expect that Hiiro would be so quick on deciding to help her.


For this reason, she had been worried on how she would coerce him in the future, as she usually hardens on the thought of persuading him, which made her dodge the thought unintentionally. But due to his words, she couldn’t help but think that Hiiro was quite a strange guy.


“That’s natural! Y-you are after all my possesion! It’s natural that you would help me!” (Liliyn)


Although Liliyn thrusted her fingers at him, she was undeniably glad from the bottom of her heart as a smile starts spilling over her face. She frantically tried to hide her embarrassment by shouting.


“Don’t joke around. This is received to the last as a request. The compensation is hmm….. How about freedom at that place?” (Hiiro)


“D-Did you say Freedom?..... What are you planning?” (Liliyn)


“I’m not sure. Should I let you know? Or maybe I shouldn’t?” (Hiiro)


“Mumuu….. Eei! Do not make light of me! Even I can give freedom to such a place!” (Liliyn)


“Heh, then promise established” (Hiiro)


Even if she were make an excuse later, he could show this memory using the character 『映』|『Project』. And if she still refused to agree then he would use his 《Word Magic》 on her to forcibly listen.


Actually, when he first came to this country, rather, when he started traveling with Liliyn, everything had been really good. He did not think that, he would find a dream to fulfill through it.


Of course he dreamt of touring around 【Edea】, but if he was pushed to say then, that was just Hiiro’s extra innings that can be addressed as a hobby.


This dream of Hiiro was, indeed, boundlessly difficult in this world. However, he had made up his mind that he would realize his dream without fail.


(Therefore, while I support Aka-Loli, I must do something about this war first…) (Hiiro)


Yes, a certain resolution was essential for Hiiro’s dream. And that would be to end the quarrel between the three countries or to lead them to a new direction. He understood that it would be difficult due to the danger it imposed. Still, he decided to do something about it despite all the difficulties.


(That is right, I should do what I have to do. For that reason, I can do nothing but work without rubbing it)(Hiiro)


Even Hiiro did not expect that he would make such a decision for himself as his heart was strangely cheerful. It would seem this was the favour he would receive after spending all his days lethargic.


(Even if things would get troublesome, this is worth the effort) (Hiiro)


He kept his determination to himself as he looked up into the sky.


Although he was in a good mood, apart from that, he turned his gaze to Nikki who was joyfully smiling while playing with Mikazuki.


“Oi Baka-deshi, you will do a one hour-seiza later” (Hiiro)


“W-Why is it?!” (Nikki)


“This is the punishment for not reminding me about those fools” (Hiiro)


“T-that is.. you see...u...uu…” (Nikki)


“Seiza later” (Hiiro)


“......Yes” (Nikki)


Since Nikki really forgot about reminding Hiiro, Nikki could do nothing but nod without objection.


“Ahaha! Nikki made Master angry~!” (Mikazuki)



Next to Nikki, who saw her down hearted, Mikazuki joyfully pointed her fingers at her. But her smile freezed instantly on hearing Hiiro’s following words.


“You are the same, Drool Bird” (Hiiro)


“.....fue?” (Mikazuki)


Mikazuki’s smile hardened.


“Previously, when I was talking, you were annoying. This the crime you have committed” (Hiiro)


“ N-No way! That’s mean Masteeeeeeer!” (Mikazuki)


“Shut up. Do what I have told you to. You will have no meal for the time being if you refuse” (Hiiro)


“Yes! Mikazuki will gladly eat obey!” (Mikazuki)


When Mikazuki hears her meals being pulled out, her attitude changed immediately. It was evident that she really hated her meals being pulled out.


“Kui…… this is also because of your fault Nikki” (Mikazuki)


“You’re wrong~zo. This is the consequence of your deeds” (Nikki)


“That is not true~mon! The reason I had become annoying wa because of Nikki~mon!” (Mikazuki)


“Mu…. Even I as well wouldn’t be in this situation if it weren’t for you Mikazuki” (Nikki)


“Ahh! That is not true~mon! Compared to Nikki, Master takes care of my body~mon!” (Mikazuki)


“Ah….mumumu” (Nikki)


“Hehe~n! BA~KA, Forgetful Nikki, boooo~” (Mikazuki)


“Uuu, this is so mortifying!” (Nikki)



Hiiro had a cramp on his cheek while hearing the communication of the two people.


“Maybe I should make it two hours instead?” (Hiiro)



Hearing his words, both of them felt a shiver run down their spine.



“ “ We won’t be annoying anymore!” ” (Mikazuki & Nikki)


The two of them cheerfully answered.
 