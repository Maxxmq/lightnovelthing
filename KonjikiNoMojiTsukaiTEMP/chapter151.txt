CHAPTER 151 - AFTER A LONG TIME, EVILA CONFERENCE 

Presently, in the【Demon Capital - Xaos】, an『Evila』 conference was finally being held after a long time. Nevertheless, they had vacancier from their usual lineup.



Kiria, who was the Demon Lord Eveam’s aide, had betrayed Eveam, and, to make things worse, Greyald of 《Rank 6》was killed.



Only the Demon Lord and four people of the 《Cruel》 were now in this location.



“Everyone, the purpose of this gathering is to decide our future, as well as to understand the condition of our country and our people.” (Eveam)



The four silently returned a nod to Eveam’s words respectively.


“But Your Majesty, are the contents written in the paper true?” (Marione)

                                                 

Marione said those words while gazing at the sheet of paper in front of Eveam. The paper was what Teckil had entrusted Judom with. Hiiro, who had received the sheet of paper, handed it to Eveam directly.



“Ah yes, I definitely sensed Teckil’s magic. Furthermore, at the present time, I cannot contact Teckil. Perhaps, most likely, Teckil has already been caught as written here. And the one who seized him was……” (Eveam)



She had a relentless look as she heavily moved her lips.



“The former Demon Lord, Avoros Gran Early Evening.” (Eveam)



Marione, who had hit the table with a don!, then said-



“Isn't that just a mistake or something? Aquinas and I were the one who confirmed the corpse of the previous Demon Lord, you know? There wasn’t anything strange about it. Isn’t that right, Aquinas?” (Marione)


“……Yeah.” (Aquinas)


“His 《Demon Core》 which is also known as the second heart of an『Evila』 had been destroyed. With that gone, it’s impossible for him to revive any longer.” (Marione)


“That’s true but…… What do you think, Aquinas?” (Eveam)



Eveam turned her gaze towards Aquinas.



“.....His death was, indeed, confirmed by these two eyes of mine. That was definitely a corpse. And, it was not a doll Kiria made either” (Aquinas)


“That’s right. No one can escape from this guy’s pair of eyes; he wouldn’t be able to fake his death.” (Marione)



From Marione’s words, one could understand that he held great trust in Aquinas’ eyes.  


“So, was what Teckil saw a mistake then?” (Ornoth)


“That seems to be the only possibility.” (Shublarz)


“He is the country’s greatest intelligence operative, you know? Teckil may not look like it, but he is next to the two of you who is 《Rank 3》. A half-baked person shouldn’t be able to capture Teckil alive. Unless they were someone of your class.” (Eveam)


“mumu…..” (Marione)


Marione groaned without being able to answer back from the sound argument.


“Besides, there is also the existence that Iraora mentioned, as well as the one that created Kiria for the sake of the conference. I have a feeling these two existences seem to be connected to one another.” (Eveam)


As that was something everyone present had sensed, none of them were capable of refuting her words.


“But, that person may not necessarily be the former Demon Lord. Even that paper only wrote that the possibility was high.” (Ornoth)


“It’s true that the details that Teckil wrote on this paper were, indeed, all of his ideas that lead to this conclusion. But, he said that he saw that guy, right? Even though he should have died…………he saw Teritorial’s face!” (Eveam)


The room fell silent. It was a testament to the weight that her words had carried.



“Teritorial….. the right arm of the former Demon Lord, huh?” (Aquinas)



Aquinas spoke, breaking the silence that permeated the room



“Now that you mention it, Their relationship was similar to that of Her Majesty and Kiria's.” (Shublarz)


“It’s true……… However, he died earlier than my elder brother. Rather, he was killed. By none other than my brother's hand!” (Eveam)



The place became quiet once again.


“If the dead was brought back to life, and was manipulated, then only one person is capable of doing it, and that is the former Demon Lord.” (Aquinas)



Everyone turned their gaze at Aquinas’ mutter.



“I agree, it is only Avoros, the 《Necromancer》, who can do it.” (Eveam)


gokuri,  the sound of everyone's throats gulping rang out.



“Supposing that what Teckil saw was Teritorial’s corpse that was being manipulated, then, without a doubt, it would be Avoros’ deed. Moreover, if it was a strong man like Teritorial who defeated Teckil, then I can also agree with this matter. After all, Teritorial was Teckil’s master.” (Aquinas)



As Aquinas said, The person known as Teritorial was Teckil's master. Rather, he may possibly be considered an existence similar to a foster parent. Teckil lost his parents at an early age. One day, he was picked up by Teritorial and became his adopted son.



Everything that Teckil knew was taught to him by Teritorial. And, everyone who was in this place knew that fact. It should be impossible for him to misidentify Teritorial’s face.



However, Teritorial who should have died, lived and captured his very own son. For him to be able to do that, one could only think that he was being manipulated by someone.



And, up to now, there was only one who could manipulate the dead, that person is the former Demon Lord Avoros.



“It is as Aquinas says. Perhaps elder brother……Avoros camouflaged his death by some method, and has lived until now. I do not understand what are his objectives but I’m sure elder brother is the one who planned this war. It’s not surprising if such a guy like him would move within this war” (Eveam)



Even though he was Eveam’s relative, she continued her sharp words about him. Disgust rather than sorrow appeared on her facial expression.



“And from now on, Avoros will undoubtedly move openly in the future. For the sake of his own incomprehensible desires.” (Eveam)


“....I wonder if Kiria was also allied with the former Demon Lord from the beginning.” (Shublarz)


Shublarz spoke her doubt, but when Kiria’s name came out, Eveam showed a dark expression.



“.....I don’t know.” (Eveam)


“Your Majesty….” (Shublarz)


Shublarz muttered anxiously.


“You see…...Kiria was by my side all the time ever since I was a child. For her to be an artificial existence……. Who could’ve thought that…..” (Eveam)



She grasped her fist and trembled.



“She said she was…..Val Kiria.” (Aquinas)



With Aquinas’ words, Eveam nodded feebly.



“In the ancient times, when the『Demon Capital: Xaos』 did not exist yet. A woman who was named the first Demon Lord gathered a large number of our brethren to create a country. However, the 『Evila』 of those days had neither knowledge nor wisdom about building a country. Simply put, their intelligence was low. The founding Demon Lord, who was the only wise one, thought that teaching them carefully one by one would take too much time.” (Aquinas)


Everyone listened to Aquinas’ story. Even Marione was listening attentively to his words for the first time.


“Thereupon, the founding Demon Lord thought that if she made many existences identical to herself, the establishment of the country would advance smoothly.” (Aquinas)


“D-Don’t tell me” (Eveam)


Eveam was wide-eyed on that revelation.



“She called it, 《Val Kiria Series》. They were the excellent doubles of the Demon Lord. Although it seems that she was unable to create too many of them, they led the people and advanced the establishment of the country. And, the result of that is the 【Demon Capital: Xaos】.” (Aquinas)


“This is the first I have heard of such a story? The founding Demon Lord was the one who founded this country by commanding the people at her side as per written on the history books.” (Marione)



Aquinas answered Marione’s doubt.


‘That is indeed so. If problems did not occur, their existence should have been left in the history records.” (Aquinas)


“P-Problems you say?” (Marione)


“Yeah, when this country was founded, some incidents happened soon after.” (Aquinas)


“Incidents……?” (Eveam)


Eveam asked this time.



“ One of the Val Kiria started to run wild without any prior warning.” (Aquinas)
