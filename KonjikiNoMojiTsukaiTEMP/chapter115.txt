CHAPTER 115 - MEETING WITH THE DEMON LORD

" You will hear from them directly? What are you talking about? " (Ornoth)


Ornoth who is 《Rank 4》 of 『Cruel』 had no idea what Okamura Hiiro told him


Hiiro wanted to talk with the Demon Lord, but she is not here in 【Xaos】.Right now, the Demon Lord is at 【Holy Oldine】 for the alliance conference. Talking to her directly seems impossible in the current situation. Primarily because there is quite a considerable distance between 【Xaos】 and 【Holy Oldine】. Ornoth shruged his shoulder in amazement.


There was something that Hiiro needed to ask the Demon Lord about. He wanted to clarify the details of the contract.


Then, Hiiro ignored Ornoth’s monologue and focused magic in both his index fingers.


"Transfer『転移』" & "Demon Lord『魔王』" 


(I should go there quickly and hear the circumstances and return back here again) (Hiiro)


However, Hiiro suddenly realized something


(......ah, I could have just called them from here instead of teleporting to them) (Hiiro)


However,l he has already written the characters. Canceling it would trigger a 《Rebound》so he sighed and said 


" Oi Stupid Disciple, I will go out for a moment, when those guys get back, tell them the current situation " (Hiiro)


" Certaintly ~zo! " (Nikki)


Hiiro activated his 《Word Magic》 after Nikki saluted him.


*pishun~!


Other people except Nikki, were dumbfounded when Hiiro’s disappeared.


" See you soon Master ~zo! " (Nikki)




Scene from the Past


When Okamura Hiiro arrived in 【Xaos】 his companions were out for a moment because they had some private business to attend to. Hiiro had no choice but to take on the task of looking for an inn with Nikki.


However, Hiiro was lost as he walked in the streets of 【Xaos】. This is because the size of the country. Although 【Victorias】 was a big country, 【Xaos】 seems to be much bigger.


【Victorias】 was a country that consisted of many towns. 【Xaos】 as well seems to be identical with two or more town condensed in one place. Hiiro thought it is insufficient to tour 【Xaos】 in just one day.


Inside the town, Hiiro and Nikki were having a hard time looking for the lodging place they were supposed to stay in. They continued looking for a while but they felt hungry in the middle of the search. Thus, Hiiro thought that it can’t be helped and looked for a place  to eat. Forutnately, they quickly found a place to eat


*karan koron 



That was the sound of the door of the coffee shop when a person goes inside. The interior of the shop looked dim and unpopulated. Hiiro found this convenient and proceed to the food counter.


" Ah, Master! I will search for our lodging ~zo! " (Nikki)


" ah? Aren’t you hungry? " (Hiiro)


" Yes ~zo! The smoked meat in Master’s bag a while ago was really delicious! " (Nikki)


*pokan!


" nowa! W.. What are you doing! " (Nikki)


" It’s not What are you doing!  Do not eat my things without my permission! " (Hiiro)


" u~ I’m really sorry ~zo " (Nikki)


Seeing Nikki depressed while hanging one’s head, Hiiro sighed.


" Then, go find our lodging quickly, I might forgive if you do that " (Nikki)


" Y… Yes ~zo! " (Nikki)


Nikki who became enthusiastic once again, ran at full speed. Hiiro entered the store and looked for a place to sit. Although there was a young girl sitting alone in the counter seat, the seat besides her seems to be empty. So Hiiro went to that place.


" What will your order be? " (Shopkeeper)


The shopkeeper appeared


" I’m hungry, serve me with your most delicious dish." (Hiiro)


The young girl noticed Hiiro and gave him a glance due to Hiiro’s bluntness. Hiiro did not mind that glance. Rather she doesn’t seem to exist to Hiiro. 


After a while, Fried rice with large serving of meat was served to Hiiro. Hiiro judged this dish was delicious just from it’s smell and ate the dish quickly.


" Give me another cup " (Hiiro)


That cycle repeated three times.


" fufufu " (???)


Hiiro heard laughter from the young girl who was beside him. Hiiro glanced back at her.


" Ah. I’m sorry, you were eating so well , that I just.. " (Young Girl)


Though Hiiro was puzzled at the young girl’s behaviour, Hiiro returned her glance and ignored her. The shopkeeper who saw that seems to be panicking.


" O… Oi! You!, This person is.. " (Shopkeeper)


" It’s okay " (Young Girl)


" B.. But " (Shopkeeper)


" I said it’s okay, he is a guest here, and I’m a regular customer. Social status have nothing to do with this " (Young Girl)


" …. haa… If you say so " (Shopkeeper)


When Hiiro heard the conversation of the two people, he can understand that the young girl besides him seems to have a high social standing. Assuming, she is an ojousama, Hiiro felt a little admiration for the young girl who came here alone into this dim and unpopulated coffee shop.


Moreover, the young girl was a regular customer. Until now, Hiiro finally noticed the young girl’s appearance. She seems to have a beautiful blonde hair. Her face seems to be proper. She smelled like a fragrant flower. Hiiro judged that lots of men would be attracted to this beautiful lady.


Hiiro was also surprised because this sort of beautiful woman came here alone into this dim shop and what’s more she is a regular costumer. But Hiiro returned to his meal instantly for he isn’t related to them.


*karan koron


The shop keeper moved his attention to the new guest. Though it seems to be a guest, three bad looking guys entered the store.


" Oo! Thi…. This is! There is a woman here! ~" (Man A)


One of the three notice the lady’s appearance and raised his voice.


" O!.. That’s nice~  " (Man B)


" Moreover, she’s a super beautiful lady! ~" (Man C)


They puffed their noses in an aroused way and approached the girl.


" U… Uhm Customers! " (Shopkeeper)


" Shut up Shop keeper! Be silent for now " (Man A)


" Un Un… If you don’t, we might kill you " (Man B)


" gyahahaha" (Man C)



The storekeeper went silent because of their words. And returned their glance to the young girl. One hand was quietly put in the young girl’s shoulder and the shopkeeper turned pale in that moment.


" naa naa Nee~chan, Do you want to have a good time with us? " (Man A)


" Un.. Un.. If that’s the case, we’ll treat you gently ~" (Man B)


" hou, you have quite an energetic spirit " (Young Girl)


The lady quietly answers the two people who talked to her.


" gyahahaha. Our spirits are full of vigor, well how about it? Do you want to play? " (Man C)


" I’m sorry, after this I have a private business to attend to " (Young Girl)


" Then just skip out on it~ " (Man A)


" Un… Un… This is a much better thing to do We’ll show you lots of things that will make you feel good~" (Man B)


" Gyahahaha! , we'll cl*ck you so hard, you won't even be able to stand the next day!  Gyahahaha! " (Man C)


Vulgar laughter resonates throughout the store. The shopkeeper became paler due to the current situation.


" I will have to refuse whatever you have to say. Besides, you guys don’t seem to be a resident to this country? " (Young Girl)



" Ah? Well, that’s right. We just came here yesterday " (Man A)


" As expected, because it’s my first time encountering such vulgar people in this country" (Young Girl)


The three people raised their eyebrow all of a sudden due to the young girl’s way of talking.


" …. ha? Oi Nee~chan, you shouldn’t be reckless with us you know? ~ " (Pervert A)


" Un.. Un.. The good feeling might become painful ~~ " (Pervert B)


" Gyahahaha! So just become obedient and play with us? " (Pervert C)


Suddenly, the hand on the young girl’s shoulder was beaten downwards.


" What are you guys doing? " (Young Girl)

　

The young girl suddenly stands up and glared at the three of them. It seems that she is angry but it cannot be confirmed because her face seems to be hidden behind her long hair.


" You people are of the『Evila』! As an 『Evila』, you should be proud of being one! " (Young Girl)


The three people were taken aback by the young girl’s outburst.



" " " Gyahahahahaha! " " " (Pervert A, B, C)


They made fun of the young girl while laughing to their hearts content.


" This life.. I should be proud of?, Bullshit! There is no value in it! " (Pervert A)


" Un.. Un… what an interesting remark ehehe~~ " (Pervert B)


" Gyahahahahahaha! I think I’m getting a stomach ache! " (Pervert C)


When the young girl saw their action, she strongly grasped her fist, the shopkeeper seems to have become more paler due the current situation. However, 


" Shut Up, Bullshit Trio " (Hiiro)



Hiiro who was docile until now, couldn’t endure and complained. And because of these vulgar people that Hiiro lost his appetite.


" Aa? Hey hey, Are you talking about us? ~" (Bullshit A)


It was clear that the only ones in this store is Hiiro and them. It was sarcastic and disrespectful, and if Hiiro didn’t fight back he wouldn’t be Hiiro.


" There is no one else here. I’m talking about but you guys? Or is it possibly you want me to rename you guys as Garbage Trio ? " (Hiiro)


" W.. What did you said !? " (Garbage A)


" Get out now. Because of you I lost my appetite, I don’t want to have a garbage existing beside me. " (Hiiro)


From Hiiro’s sarcasm, he earned the glares of the three angry people.


" Hey Red Robe, do you want to die? huh? " (Garbage A)


" What are you doing?That person doesn’t have anything to do with this! " (Young Girl)


The other two started to walk toward Hiiro; the young girl tried to obstruct them but was pushed aside. It was clear that they were not going to head out. This caused Hiiro to let out sigh. 


" ku! " (Young Girl)


" Nee~chan, afterwards let’s have some fun, but before that …. ~ " (Garbage A)


However, Hiiro averts his glance from the man, the storekeeper is seen.


" Oi shopkeeper, in this situation right now, if I was able to drive out the Garbage Trio in this cafe, can I have this meal for free " (Hiiro)


" Ehh….. Ah…  I don’t mind but... " (Shopkeeper)


" Good, agreement settled " (Hiiro)


Hiiro then chuckled after that.


" Let’s go out. I will be your opponent Garbage Trio " (Hiiro)


" Bring it on Red Robe! Don’t complain if we kill you! " (Garbage A)


Fortunately, the street doesn’t seem to be populated and all of them went into an alley and Hiiro faced the Garbage Trio.


" Don’t tell me you three people will fight me at the same time, isn’t it unfair? " (Hiiro)


The three people grinned and Hiiro found it troublesome to wait for an answer.


" Let’s skip the talk, Come at me Garbage Trio! " (Hiiro)


" W.. We will kill you! " (Garbage Trio)


Three people attacked together.


" B.. Be careful! " (Young Girl)


The young girl shouted, because she saw the three people had knives. However, Hiiro sighed. Then Hiiro drew magic with both his index finger and quickly wrote the character.


" Can you manage skydiving without a parachute? " (Hiiro)


Suddenly, Hiiro disappeared along with the three people.


The young girl who witnessed it forgot to blink her eyes in amazement. Then she perceived a considerable magic appear above her, and looked up.


Four people were now suspended the sky.


" Eh? Ah? Oh? Na… naaaaaaaaa !? " (Garbage A)


" What the heck is thiiiiiiiiiiiiiis !? " (Garbage B)


" Howaaaaaaa!? high! Too high! I will die from this height " (Garbage C)


Hiiro did not blame them for being surprised. Previously there were on the alley but right now they are suspended in the sky. Moreover, they were considerably high up.


" Yo, how is it? This will be your first time sky diving, right? " (Hiiro)


With a evil grin, Hiiro shot those words at the three people who were floating in the air.


" Y… You! D.. What did you do! Th.. This…! " (Garbage A)


Hiiro did not understand what they are trying to tell him. But one thing for sure, they are frightened little shits right now.


" Well, even if you guys have a rotten personality you are a still an 『Evila』, even from falling, you will only sustain a serious injury…… that is if you are lucky? " (Hiiro)


" I-If we are lucky-?! Uwaaaaaaaaaa! " (Garbage A)


" Bye " (Hiiro)

*pishun


Hiiro disappeared again.


" " " Noooooooooooooooooooooooooooooooooooo! " " " (Garbage Trio)


Hiiro returned to his former position and faced the shopkeeper.


" I told them not to come here again. And as agreed, this meal is free of charge right? " (Hiiro)


" eh.. ah.. Yeah " (Shopkeeper)


Hiiro then heard something crash outside and knew that those three were able to kiss the ground splendidly.


By the way, the character Hiiro used a little while ago,

"Transfer『転移』" & "Four People『四人』"

He returned with the  same character "Transfer『転移』" that he conjured with his magic.


Hiiro was satisfied from his full belly and started to leave the place from there.


*gashi!


Hiiro’s arm was grabbed by the young girl


" What do you want? " (Hiiro)


Hiiro expected the young girl to return her expression of gratitude but was taken aback by her following words.


" I… I’m the Demon Lord of this country! Th.. That’s why, Please let me treat you to any drink you like!! " (Young Girl)


Hiiro thought that there is something wrong with the head of this young girl after all. 