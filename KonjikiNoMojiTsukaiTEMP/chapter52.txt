Chapter 52: Arnold and Company’s Suspicions

A large amount of soldiers occupied the garden’s entrance.



"What the hell happened here!?"



Arnold’s group, who had rushed here after they heard a sharp cry, widened their eyes in wonder. As Kukklia saw what happened, she involuntarily shouted with a loud voice.


They couldn’t believe that something would occur in the garden. Thinking that Mimiru could’ve gotten involved caused Kukklia to become frustrated. She had to first ascertain Mimiru’s safety.



Kukklia: "Mimiru? Where’s Mimiru?"

Soldier: "Ah, Kukklia-sama(1)! A-actually..."



Something had suddenly occurred to Kukklia. If something unfortunate had indeed happened to Mimiru, everyone's face would have been colored by despair. However, looking at their faces, Kukklia was unable to hide her confusion.


The reason for this was because there was nobody shedding tears. On the contrary, people were displaying expressions of joy. No, if you actually looked closer, tears were being shed by several people.



Kukklia: (W-what in the world happened here?)



Kukklia was taken aback by the soldiers’ strange appearance. She tilted her head, wondering whether something worth rejoicing so frantically over really occurred here. But first, she had to find the cause of this commotion.



Kukklia: “What’s wrong? Why’re you making such an expression?"

Soldier: "Mimiru-sama...... Mimiru-sama is......Uu~......"

Kukklia: "Aa~ Mou~!(2) Speak clearly, damn it! What happened to Mimiru?! Actually, where is she?!"

Mimiru: "I’m here, Kuu Onee-sama(3)."



Kukklia’s body instantly froze with a jolt.



Kukklia: (Just now...what did I just hear?)



She couldn’t help but doubt her own ears. However, she could never forget that voice. The voice she had just heard was certainly a voice that she was familiar with a few years ago. But it was unfathomable. Even if one thought about hearing such a voice again, it was a voice that should be impossible to hear again.


It was precisely because of this that she had obviously assumed that the voice that entered her ears was an auditory hallucination. However, before the Kukklia whose body had been rigidly stuck in time, the figure of a girl slowly emerged.


From Kukklia’s perspective, the image of the girl who had eaten breakfast with them this morning was projected onto this girl.


Forgetting to blink, Kukklia just stared at the girl. Looking closer, she realized that something was off.


She was not carrying her inseparable paper and board used for communicating her thoughts. As Kukklia was wondering what in the world had occurred, the girl’s lips quietly moved.



Mimiru: "Kuu Onee-sama."



There was no mistake. At present, right in front of Kukklia’s eyes, her little sister Mimiru had opened her mouth. Brought forth from those lips was a charming voice that conveyed a loving atmosphere.



Kukklia: "H...... how ...... are ...... you...?"



Kukklia was unable to comprehend it. However, it was indisputable. She was able to unmistakably recognize this voice as Mimiru’s



Mimiru: "We...... we can now sing songs together again."



Mimiru’s lips trembled as she began to shed tears. Seeing such an emotional Mimiru, Kukklia embraced her.



Mimiru: “I-it hurts, Kuu Onee-sama."



Even though Mimiru closed her eyes in pain, she smiled as felt her sister’s joy.



Kukklia: “I’m happy...I’m so glad...thank god…”(4)

Mimiru: " ......yeah...... me too......"(5)



Both Kukklia and Mimiru had huge blobs of tears falling out of their eyes. Kukklia slowly brought her face in front of Mimiru. She gently used her fingers to wipe Mimiru’s tears.



Kukklia: "B-but how? How come your voice suddenly-?"



Indeed, this was the most prevalent question. Mimiru’s disorder was one that not only famous doctors couldn’t solve, but even [Passion]’s most prided researchers couldn’t cure.


Of course, it was undeniable that she was overjoyed about her improved condition. However, as expected, she also wanted to know what had cured Mimiru. Thinking such thoughts would be obvious.



Mimiru: "Eto(6), about that......"



Needless to say, Hiiro’s face instantly floated to the surface of Mimiru’s head. However, she remembered her promise to him which Hiiro expressly proposed, entailing that she would not mention him at all.



Mimiru: (I really want to tell her, though...)



No matter what he may be, he was the person who had saved her. Even if he was a ghost that was unperceivable to everyone else, she had the uncontrollable urge to tell everyone.



Kukklia: "Mimiru?"



Due to Mimiru not answering her question, Kukklia anxiously frowned.



Mimiru: "Even I’m not too sure what happened."

Kukklia: "I-is that so?"

Mimiru: "Yeah. Because today’s wind was quite pleasant, I decided to sun bask in the garden. While I was sunbathing, I became drowsy and nodded off. However, when I woke up, my voice somehow came back."

Kukklia: "...?"



Of course, Kukklia blanked out(7). She was skeptical as to whether such a miracle could occur. Mimiru began panicking a little as she began to string words.



Mimiru: "B-but you know, in my dream, a [Spirit]-san(8) came out."

Kukklia: "A [Spirit] did?"

Mimiru: "Y-yes. And this [Spirit]-san told me this. ‘This is a loan. I’ll come back and collect my due so don’t go forgetting it’. Maybe the [Spirit]-san just decided to cure me on a whim..."



Mimiru judged that this degree of story telling would be fine as she arranged Hiiro’s words. However, Arnold, who was overseeing Mimiru and the others, suddenly turned pale as he heard her words.



Arnold: (Oi, oi. Those words just now............ they couldn’t be...... right?)



From the flow of the conversation, one was able to observe that Mimiru, who had lost their voice, was suddenly able to become able to speak again. Moreover, from Kukklia’s extreme delight, it could be determined that Mimiru’s symptoms were considerably severe. However, what had cured her was just simply going to sleep.


Mimiru said that she was cured by a [Spirit]. However, are [Spirits] even capable of something like that in the first place? Even if they did heal her, Arnold felt like they wouldn’t do something like ask for repayment. Furthermore, those words that they had left her...


That use of words. Arnold felt like somebody else had used similar wording. A teenager with an arrogant attitude that Arnold was very familiar with.



Arnold: (F-for now, wouldn't it be better if I checked this...?)



Just as this thought entered his mind, Arnold felt someone tug on his clothes. It was Muir.



Muir: "N-nee~(9) Ojisan(10). That conversation just now..."



It seemed that Muir was also concerned.



Arnold: "A-aa(11). But what do you think that guy’s motive is for doing this? I mean, the subject was the princess. Fixing such an illness in one go...well, if it’s that guy then he could probably cure it. But wouldn’t doing such a thing draw attention to himself?"

Muir: "B-but those words ......"

Arnold: “I-I know what you mean...I guess even princesses tell lies. If it was a total lie though, she’d dress up the message to make it sound [Spirit]-like and sacred. To me, those words felt strangely realistic."



As it was actually Hiiro himself who had said those words, it was obviously going to sound extremely realistic. However, as they were unaware of that fact, they were still undecided as to whether it was truly Hiiro who had cured the disorder.


The main reason for this indecision is because there was no merit. For Hiiro, who had inherently loathed noise, he would avoid anything that would make him conspicuous. Especially since he was not a Gabranth, he deliberately made sure he was wary of when and where he used his magic to avoid being found out.


Well, when food was involved, everything went out the window....
However, Hiiro was one who would act upon his gains and losses, or at least that’s how Arnold and the others had evaluated him.


It is precisely because of this that curing a princess with magic in one of this country’s famous monuments, the [King’s Tree], was something unfathomable for Hiiro to do.


However, the princess’s words indicated that her disorder was cured instantly by a mysterious phenomena. Taking this into consideration, only the teenager that they had grown to become overly familiar with came into mind.



Arnold: "While I’m still uncertain, if this was Hiiro’s doing, then it looks like a hush order was put in place. Although looking at the Princess’ state, its likely that she met Hiiro."

Muir: “Wouldn’t it be better if we made sure?"

Arnold: “You’re right. Although I doubt I can...wait, can you go ask her, Muir?"

Muir: “Me?"



After processing what Arnold had asked her, it was no wonder that Muir became surprised.



Arnold: "Aa. The person in question’s a nine year old kid. Wouldn’t it be easier for you to approach her?"

Muir: "U-un(12). I got it."



As she was going to be talking to a princess, Muir nervously set off to complete the mission she had been entrusted with. 