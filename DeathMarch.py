import requests
from ebooklib import epub
import os.path
from bs4 import BeautifulSoup


def getpage(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html5lib")
    html = soup.find("div", "post-body entry-content")
    title = html.find("span").prettify(formatter="html")
    while html.h3 != None:
        html.h3.decompose()
    while html.a != None:
        html.a.decompose()
    while html.div != None:
        html.div.decompose()

    return title + "\n\n" + html.prettify(formatter="html")


chapname = ["Chapter 1-1", "Chapter 1-2", "Chapter 1-3", "Chapter 1-4", "Chapter 1-5", "Chapter 1-6", "Chapter 1-7",
            "Chapter 1-8", "Chapter 1-9", "Chapter 1-10", "Chapter 2-1", "Chapter 2-2", "Chapter 2-3", "Chapter 2-4",
            "Chapter 2-5", "Chapter 2-6", "Chapter 2-7", "Chapter 2-8", "Chapter 2-9", "Chapter 2-10", "Chapter 2-11",
            "Chapter 2-12", "Chapter 3-1", "Chapter 3-2", "Chapter 3-3", "Chapter 3-4", "Chapter 3-5", "Chapter 3-6",
            "Chapter 3-7", "Chapter 3-8", "Intermission-Zena", "Chapter 4-1", "Chapter 4-2", "Chapter 4-3",
            "Chapter 4-4", "Chapter 4-5", "Chapter 4-6", "Chapter 4-7", "Chapter 4-8", "Chapter 4-9", "Chapter 4-10",
            "Chapter 4-11", "Chapter 4-12", "Chapter 4 Intermission 1", "Chapter 4 Intermission 2", "Chapter 5-1",
            "Chapter 5-2", "Chapter 5-3", "Chapter 5-4", "Chapter 5-5", "Chapter 5-6", "Chapter 5-7", "Chapter 5-8",
            "Chapter 5-9", "Chapter 5-10", "Chapter 5-11", "Chapter 5-12", "Chapter 5-13", "Chapter 5-14",
            "Chapter 5-15", "Chapter 5 Intermission 1", "Chapter 5 Intermission 2", "Chapter 5 Intermission 3",
            "Chapter 5 Intermission 4", "Chapter 5 Intermission 5", "Chapter 5 Intermission 6", "Chapter 6-1",
            "Chapter 6-2", "Chapter 6-3", "Chapter 6-4", "Chapter 6-5", "Chapter 6-6", "Chapter 6-7", "Chapter 6-8",
            "Chapter 6-9", "Chapter 6-10", "Chapter 6-11", "Chapter 6-12", "Chapter 6-13", "Chapter 6-14",
            "Chapter 6-15", "Chapter 6-16", "Chapter 6-17", "Chapter 6-18", "Chapter 6-19", "Chapter 6-20",
            "Chapter 6-21", "Chapter 6-22", "Chapter 6-23", "Chapter 6-24", "Chapter 6-25", "Chapter 6-26",
            "Chapter 6-27", "Chapter 6-28", "Chapter 6-29", "Chapter 6-30", "Chapter 6-31", "Chapter 6-32",
            "Chapter 6-33", "Chapter 6-34", "Chapter 6 Intermission 1", "Chapter 6 Intermission 2",
            "Chapter 6 Intermission 3", "Chapter 6 Intermission 4", "Chapter 6 Intermission 5",
            "Chapter 6 Intermission 6", "Chapter 7-1", "Chapter 7-2", "Chapter 7-3", "Chapter 7-4", "Chapter 7-5",
            "Chapter 7-6", "Chapter 7-7", "Chapter 7-8", "Chapter 7-9", "Chapter 7-10", "Chapter 7-11", "Chapter 7-12",
            "Chapter 7-13", "Chapter 7-14", "Chapter 7-15", "Chapter 7-16", "Chapter 7-17", "Chapter 7-18",
            "Chapter 7-19", "Chapter 7-20", "Chapter 7-21", "Chapter 7-22", "Chapter 7-23", "Chapter 7 Intermission 1",
            "Chapter 7 Intermission 2", "Chapter 7 Intermission 3", "Chapter 7 Intermission 4",
            "Chapter 7 Intermission 5", "Chapter 8-1", "Chapter 8-2", "Chapter 8-3", "Chapter 8-4", "Chapter 8-5",
            "Chapter 8-6", "Chapter 8-7", "Chapter 8-8", "Chapter 8-9", "Chapter 8-10", "Chapter 8-11", "Chapter 8-12",
            "Chapter 8-13", "Chapter 8-14", "Chapter 8-15", "Chapter 8-16", "Chapter 8-17", "Chapter 8-18",
            "Chapter 8-19", "Chapter 8-20", "Chapter 8-21", "Chapter 8-22", "Chapter 8-23", "Chapter 8-24",
            "Chapter 8-25", "Chapter 8-26", "Chapter 8 Intermission 1", "Chapter 8 Intermission 2",
            "Chapter 8 Intermission 3", "Chapter 8 Intermission 4", "Chapter 8 Short Story 1",
            "Chapter 8 Intermission 5", "Chapter 8 Intermission 6", "Chapter 8 Intermission 7",
            "Chapter 8 Short Story 2", "Chapter 8 Short Story 3", "Chapter 8 Short Story 4", "Chapter 8 Intermission 8",
            "Chapter 9-1", "Chapter 9-2", "Chapter 9-3", "Chapter 9-4", "Chapter 9-5", "Chapter 9-6", "Chapter 9-7",
            "Chapter 9-8", "Chapter 9-9", "Chapter 9-10", "Chapter 9-11", "Chapter 9-12", "Chapter 9-13",
            "Chapter 9-14", "Chapter 9-15", "Chapter 9-16", "Chapter 9-17", "Chapter 9-18", "Chapter 9-19",
            "Chapter 9-20", "Chapter 9-21", "Chapter 9-22", "Chapter 9-23", "Chapter 9-24", "Chapter 9-25",
            "Chapter 9-26", "Chapter 9-27", "Chapter 9-28", "Chapter 9-29", "Chapter 9-30", "Chapter 9-31",
            "Chapter 9-32", "Chapter 9-33", "Chapter 9 Intermission 1", "Chapter 9 Intermission 2",
            "Chapter 9 Short Story 1", "Chapter 9 Intermission 3", "Chapter 9 Short Story 2",
            "Chapter 9 Intermission 4", "Chapter 9 Short Story 3", "Chapter 9 Short Story 4", "Chapter 9 Short Story 5",
            "Chapter 10-1", "Chapter 10-2", "Chapter 10-3", "Chapter 10-4", "Chapter 10-5", "Chapter 10-6",
            "Chapter 10-7", "Chapter 10-8", "Chapter 10-9", "Chapter 10-10", "Chapter 10-11", "Chapter 10-12",
            "Chapter 10-13", "Chapter 10-14", "Chapter 10-15", "Chapter 10-16", "Chapter 10-17", "Chapter 10-18",
            "Chapter 10-19", "Chapter 10-20", "Chapter 10-21", "Chapter 10-22", "Chapter 10-23", "Chapter 10-24",
            "Chapter 10-25", "Chapter 10-26", "Chapter 10-27", "Chapter 10-28", "Chapter 10-29", "Chapter 10-30",
            "Chapter 10-31", "Chapter 10-32", "Chapter 10-33", "Chapter 10-34", "Chapter 10-35", "Chapter 10-35-2",
            "Chapter 10-36", "Chapter 10-37", "Chapter 10-38", "Chapter 10-39", "Chapter 10-40", "Chapter 10-41",
            "Chapter 10-42", "Chapter 10-43", "Chapter 10-44", "Chapter 10-45", "Chapter 10-46", "Chapter 10-47",
            "Chapter 10-48", "Chapter 10-49", "Chapter 10-50", "Chapter 10-51", "Chapter 10 Intermission 1",
            "Chapter 10 Intermission 2", "Chapter 10 Short Story 1", "Chapter 10 Short Story 2",
            "Chapter 10 Short Story 3", "Chapter 10 Short Story 4", "Chapter 10 Intermission 3",
            "Chapter 10 Short Story 5", "Chapter 10 Short Story 6", "Chapter 10 Short Story 7",
            "Chapter 10 Short Story 8", "Chapter 10 Short Story 9", "Chapter 10 Short Story 10",
            "Chapter 10 Intermission 4", "Chapter 10 Intermission 5", "Chapter 10 Intermission 6",
            "Chapter 10 Intermission 7", "Chapter 10 Short Story 11", "Chapter 10 Short Story 12",
            "Chapter 10 Short Story 13", "Chapter 11-1", "Chapter 11-2", "Chapter 11-3", "Chapter 11-4", "Chapter 11-5",
            "Chapter 11-6", "Chapter 11-7", "Chapter 11-8", "Chapter 11-9", "Chapter 11-10", "Chapter 11-11",
            "Chapter 11-12", "Chapter 11-13", "Chapter 11-14", "Chapter 11-15", "Chapter 11-16", "Chapter 11-17",
            "Chapter 11-18", "Chapter 11-19", "Chapter 11-20", "Chapter 11-21", "Chapter 11-22", "Chapter 11-23",
            "Chapter 11 Short Story 1", "Chapter 11 Short Story 2", "Chapter 11 Short Story 3",
            "Chapter 11 Intermission 1", "Chapter 11 Short Story 4", "Chapter 11 Intermission 2",
            "Chapter 11 Short Story 5", "Chapter 11 Short Story 6", "Chapter 1 Short Story 7",
            "Chapter 1 Short Story 8", "Chapter 11 Short Story 9", "Chapter 12-1", "Chapter 12-2", "Chapter 12-3",
            "Chapter 12-4", "Chapter 12-5", "Chapter 12-6", "Chapter 12-7", "Chapter 12-8", "Chapter 12-9",
            "Chapter 12-10", "Chapter 12-11", "Chapter 12-12", "Chapter 12-13", "Chapter 12-14", "Chapter 12-15",
            "Chapter 12-16", "Chapter 12-17", "Chapter 12-18", "Chapter 12-19", "Chapter 12-20", "Chapter 12-21",
            "Chapter 12-22", "Chapter 12-23", "Chapter 12-24", "Chapter 12-25", "Chapter 12-26", "Chapter 12-27",
            "Chapter 12-28", "Chapter 12-29", "Chapter 12-30", "Chapter 12 Intermission 1", "Chapter 12 Short Story 1",
            "Chapter 12 Short Story 2", "Chapter 12 Short Story 3", "Chapter 12 Short Story 4",
            "Chapter 12 Intermission 2", "Chapter 12 Intermission 3", "Chapter 13-1", "Chapter 13-2", "Chapter 13-3",
            "Chapter 13-4", "Chapter 13-5", "Chapter 13-6", "Chapter 13-7", "Chapter 13-8", "Chapter 13-9",
            "Chapter 13-10", "Chapter 13-11", "Chapter 13-12", "Chapter 13-13", "Chapter 13-14", "Chapter 13-15",
            "Chapter 13-16", "Chapter 13-17", "Chapter 13-18", "Chapter 13-19", "Chapter 13-20", "Chapter 13-21",
            "Chapter 13-22", "Chapter 13-23", "Chapter 13-24", "Chapter 13-25", "Chapter 13-26", "Chapter 13-27",
            "Chapter 13-28", "Chapter 13-29", "Chapter 13-30", "Chapter 13-31", "Chapter 13-32", "Chapter 13-33",
            "Chapter 13-34", "Chapter 13-35", "Chapter 13-36", "Chapter 13-37", "Chapter 13-38",
            "Chapter 13 Intermission 1", "Chapter 13 Intermission 2", "Chapter 13 Short Story1",
            "Chapter 13 Short Story2", "Chapter 13 Short Story3", "Chapter 13 Short Story4", "Chapter 13 Short Story5",
            "Chapter 13 Short Story6", "Chapter 14-1", "Chapter 14-2", "Chapter 14-3", "Chapter 14-4", "Chapter 14-5",
            "Chapter 14-6", "Chapter 14-7", "Chapter 14-8", "Chapter 14-9", "Chapter 14-10", "Chapter 14-11",
            "Chapter 14 Short Story", "Chapter 14-12", "Chapter 14-13", "Chapter 14-14", "Chapter 14-15",
            "Chapter 14-16", "Chapter 14-17", "Chapter 14-18", "Chapter 14-19", "Chapter 14-20",
            "Chapter 14-21_Short Story", "Chapter 14-22", "Chapter 14-23", "Chapter 14-24", "Chapter 14-25",
            "Chapter 14-26", "Chapter 14-27", "Chapter 14-28", "Chapter 14-29", "Chapter 14-30", "Chapter 14-31",
            "Chapter 14-32", "Chapter 14-33", "Chapter 14-34", "Chapter 14-35", "Chapter 14-36", "Chapter 14-37",
            "Chapter 14-38", "Chapter 14-39", "Chapter 14-40", "Chapter 14-41", "Chapter 14-42", "Chapter 14-43",
            "Chapter 14-44", "Chapter 14-45", "Chapter 14-46", "Chapter 14-47", "Chapter 14 Intermission 1",
            "Chapter 14 Short Story1", "Chapter 14 Intermission 2", "Chapter 14 Intermission 3",
            "Chapter 14 Intermission 4", "Chapter 15-1", "Chapter 15-2", "Chapter 15-3", "Chapter 15-4", "Chapter 15-5",
            "Chapter 15-6", "Chapter 15-7", "Chapter 15-8", "Chapter 15-9", "Chapter 15-10", "Chapter 15-11",
            "Chapter 15-12", "Chapter 15-Intermission", "Chapter 15-13", "Chapter 15-14", "Chapter 15-15",
            "Chapter 15-16", "Chapter 15-17", "Chapter 15-Short Story", "Chapter 15-18", "Chapter 15-19",
            "Chapter 15-20", "Chapter 15-21", "Chapter 15-22", "Chapter 15-23", "Chapter 15-24", "Chapter 15-25",
            "Chapter 15-26", "Chapter 15-27", "Chapter 15-28", "Chapter 15-29", "Chapter 15-30", "Chapter 15-31",
            "Chapter 15-32", "Chapter 15-33", "Chapter 15-34", "Chapter 15-35", "Chapter 15-36", "Chapter 15-37",
            "Chapter 15-38", "Chapter 15-39", "Chapter 15-40", "Chapter 15-41", "Chapter 15-42", "Chapter 15-43",
            "Chapter 15-44", "Chapter 15-Intermission 1", "Chapter 15-Intermission 2", "Chapter 15-Intermission 3",
            "Chapter 15-Intermission 4", "Chapter 15-Intermission 5", "Chapter 15-Intermission 6",
            "Chapter 15-Intermission 7", "Chapter 15-Intermission 8", "Chapter 15-Intermission 9",
            "Chapter 15-Intermission 10", "Chapter 16-1", "Chapter 16-2", "Chapter 16-3", "Chapter 16-4",
            "Chapter 16-5", "Chapter 16-6", "Chapter 16-7", "Chapter 16-8", "Chapter 16-9", "Chapter 16-10",
            "Chapter 16-11", "Chapter 16-12", "Chapter 16-13", "Chapter 16-14", "Chapter 16-15", "Chapter 16-16",
            "Chapter 16-Intermission 1", "Chapter 16-17", "Chapter 16-18", "Chapter 16-19", "Chapter 16-20",
            "Chapter 16-21", "Chapter 16-22", "Chapter 16-23", "Chapter 16-24", "Chapter 16-25", "Chapter 16-26",
            "Chapter 16-27", "Chapter 16-28", "Chapter 16-29", "Chapter 16-30", "Chapter 16-31", "Chapter 16-32",
            "Chapter 16-33", "Chapter 16-34", "Chapter 16-35", "Chapter 16-36", "Chapter 16-37", "Chapter 16-38",
            "Chapter 16-39", "Chapter 16-40"]
chapurl = ["http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai.html",
           "http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_13.html",
           "http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_14.html",
           "http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_15.html",
           "http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_16.html",
           "http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_20.html",
           "http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_2.html",
           "http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_22.html",
           "http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_23.html",
           "http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_25.html",
           "http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai.html",
           "http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_8.html",
           "http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_17.html",
           "http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_22.html",
           "http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_23.html",
           "http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_25.html",
           "http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_27.html",
           "http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_28.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_2.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_3.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_86.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_93.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_71.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_6.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_7.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_9.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_10.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_11.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_13.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_17.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_18.html",
           "http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_7.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_98.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2015/05/chapter-5-intermission-1.html",
           "http://sousetsuka.blogspot.com/2015/05/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_34.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2015/05/6-2.html", "http://www.sousetsuka.com/2015/05/6-3.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2015/05/6-6.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2015/06/6-25.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_7.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2015/06/chapter-6-intermission-1.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2015/06/7-2.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_3.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_77.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_59.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_3.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_7.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_45.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_73.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_77.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_1.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_3.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2015/09/9-12.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_92.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_7.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_62.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_78.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_85.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_45.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2015/10/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_1.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_3.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_7.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_94.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2015/11/death-march-kara-hajimaru-isekai_52.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_3.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_7.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_96.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_78.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_32.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_57.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_38.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_52.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_79.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_75.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_62.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_37.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_89.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2015/12/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_3.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_86.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_79.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_81.html",
           "http://www.sousetsuka.com/2016/01/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2016/02/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_3.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_74.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_1.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2016/03/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_3.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2016/04/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_3.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2016/05/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_1.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_49.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_78.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2016/06/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_2.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_75.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2016/07/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_6.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_7.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2016/08/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2016/09/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_4.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_7.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2016/10/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_7.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2016/11/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2016/12/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2016/12/death-march-kara-hajimaru-isekai_10.html",
           "http://www.sousetsuka.com/2016/12/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2016/12/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2016/12/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2016/12/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2016/12/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2017/01/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/01/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2017/01/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2017/01/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2017/01/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2017/02/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/02/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2017/02/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2017/02/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2017/03/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/03/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2017/04/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/04/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2017/05/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/05/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2017/06/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/06/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2017/07/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/07/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2017/07/death-march-kara-hajimaru-isekai_17.html",
           "http://www.sousetsuka.com/2017/07/death-march-kara-hajimaru-isekai_24.html",
           "http://www.sousetsuka.com/2017/07/death-march-kara-hajimaru-isekai_31.html",
           "http://www.sousetsuka.com/2017/08/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/08/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2017/08/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2017/08/death-march-kara-hajimaru-isekai_14.html",
           "http://www.sousetsuka.com/2017/08/death-march-kara-hajimaru-isekai_21.html",
           "http://www.sousetsuka.com/2017/08/death-march-kara-hajimaru-isekai_28.html",
           "http://www.sousetsuka.com/2017/09/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/09/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2017/09/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2017/09/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2017/10/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/10/death-march-kara-hajimaru-isekai_5.html",
           "http://www.sousetsuka.com/2017/10/death-march-kara-hajimaru-isekai_9.html",
           "http://www.sousetsuka.com/2017/10/death-march-kara-hajimaru-isekai_16.html",
           "http://www.sousetsuka.com/2017/10/death-march-kara-hajimaru-isekai_23.html",
           "http://www.sousetsuka.com/2017/10/death-march-kara-hajimaru-isekai_30.html",
           "http://www.sousetsuka.com/2017/11/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/11/death-march-kara-hajimaru-isekai_13.html",
           "http://www.sousetsuka.com/2017/11/death-march-kara-hajimaru-isekai_20.html",
           "http://www.sousetsuka.com/2017/11/death-march-kara-hajimaru-isekai_27.html",
           "http://www.sousetsuka.com/2017/12/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2017/12/death-march-kara-hajimaru-isekai_11.html",
           "http://www.sousetsuka.com/2017/12/death-march-kara-hajimaru-isekai_18.html",
           "http://www.sousetsuka.com/2017/12/death-march-kara-hajimaru-isekai_25.html",
           "http://www.sousetsuka.com/2018/01/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2018/01/death-march-kara-hajimaru-isekai_8.html",
           "http://www.sousetsuka.com/2018/01/death-march-kara-hajimaru-isekai_15.html",
           "http://www.sousetsuka.com/2018/01/death-march-kara-hajimaru-isekai_22.html",
           "http://www.sousetsuka.com/2018/01/death-march-kara-hajimaru-isekai_29.html",
           "http://www.sousetsuka.com/2018/02/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2018/02/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2018/02/death-march-kara-hajimaru-isekai_19.html",
           "http://www.sousetsuka.com/2018/02/death-march-kara-hajimaru-isekai_26.html",
           "http://www.sousetsuka.com/2018/03/death-march-kara-hajimaru-isekai.html",
           "http://www.sousetsuka.com/2018/03/death-march-kara-hajimaru-isekai_12.html",
           "http://www.sousetsuka.com/2018/03/death-march-kara-hajimaru-isekai_19.html"]

# for i in range(1, 17):
#     if i == 1:
#         html = ""
#         html = html + getpage("http://www.sousetsuka.com/2015/01/death-march-kara-hajimaru-isekai.html")
#         print("chapter 1.1 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_13.html")
#         print("chapter 1.2 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_14.html")
#         print("chapter 1.3 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_15.html")
#         print("chapter 1.4 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_16.html")
#         print("chapter 1.5 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_20.html")
#         print("chapter 1.6 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_2.html")
#         print("chapter 1.7 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_22.html")
#         print("chapter 1.8 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_23.html")
#         print("chapter 1.9 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/01/death-march-kara-hajimaru-isekai_25.html")
#         print("chapter 1.10 - DONE")
#
#         file = open("Deathmarch/chapter1.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 1 - DONE")
#     elif i == 2:
#         html = ""
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai.html")
#         print("chapter 2.1 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_8.html")
#         print("chapter 2.2 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_17.html")
#         print("chapter 2.3 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_22.html")
#         print("chapter 2.4 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_23.html")
#         print("chapter 2.5 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_25.html")
#         print("chapter 2.6 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_27.html")
#         print("chapter 2.7 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/02/death-march-kara-hajimaru-isekai_28.html")
#         print("chapter 2.8 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai.html")
#         print("chapter 2.9 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_2.html")
#         print("chapter 2.10 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_3.html")
#         print("chapter 2.11 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_86.html")
#         print("chapter 2.12 - DONE")
#
#         file = open("Deathmarch/chapter2.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 2 - DONE")
#     elif i == 3:
#         html = ""
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_93.html")
#         print("chapter 3.1 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_71.html")
#         print("chapter 3.2 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_6.html")
#         print("chapter 3.3 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_7.html")
#         print("chapter 3.4 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_9.html")
#         print("chapter 3.5 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_10.html")
#         print("chapter 3.6 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_11.html")
#         print("chapter 3.7 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_13.html")
#         print("chapter 3.8 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_17.html")
#         print("chapter 3.9 - DONE")
#
#         file = open("Deathmarch/chapter3.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 3 - DONE")
#     elif i == 4:
#         html = ""
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_18.html")
#         print("chapter 4.1 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/03/death-march-kara-hajimaru-isekai_21.html")
#         print("chapter 4.2 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_23.html")
#         print("chapter 4.3 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_25.html")
#         print("chapter 4.4 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_26.html")
#         print("chapter 4.5 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_27.html")
#         print("chapter 4.6 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_28.html")
#         print("chapter 4.7 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_29.html")
#         print("chapter 4.8 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_30.html")
#         print("chapter 4.9 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/03/death-march-kara-hajimaru-isekai_31.html")
#         print("chapter 4.10 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai.html")
#         print("chapter 4.11 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_2.html")
#         print("chapter 4.12 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_25.html")
#         print("chapter 4.13 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_27.html")
#         print("chapter 4.14 - DONE")
#
#         file = open("Deathmarch/chapter4.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 4 - DONE")
#     elif i == 5:
#         html = ""
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_4.html")
#         print("chapter 5.1 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_5.html")
#         print("chapter 5.2 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_6.html")
#         print("chapter 5.3 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_7.html")
#         print("chapter 5.4 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_8.html")
#         print("chapter 5.5 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_10.html")
#         print("chapter 5.6 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_15.html")
#         print("chapter 5.7 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_18.html")
#         print("chapter 5.8 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_98.html")
#         print("chapter 5.9 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_19.html")
#         print("chapter 5.10 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_20.html")
#         print("chapter 5.11 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_21.html")
#         print("chapter 5.12 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_22.html")
#         print("chapter 5.13 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_23.html")
#         print("chapter 5.14 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_24.html")
#         print("chapter 5.15 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/chapter-5-intermission-1.html")
#         print("chapter 5.16 - DONE")
#         html = html + getpage("http://sousetsuka.blogspot.com/2015/05/death-march-kara-hajimaru-isekai.html")
#         print("chapter 5.17 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_9.html")
#         print("chapter 5.18 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_24.html")
#         print("chapter 5.19 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_34.html")
#         print("chapter 5.20 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_12.html")
#         print("chapter 5.21 - DONE")
#
#         file = open("Deathmarch/chapter5.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 5 - DONE")
#     elif i == 6:
#         html = ""
#         html = html + getpage("http://www.sousetsuka.com/2015/04/death-march-kara-hajimaru-isekai_28.html")
#         print("chapter 6.1 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/6-2.html")
#         print("chapter 6.2 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/6-3.html")
#         print("chapter 6.3 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_8.html")
#         print("chapter 6.4 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_11.html")
#         print("chapter 6.5 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/6-6.html")
#         print("chapter 6.6 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_14.html")
#         print("chapter 6.7 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_16.html")
#         print("chapter 6.8 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_17.html")
#         print("chapter 6.9 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_18.html")
#         print("chapter 6.10 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_19.html")
#         print("chapter 6.11 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_20.html")
#         print("chapter 6.12 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_21.html")
#         print("chapter 6.13 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_22.html")
#         print("chapter 6.14 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_23.html")
#         print("chapter 6.15 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_24.html")
#         print("chapter 6.16 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_25.html")
#         print("chapter 6.17 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_26.html")
#         print("chapter 6.18 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_27.html")
#         print("chapter 6.19 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_28.html")
#         print("chapter 6.20 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_29.html")
#         print("chapter 6.21 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_30.html")
#         print("chapter 6.22 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/05/death-march-kara-hajimaru-isekai_31.html")
#         print("chapter 6.23 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai.html")
#         print("chapter 6.24 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/6-25.html")
#         print("chapter 6.25 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_4.html")
#         print("chapter 6.26 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_5.html")
#         print("chapter 6.27 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_6.html")
#         print("chapter 6.28 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_7.html")
#         print("chapter 6.29 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_9.html")
#         print("chapter 6.30 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_10.html")
#         print("chapter 6.31 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_11.html")
#         print("chapter 6.32 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_12.html")
#         print("chapter 6.33 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_14.html")
#         print("chapter 6.34 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/chapter-6-intermission-1.html")
#         print("chapter 6.35 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_18.html")
#         print("chapter 6.36 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_19.html")
#         print("chapter 6.37 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_22.html")
#         print("chapter 6.38 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_24.html")
#         print("chapter 6.39 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_25.html")
#         print("chapter 6.40 - DONE")
#
#         file = open("Deathmarch/chapter6.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 6 - DONE")
#     elif i == 7:
#         html = ""
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_27.html")
#         print("chapter 7.1 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/7-2.html")
#         print("chapter 7.2 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_29.html")
#         print("chapter 7.3 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/06/death-march-kara-hajimaru-isekai_30.html")
#         print("chapter 7.4 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai.html")
#         print("chapter 7.5 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_2.html")
#         print("chapter 7.6 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_3.html")
#         print("chapter 7.7 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_4.html")
#         print("chapter 7.8 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_6.html")
#         print("chapter 7.9 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_8.html")
#         print("chapter 7.10 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_9.html")
#         print("chapter 7.11 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_10.html")
#         print("chapter 7.12 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_11.html")
#         print("chapter 7.13 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_12.html")
#         print("chapter 7.14 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_13.html")
#         print("chapter 7.15 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_15.html")
#         print("chapter 7.16 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_77.html")
#         print("chapter 7.17 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_17.html")
#         print("chapter 7.18 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_18.html")
#         print("chapter 7.19 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_19.html")
#         print("chapter 7.20 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_20.html")
#         print("chapter 7.21 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_21.html")
#         print("chapter 7.22 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_22.html")
#         print("chapter 7.23 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_23.html")
#         print("chapter 7.24 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_25.html")
#         print("chapter 7.25 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_59.html")
#         print("chapter 7.26 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_26.html")
#         print("chapter 7.27 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_27.html")
#         print("chapter 7.28 - DONE")
#
#         file = open("Deathmarch/chapter7.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 7 - DONE")
#     elif i == 8:
#         html = ""
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_29.html")
#         print("chapter 8.1 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/07/death-march-kara-hajimaru-isekai_31.html")
#         print("chapter 8.2 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai.html")
#         print("chapter 8.3 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_2.html")
#         print("chapter 8.4 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_3.html")
#         print("chapter 8.5 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_4.html")
#         print("chapter 8.6 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_5.html")
#         print("chapter 8.7 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_6.html")
#         print("chapter 8.8 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_7.html")
#         print("chapter 8.9 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_8.html")
#         print("chapter 8.10 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_9.html")
#         print("chapter 8.11 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_10.html")
#         print("chapter 8.12 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_11.html")
#         print("chapter 8.13 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_12.html")
#         print("chapter 8.14 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_13.html")
#         print("chapter 8.15 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_14.html")
#         print("chapter 8.16 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_15.html")
#         print("chapter 8.17 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_45.html")
#         print("chapter 8.18 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_16.html")
#         print("chapter 8.19 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_17.html")
#         print("chapter 8.20 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_18.html")
#         print("chapter 8.21 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_19.html")
#         print("chapter 8.22 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_20.html")
#         print("chapter 8.23 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_21.html")
#         print("chapter 8.24 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_22.html")
#         print("chapter 8.25 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_23.html")
#         print("chapter 8.26 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_25.html")
#         print("chapter 8.27 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_26.html")
#         print("chapter 8.28 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_28.html")
#         print("chapter 8.29 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_29.html")
#         print("chapter 8.30 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_30.html")
#         print("chapter 8.31 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_31.html")
#         print("chapter 8.32 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_2.html")
#         print("chapter 8.33 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_3.html")
#         print("chapter 8.34 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_73.html")
#         print("chapter 8.35 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/08/death-march-kara-hajimaru-isekai_77.html")
#         print("chapter 8.36 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai.html")
#         print("chapter 8.37 - DONE")
#         html = html + getpage("http://www.sousetsuka.com/2015/09/death-march-kara-hajimaru-isekai_1.html")
#         print("chapter 8.38 - DONE")
#
#         file = open("Deathmarch/chapter8.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 8 - DONE")
#     elif i == 9:
#         html = ""
#         html = html + getpage("")
#         print("chapter 3.1 - DONE")
#
#         file = open("Deathmarch/chapter3.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 2 - DONE")
#     elif i == 10:
#         html = ""
#         html = html + getpage("")
#         print("chapter 3.1 - DONE")
#
#         file = open("Deathmarch/chapter3.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 2 - DONE")
#     elif i == 11:
#         html = ""
#         html = html + getpage("")
#         print("chapter 3.1 - DONE")
#
#         file = open("Deathmarch/chapter3.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 2 - DONE")
#     elif i == 12:
#         html = ""
#         html = html + getpage("")
#         print("chapter 3.1 - DONE")
#
#         file = open("Deathmarch/chapter3.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 2 - DONE")
#     elif i == 13:
#         html = ""
#         html = html + getpage("")
#         print("chapter 3.1 - DONE")
#
#         file = open("Deathmarch/chapter3.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 2 - DONE")
#     elif i == 14:
#         html = ""
#         html = html + getpage("")
#         print("chapter 3.1 - DONE")
#
#         file = open("Deathmarch/chapter3.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 2 - DONE")
#     elif i == 15:
#         html = ""
#         html = html + getpage("")
#         print("chapter 3.1 - DONE")
#
#         file = open("Deathmarch/chapter3.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 2 - DONE")
#     elif i == 16:
#         html = ""
#         html = html + getpage("")
#         print("chapter 3.1 - DONE")
#
#         file = open("Deathmarch/chapter3.html", 'w', encoding="utf-8")
#         file.write(html)
#         file.close()
#
#         print("chapter 2 - DONE")

def getchapters():
    i = 0
    while chapname[i] != None:
        html = getpage(chapurl[i])
        file = open("Deathmarch/" + chapname[i] + ".html", 'w', encoding="utf-8")
        file.write(html)
        file.close()
        print(chapname[i] + ", DONE")
        i = i + 1


def makebook():
    folder = "/Deathmarch"
    title = "Deathmarch"
    if folder.startswith("/"):
        path = os.path.curdir + folder
    else:
        path = os.path.curdir + "/" + folder

    if os.path.isdir(path):

        book = epub.EpubBook()
        book.set_identifier('id44773005')
        book.set_title(title)
        book.set_language('en')

        chapterno = 0
        toc = ()
        while chapterno < len(chapname) and os.path.isfile(path + "/" + chapname[chapterno] + ".html"):
            chapter = epub.EpubHtml(title=chapname[chapterno], file_name="chapter" + str(chapterno) + ".xhtml")
            file = open(path + "/" + chapname[chapterno] + ".html", "r", encoding="utf8")
            chapter.content = file.read()
            file.close()
            book.add_item(chapter)
            toc = toc + (chapter,)
            chapterno = chapterno + 1
        book.spine = list(book.get_items())

        book.toc = toc
        book.add_item(epub.EpubNcx())

        style = 'BODY {color: white;}'
        nav_css = epub.EpubItem(uid="style_nav", file_name="style/nav.css", media_type="text/css", content=style)
        book.add_item(nav_css)

        epub.write_epub(title + ".epub", book, {})
        os.rename(os.path.curdir + "/" + title + ".epub", os.path.curdir + "/" + "/BOOKS/" + title + ".epub")
        print(title + " - BOOK MADE")

    else:
        print("ERROR FOLDER PATH DOSENT EXIST")
        return False

    return True

makebook()