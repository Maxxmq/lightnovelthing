import requests
from bs4 import BeautifulSoup

from book import makebook

#KumoDesuGaNaniKa

#http://blastron01.tumblr.com/kumoko-contents


def getPage(pageurl):
    r = requests.get(pageurl)
    soup = BeautifulSoup(r.text, "html5lib")
    html = soup.find("div", "post text")
    [t.extract() for t in html.findAll('ul', 'meta')]
    [t.extract() for t in html.findAll('div', 'permalink-footer clearfix')]
    [t.extract() for t in html.findAll('h1')]
    [t.extract() for t in html.findAll('h2')]
    html('p')[-1].extract()

    return html.prettify(formatter="html")


toc = BeautifulSoup(requests.get("http://blastron01.tumblr.com/kumoko-contents").text, "html5lib").find('ul', 'toc')
chapter = 1
for i in toc.findAll('li'):
    title = "<h1>" + i('span')[0].get_text() + " : " + i('span')[1].get_text() + "</h1>"
    content = getPage(i('a')[0].get('href'))

    file = open("FILES/KumoDesuGaNaniKa/chapter" + str(chapter) + ".html", 'w', encoding="utf-8")
    file.write(title)
    file.write(content)
    file.close()
    print(title + " - DONE")
    chapter = chapter + 1


makebook("FILES/KumoDesuGaNaniKa", "KumoDesuGaNaniKa")
print("DONE")