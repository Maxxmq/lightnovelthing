import requests
from bs4 import BeautifulSoup

url = "http://www.wuxiaworld.com/noveltoebook-index/noveltoebook-chapter-"
htmlstart = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
"""
htmlend = """
</body>
</html>
"""

DEBUG = False
DEBUG2 = False


chapnotest = [1]
for chapno in range(1, 456):
    r = requests.get(url + str(chapno))
    chaptername = ""
    chaptertext = ""

    soup = BeautifulSoup(r.text, "html.parser")
    data = soup.find("div", {"itemprop": "articleBody"})


    if str(data).count("<p>") < 4:
        if DEBUG:
            print("stupid formating")
        start = False
        for i in soup.findAll("p"):
            d = i.get_text()

            if start:
                if d.find("Previous Chapter Next Chapter") != -1:
                    start = False

                elif d.find("Previous Chapter Next Chapter") == -1:
                    if d.find("Chapter " + str(chapno)) != -1:
                        chaptername = d
                        chaptertext = chaptertext + '<p class="title"><strong>' + d + "</strong></p>"  + "\n\n"
                    else:
                        chaptertext = chaptertext  + "<p>" + d + "</p>" + "\n"

            else:
                if d.find("Previous Chapter Next Chapter") != -1:
                    start = True
        if chaptername == "":
            temp = str(data)
            start = temp.find("Chapter "+ str(chapno))
            end = temp[start:].find("<") + start
            chaptername = temp[start:end]
            chaptertext = '<p class="title"><strong>' + chaptername + "</strong></p>" + "\n\n" + chaptertext


    else:
        if DEBUG:
            print("easy formating")

        for i in data.findAll("p"):
            d = i.get_text()

            if DEBUG2:
                print(d)

            if d.find("Next Chapter") == -1 and d.find("Previous Chapter") == -1:
                if d.find("Chapter " + str(chapno)) != -1:
                    chaptername = d
                    chaptertext = chaptertext + '<p class="title"><strong>' + d + "</strong></p>" + "\n\n"
                else:
                    chaptertext = chaptertext + "<p>" + d + "</p>" + "\n"



    file = open("TalesOfDemonsAndGods/chapter"+str(chapno)+".html", 'w', encoding="utf-8")
    file.write(htmlstart)
    file.write(chaptertext)
    file.write(htmlend)
    file.close()
    if chaptertext.find("Next Chapter") == -1 and chaptertext.find("Previous Chapter") == -1:
        print(chaptername + ": complete")
    else:
        print(chaptername + ": ERROR =============================")

    if DEBUG:
        print(chaptertext)

