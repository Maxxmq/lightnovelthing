import requests
import os.path
import html.parser

#https://dekinaidiary.wordpress.com/translations/
#FUCKING LOTS OF WORK
#KonjikiNoMojiTsukai
from book import makebook

url1 = "https://dekinaidiary.wordpress.com/translations/"
urldict = {231: "https://dekinaidiary.wordpress.com/2016/12/22/chapter-231-the-core-of-the-founder-demon-lord/",
           232: "https://dekinaidiary.wordpress.com/2016/12/28/chapter-232-appoinment/",
           233: "https://dekinaidiary.wordpress.com/2016/12/28/chapter-233-gathering-of-hiiros-party/",
           234: "https://dekinaidiary.wordpress.com/2016/12/29/chapter-234-surprising-point-of-contact/",
           235: "https://dekinaidiary.wordpress.com/2017/01/06/chapter-235-connection-of-hiiro-and-zangeki/",
           236: "https://dekinaidiary.wordpress.com/2017/01/06/chapter-236-solicitation-success/",
           237: "https://dekinaidiary.wordpress.com/2017/01/25/chapter-237-visit-to-passion/",
           238: "https://dekinaidiary.wordpress.com/2017/01/25/chapter-238-the-tragedy-after-their-reunion/",
           239: "https://dekinaidiary.wordpress.com/2017/01/25/chapter-239-the-spirit-dwelling-in-the-tree/",
           240: "https://dekinaidiary.wordpress.com/2017/01/31/chapter-240-the-dagger-that-attacked-aragorn/",
           241: "https://dekinaidiary.wordpress.com/2017/02/03/chapter-241-ideal-land/",
           242: "https://dekinaidiary.wordpress.com/2017/02/08/chapter-242-real-food-raives-homemade-dish/",
           243: "https://dekinaidiary.wordpress.com/2017/02/08/chapter-243-mimirus-song/",
           244: "https://dekinaidiary.wordpress.com/2017/02/08/chapter-244-a-visit-at-midnight/",
           245: "https://dekinaidiary.wordpress.com/2017/02/08/chapter-245-takeover/",
           246: "https://dekinaidiary.wordpress.com/2017/02/08/chapter-246-fall-of-victorias/",
           247: "https://dekinaidiary.wordpress.com/2017/02/13/chapter-247-clarification-of-the-knife/",
           248: "https://dekinaidiary.wordpress.com/2017/02/15/chapter-248-for-now-massage/",
           249: "https://dekinaidiary.wordpress.com/2017/02/15/chapter-249-the-power-of-crimson-aura/",
           250: "https://dekinaidiary.wordpress.com/2017/02/17/chapter-250-the-newborn-power-of-zangeki/",
           251: "https://dekinaidiary.wordpress.com/2017/02/21/chapter-251-flash-fireworks/",
           252: "https://dekinaidiary.wordpress.com/2017/02/21/chapter-252-the-modest-love-of-the-two/",
           253: "https://dekinaidiary.wordpress.com/2017/02/22/chapter-253-the-simple-minded-old-loli/",
           254: "https://dekinaidiary.wordpress.com/2017/02/28/chapter-254-second-princess-farah/",
           255: "https://dekinaidiary.wordpress.com/2017/03/01/chapter-255-marquis-visit/",
           256: "https://dekinaidiary.wordpress.com/2017/03/02/chapter-256-marquis-advice/",
           257: "https://dekinaidiary.wordpress.com/2017/03/14/chapter-257-the-decision-of-the-two/",
           258: "https://dekinaidiary.wordpress.com/2017/03/25/chapter-258-what-is-body-power/",
           259: "https://dekinaidiary.wordpress.com/2017/03/30/chapter-259-hiiros-roots/",
           260: "https://dekinaidiary.wordpress.com/2017/04/05/chapter-260-sacrifice/",
           261: "https://dekinaidiary.wordpress.com/2017/04/06/chapter-261-the-core-of-the-founder-demon-king-deprived/",
           262: "https://dekinaidiary.wordpress.com/2017/04/05/chapter-262-cruzers-confession/",
           263: "https://dekinaidiary.wordpress.com/2017/04/06/chapter-263-the-creation-of-sacrifice/",
           264: "https://dekinaidiary.wordpress.com/2017/04/06/chapter-264-cupidos-tribe/",
           265: "https://dekinaidiary.wordpress.com/2017/04/07/chapter-265-warped-ideology/",
           266: "https://dekinaidiary.wordpress.com/2017/04/08/chapter-266-the-grave-posts/",
           267: "https://dekinaidiary.wordpress.com/2017/04/29/chapter-267-countdown-to-the-start-of-war/",
           268: "https://dekinaidiary.wordpress.com/2017/05/06/chapter-268-is-liliyn-a-little-girl/",
           269: "https://dekinaidiary.wordpress.com/2017/05/14/chapter-269-before-the-separation/",
           270: "https://dekinaidiary.wordpress.com/2017/05/27/chapter-270-hiiros-return-to-xaous/",
           271: "https://dekinaidiary.wordpress.com/2017/05/28/chapter-271-unforseen-accident/",
           272: "https://dekinaidiary.wordpress.com/2017/06/10/chapter-272-as-expected-of-hiiro/",
           273: "https://dekinaidiary.wordpress.com/2017/06/11/chapter-273-the-pure-hearted-demon-lord/",
           274: "https://dekinaidiary.wordpress.com/2017/07/15/chapter-274-the-terms-of-forgiveness/"
           }

htmlstart = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
"""
htmlend = """
</body>
</html>
"""

def downloadthing(i):
    if os.path.isfile(os.path.curdir + "/KonjikiNoMojiTsukai/Chapter" + str(i) + ".html"):
        print("CHAPTER " + str(i) + " - FOUND PREVIOUS ONE")
        return True
    if i in urldict:
        r = requests.get(urldict[i])
        html = r.text
        html = html[html.find('<h1 class="entry-title">')+24:]
        title = html[:html.find("</h1>")]
        html = html[html.find("<hr>")+4:]
        file = open(os.path.curdir + "/KonjikiNoMojiTsukai/Chapter" + str(i) + ".html", 'w', encoding="utf-8")
        file.write(htmlstart)
        file.write("<b>"+title+"</b>")
        file.write(html[:html.find('<style type="text/css">')])
        file.write(htmlend)
        file.close()
        print("CHAPTER " +str(i)+" : DONE")
        return True
    else:
        print(str(i) + " - NOT FOUND IN DICTIONARY")
        return False

def lookandformat(i):
    if os.path.isfile(os.path.curdir + "/KonjikiNoMojiTsukai/Chapter" + str(i) + ".html"):
        print("CHAPTER " + str(i) + " - FOUND PREVIOUS ONE")
        return True
    elif os.path.isfile(os.path.curdir + "/KonjikiNoMojiTsukaiTEMP/chapter" + str(i) + ".txt"):
        oldfile = open(os.path.curdir + "/KonjikiNoMojiTsukaiTEMP/chapter" + str(i) + ".txt", 'r')
        newfile = open(os.path.curdir + "/KonjikiNoMojiTsukai/Chapter" + str(i) + ".html", 'w', encoding="utf-8")
        newfile.write(htmlstart)

        temp = True
        for line in oldfile:
            if temp:
                newfile.write("<b>" + line + "</b>")
                temp = False
            elif temp == "":
                continue
            elif temp == " ":
                continue
            else:
                line2 = line.replace('<', '&lt;')
                line3 = line2.replace('>', '&gt;')
                newfile.write("<p>" + line3 + "</p>")

        newfile.write(htmlend)
        newfile.close()
        oldfile.close()
        print("CHAPTER " + str(i) + " - DONE")
        return True

    else:
        print("ERROR WITH CHAPTER " + str(i))
        return False



chapterno = 1

while True:
    if chapterno in range(1, 231):
        lookandformat(chapterno)
        chapterno = chapterno + 1
    else:
        if downloadthing(chapterno):
            chapterno = chapterno + 1
            continue
        else:
            break

makebook("/KonjikiNoMojiTsukai", "KonjikiNoMojiTsukai")
print("Done")

