import requests
from bs4 import BeautifulSoup

url = "http://phoenix.translatednovels.com/smgt-chapter-"
htmlstart = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<body>
"""
htmlend = """
</body>
</html>
"""


chapterno = 1

while True:
    r = requests.get(url + str(chapterno))
    chaptername = ""
    chaptertext = ""

    soup = BeautifulSoup(r.text, "html5lib")

    if soup.find("section", "error-404 not-found"):
        print("CHAPTER " + str(chapterno) + " NOT FOUND")
        break

    data = soup.find("div", "entry-content")
    if data.find("b"):
        chaptername = data.find("b").prettify(formatter="html")
    elif data.find("strong"):
        chaptername = data.find("strong").prettify(formatter="html")
    else:
        print("ERROR CHAPTER TITLE NOT FOUND")
        break
    for i in data.find_all("span"):
        if 'style' in i.attrs:
            del i.attrs['style']
        i.name = "p"
        chaptertext = chaptertext + i.prettify(formatter="html") + "\n"

    chaptertext = chaptertext.replace("&nbsp;", " ")
    file = open("StarMartialGodTechnique/chapter" + str(chapterno) + ".html", 'w', encoding="utf-8")
    file.write(htmlstart)
    file.write(chaptername + "\n\n")
    file.write(chaptertext)
    file.write(htmlend)
    file.close()
    print("CHAPTER " + str(chapterno) + " - COMPLETE")
    chapterno = chapterno + 1

